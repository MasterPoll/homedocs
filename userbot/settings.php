<?php

$settings = array (
  'language' => 'en',
  'bot_file' => 'bot.php',
  'readmsg' => true,
  'send_errors' => false,
  'always_online' => false,
  'auto_reboot' => false,
  'madelineCli' => false,
  'send_data' => false,
  'madelinePhar' => 'madeline.php',
);

$madelineSettings = array (
  'app_info' => 
  array (
    'api_id' => 6,
    'api_hash' => 'eb06d4abfb49dc3eeb1aeb98ae0f581e',
    'lang_code' => 'en',
    'app_version' => '5.9.0',
    'device_model' => 'Asus ASUS_Z00ED',
    'system_version' => 'Android Nougat MR1 (25)',
  ),
  'logger' => 
  array (
    'logger' => 0,
  ),
  'secret_chats' => 
  array (
    'accept_chats' => false,
  ),
);
