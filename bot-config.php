<?php

$config = [
    'usernames' => [
        689942156 => "MasterPollBot",
        964908342 => "MasterPoll_Bot",
        757060205 => "MasterPoll2Bot",
        754307698 => "NewGroupAgreeBot",
        937893103 => "MasterPollBetaBot",
		753396760 => "GoldenPollBot",
		1131475161 => "endfoeugwbfbeqfndbot"
    ],
	'status' => json_decode(file_get_contents("/home/masterpoll-documents/status-bots.json"), true),
    'beta_bots' => [754307698, 937893103, 1131475161],
    'password' => "818wih2hegyw7wiwhg3geuw82qoa",
    'encrypt_method' => "BF-OFB",
    'secret_key' => 'zx315g43fq30f3zf3fs',
    'secret_iv' => 'gs3116g5es6s8',
    'admins' => [244432022, 836296867],
    'devmode' => false,
    'method' => 'post',
    'response' => false,
    'request_timeout' => 2,
    'json_payload' => false,
    'usa_il_db' => false,
    'usa_redis' => false,
    'console' => -1001493469874,
    'whitelist_users' => false,
    'whitelist_chats' => false,
	'log_report' => [
        'SHUTDOWN' => true,
        'FATAL' => true,
        'ERROR' => true,
        'WARN' => true,
        'INFO' => false,
        'DEBUG' => false
    ],
    'sponsor' => -1001253538591,
	// ID del Canale Sponsor
    'sponsor_messages' => [
		'global' => [13, 15, 27, 33, 41, 43],
		'it' => [16, 24, 38, 44, 45]
	],
	'affiliate' => [
		'user_start' => 10,
		'user_advertising' => 2
	],
	// Messaggi sponsor attivi
	'links' => [
		'group' => "https://r.gomp.cf/tg-group",
		'FAQ' => "https://r.gomp.cf/tg-faq",
		'premiumFAQ' => "https://r.gomp.cf/tg-pro",
		'premiumGroupInvite' => "https://t.me/joinchat/DpG8llhgNwtxHG2goIikUw",
		'paypal' => "https://paypal.me/NeleB54Gold"
	],
	'clonerbot_username' => 'MPClonesBot',
	// Vari link del Bot
    'types' => json_decode(file_get_contents("/home/masterpoll-documents/types.json"), true),
	// Tipologie di sondaggi gestibili da /types
	'shop' => json_decode(file_get_contents("/home/masterpoll-documents/shop.json"), true),
	// Oggetti dello shop gestibili da /shop
	'shop_discount' => 100,
	// Sconto in percentuale
	
    'operatori_comandi' => ['/'],
    'post_canali' => true,
    'modificato' => false,
    'azioni' => false,
    'parse_mode' => "HTML",
    'disabilita_notifica' => false,
    'disabilita_anteprima_link' => true,
    'logs_token' => "1244848329:AAHiFgd2q9bVInJlQ9AA-EYfO8m1dDKgl6g",
    'numbers' => [
        0 => [
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5,
            6 => 6,
            7 => 7,
            8 => 8,
            9 => 9,
            10 => 10
        ],
        1 => [
            1 => "1️⃣",
            2 => "2️⃣",
            3 => "3️⃣",
            4 => "4️⃣",
            5 => "5️⃣",
            6 => "6️⃣",
            7 => "7️⃣",
            8 => "8️⃣",
            9 => "9️⃣",
            10 => "🔟"
        ],
        2 => [
            1 => "¹",
            2 => "²",
            3 => "³",
            4 => "⁴",
            5 => "⁵",
            6 => "⁶",
            7 => "⁷",
            8 => "⁸",
            9 => "⁹",
            10 => "¹⁰"
        ],
        3 => [
            1 => "①",
            2 => "②",
            3 => "③",
            4 => "④",
            5 => "⑤",
            6 => "⑥",
            7 => "⑦",
            8 => "⑧",
            9 => "⑨",
            10 => "⑩"
        ],
        4 => [
            1 => "⑴",
            2 => "⑵",
            3 => "⑶",
            4 => "⑷",
            5 => "⑸",
            6 => "⑹",
            7 => "⑺",
            8 => "⑻",
            9 => "⑼",
            10 => "⑽"
        ]
    ],
	'logger' => "bot1203914620:AAFCbW2QAdli7DRKlvPw00O9hZ1ktOTqkAI",
	'logger_chat' => -1001173899239,
	'clones_logger' => "bot1244848329:AAHiFgd2q9bVInJlQ9AA-EYfO8m1dDKgl6g",
	'clones_logger_chat' => -1001217903505,
	'shop_logger' => "bot1170799712:AAETLNw0hDiiezmcqV3a1BGQUtLL0KF9J1E",
	'shop_logger_chat' => -1001415102945,
	'admin_perms' => ['infopoll', 'spoll', 'gestione']
];
if (!isset($config['usernames'][$botID])) {
	$is_clone = true;
} else {
	$cmessage = "\n📢 @MasterPoll";
	$config['username_bot'] = $config['usernames'][$botID];
}

$database = [
    'type' => "postgre",
    'nome_database' => "pollbot",
    'utente' => "pollbot",
    'password' => "BFTs-rnc-rOxbiEIVlAZijKn5dSNhnv8",
    'host' => "localhost"
];

$config['redis'] = [
    'database' => 498,
    'host' => "localhost",
    'port' => 6379,
    'password' => false,
];

$langsname = json_decode('{"en":"🇬🇧 English","de":"🇩🇪 Deutsch","it":"🇮🇹 Italiano","pt":"🇧🇷 Português","he":"🇮🇱 עברית","uk":"🇺🇦 Українська","nb":"🇳🇴 Norsk","fa":"🇮🇷 پارسی","es":"🇪🇸 Español","zh_TW":"🇹🇼 台灣正體","zh_HK":"🇭🇰 港澳正體","ja":"🇨🇳 中囯簡体","ru":"🇷🇺 Русский","fr":"🇫🇷 Français","uz":"🇺🇿 O\'zbek"}', true);

if (in_array($botID, $config['beta_bots'])) {
	$f['bot.dir'] = "/var/www/bot-betas/";
    $config['devmode'] = true;
    $config['response'] = false;
    $config['whitelist_users'] = [];
	$f['thumb_logo'] = "/home/masterpoll-documents/thumb_logo_beta.jpeg";
    $f['languages'] = "languages_beta.json";
    $cmessage = "\n📢 @MasterPollBeta";
	/*$database['host'] = "45.137.200.117";
	$database['utente'] = "pollbot";
	$database['password'] = "1ht8Lnpv5NfSWgshbamYqDfXb95JlUu1";
	$database['nome_database'] = "pollbot";
	 */
}
