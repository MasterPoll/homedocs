<?php

if (!$api) {
	if (substr(php_sapi_name(), 0, 3) == 'cgi') {
		header("Status: 502 Bad Gateway");
	} else {
		header("HTTP/1.1 502 Bad Gateway");
	}
	echo json_encode(['ok' => false, 'error' => 502, 'description' => "Bad Gateway: #0"]);
	die;
} else {
	$pluginp = "api.php";
}
$ok = false;

// Configuration
@require("/home/masterpoll-documents/api-config.php");
if (!isset($config)) {
	if (substr(php_sapi_name(), 0, 3) == 'cgi') {
		header("Status: 502 Bad Gateway");
	} else {
		header("HTTP/1.1 502 Bad Gateway");
	}
	echo json_encode(['ok' => false, 'error' => 502, 'description' => "Bad Gateway: #2"]);
	die;
}
if ($config['logs_token']) {
	function call_error($error = "Null", $plugin = 'no', $chat = 'def') {
		global $config;
		global $pluginp;
		global $poll_id;
		global $creator;
		global $userID;
		if ($chat == 'def') {
			$chat = $config['console'];
		}
		if (!$pluginp) {
		} elseif ($plugin == 'no') {
			$plugin = $pluginp;
		}
		if (isset($poll_id) or isset($creator)) {
			$sondaggio = "\n<b>Sondaggio:</b> $poll_id-$creator";
		}
		$text = "#APIError \n$error \n<b>Plugin:</b> $plugin $sondaggio\n<b>API User</b>: <code>$userID</code>";
		$args = [
			'chat_id' => $chat,
			'text' => $text,
			'parse_mode' => 'html'
		];
		$ch = curl_init();
		$url .= "https://api.telegram.org/bot" . $config['logs_token'] ."/sendMessage?" . http_build_query($args);
		curl_setopt_array($ch, [
			CURLOPT_URL => $url,
			CURLOPT_POST => false,
			CURLOPT_CONNECTTIMEOUT_MS => 100,
			CURLOPT_RETURNTRANSFER => true
		]);
		$output = curl_exec($ch);
		return true;
	}
	if (file_exists("/home/masterpoll-documents/api-methods.json")) {
		$methods = json_decode(file_get_contents("/home/masterpoll-documents/api-methods.json"), true);
	} else {
		$methods = [];
	}
	# Segnalazione errori php
	set_error_handler("errorHandler");
	register_shutdown_function("shutdownHandler");
	function errorHandler($error_level, $error_message, $error_file, $error_line, $error_context) {
		global $config;
		$error = $error_message . " \nLine: " . $error_line;
		switch ($error_level) {
			case E_ERROR:
			case E_CORE_ERROR:
			case E_COMPILE_ERROR:
			case E_PARSE:
				if ($config['log_report']['FATAL']) {
					call_error("FATAL: " . $error, $error_file);
				}
				break;
			case E_USER_ERROR:
			case E_RECOVERABLE_ERROR:
				if ($config['log_report']['ERROR']) {
					call_error("ERROR: " . $error, $error_file);
				}
				break;
			case E_WARNING:
			case E_CORE_WARNING:
			case E_COMPILE_WARNING:
			case E_USER_WARNING:
				if ($config['log_report']['WARN']) {
					call_error("WARNING: " . $error, $error_file);
				}
				break;
			case E_NOTICE:
			case E_USER_NOTICE:
				if ($config['log_report']['INFO']) {
					call_error("INFO: " . $error, $error_file);
				}
				break;
			case E_STRICT:
				if ($config['log_report']['DEBUG']) {
					call_error("DEBUG: " . $error, $error_file);
				}
				break;
			default:
				if ($config['log_report']['WARN']) {
					call_error("WARNING: " . $error, $error_file);
				}
		}
	}
	function shutdownHandler() {
		global $config;
		$lasterror = error_get_last();
		switch ($lasterror['type']) {
			case E_ERROR:
			case E_CORE_ERROR:
			case E_COMPILE_ERROR:
			case E_USER_ERROR:
			case E_RECOVERABLE_ERROR:
			case E_CORE_WARNING:
			case E_COMPILE_WARNING:
			case E_PARSE:
				if ($config['log_report']['SHUTDOWN']) {
					$error = $lasterror['message'] . " \nLine: " . $lasterror['line'];
					call_error($error, $lasterror['file']);
				}
				echo json_encode(['ok' => false, 'description' => "Bad Gateway"]);
				fastcgi_finish_request();
		}
	}
} else {
	if (substr(php_sapi_name(), 0, 3) == 'cgi') {
		header("Status: 502 Bad Gateway");
	} else {
		header("HTTP/1.1 502 Bad Gateway");
	}
	echo json_encode(['ok' => false, 'error' => 502, 'description' => "Bad Gateway: #1"]);
	die;
}

if (isset($_GET)) {
	$query = $_GET;
} elseif (isset($_POST)) {
	$query = $_POST;
} else {
	$query = false;
}
$request = strtolower(str_replace("?" . $_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']));
function log_api($response = [], $tosend = 'def') {
	global $config;
	global $pluginp;
	global $poll_id;
	global $creator;
	global $userID;
	global $islocal;
	global $request;
	if (!$config['logs']) return false;
	if ($tosend === 'def') $tosend = false;
	$chat = $config['console'];
	$plugin = $pluginp;
	if (isset($poll_id) or isset($creator)) {
		$sondaggio = "\n<b>Sondaggio:</b> $poll_id-$creator";
	}
	if ($userID) {
		$user = "<b>Utente:</> " . $userID;
		$hashtag .= "#id$userID ";
	} elseif ($islocal) {
		$user = "<b>Utente:</> locale";
	} else {
		$user = "<b>Utente:</> non identificato";
		$hashtag .= "#ip_" . str_replace('.', '_', $_SERVER['REMOTE_ADDR']) . " ";
	}
	if ($response['ok']) {
		$r = "<b>Risultato:</> ✅";
	} else {
		$r = "<b>Risultato:</> ❌";
		$r .= "\n<b>Error " . $response['error_code'] . ":</> " . $response['description'];
		$hashtag .= "#Errors ";
	}
	$text = "#Request $hashtag\n<b>Metodo:</> $request\n$r\n$user\n<b>Plugin:</b> $plugin $sondaggio";
	if ($tosend) {
		$args = [
			'chat_id' => $chat,
			'text' => $text,
			'parse_mode' => 'html'
		];
		$ch = curl_init();
		$url .= "https://api.telegram.org/bot" . $config['logs_token'] ."/sendMessage?" . http_build_query($args);
		curl_setopt_array($ch, [
			CURLOPT_URL => $url,
			CURLOPT_POST => false,
			CURLOPT_CONNECTTIMEOUT_MS => 100,
			CURLOPT_RETURNTRANSFER => true
		]);
		curl_exec($ch);
		curl_close($ch);
	}
	return true;
}
function done() {
	global $ok;
	global $config;
	global $result;
	global $error;
	global $error_description;
	$json['ok'] = $ok;
	if (isset($result)) {
		$json['result'] = $result;
	} elseif (isset($error)) {
		$json['error_code'] = $error;
		if (isset($error_description)) {
			$json['description'] = $error_description;
		} else {
			$json['description'] = $config['errors'][$error];
		}
	} else {
		$json['result'] = $ok;
	}
	if ($ok) {
		if (substr(php_sapi_name(), 0, 3) == 'cgi') {
			header("Status: 200");
		} else {
			header("HTTP/1.1 200");
		}
	} else {
		if (substr(php_sapi_name(), 0, 3) == 'cgi') {
			header("Status: $error " . $config['errors'][$error]);
		} else {
			header("HTTP/1.1 $error " . $config['errors'][$error]);
		}
	}
	echo json_encode($json);
	fastcgi_finish_request();
	log_api($json);
	return true;
}

if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
	$_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
}
if ($config['redis']) {
	if (!class_exists("Redis")) {
		$error = 500;
		$error_description = $config['errors'][$error] . ": Redis not work at this moment...";
		done();
		die;
	}
	$redisc = $config['redis'];
	try {
		$redis = new Redis();
		$redis->connect($redisc['host'], $redisc['port']);
	} catch (Exception $e) {
		$error = 500;
		$error_description = $config['errors'][$error];
		done();
		die;
	}
	if ($redisc['password'] !== false) {
		try {
			$redis->auth($redisc['password']);
		} catch (Exception $e) {
			$error = 500;
			$error_description = $config['errors'][$error];
			done();
			die;
		}
	}
	if ($redisc['database'] !== false) {
		try {
			$redis->select($redisc['database']);
		} catch (Exception $e) {
			$error = 500;
			$error_description = $config['errors'][$error];
			done();
			die;
		}
	}
} else {
	$error = 500;
	$error_description = $config['errors'][$error] . ": Redis is offline at this moment...";
	done();
	die;
}
if ($config['database']) {
	function db_query($query, $args = false, $fetch = false) {
		global $PDO;
		if (!$PDO) {
			call_error("#PDOError \nQuery: " . code($query) . " \nDatabase non avviato.");
			return ['ok' => false, 'error_code' => 500, 'description' => "Internal Server Error: database not started"];
		}
		try {
			$q = $PDO->prepare($query);
			$db_query['ok'] = true;
		} catch (PDOException $e) {
			$db_query['ok'] = false;
			$db_query['error'] = $e;
			return $db_query;
		}
		if (is_array($args)) {
			$q->execute($args);
			$db_query['args'] = $args;
		} else {
			$q->execute();
		}
		$error = $q->errorInfo();
		if ($error[0] !== "00000") {
			$db_query['ok'] = false;
			call_error("PDO Error\n<b>INPUT:</> " . code($query) . "\n<b>OUTPUT:</> " . code(json_encode($error)));
			$db_query['error_code'] = $error[0];
			$db_query['description'] = "[MYSQL]" . $error[2];
			return $db_query;
		}
		if ($fetch) {
			$db_query['result'] = $q->fetch(\PDO::FETCH_ASSOC);
		} elseif ($fetch == "no") {
			$db_query['result'] = true;
		} else {
			$db_query['result'] = $q->fetchAll();
		}
		return $db_query;
	}
	function getName($userID = false) {
		global $PDO;
		global $config;
		if (!$userID) {
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$userID was not found", 'result' => "Unknown user"];
		} elseif (!is_numeric($userID)) {
			return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$userID must be a numeric value", 'result' => "Unknown user"];
		}
		if (!$PDO) {
			return ['ok' => false, 'error_code' => 500, 'description' => "Internal Server Error: database not started"];
		}
		if ($config['usa_redis']) {
			$nomer = rget("name$userID");
			if ($nomer['ok'] and $nomer['result'] and !isset($nomer['result']['ok'])) return ['ok' => true, 'redis' => $nomer, 'result' => $nomer['result']];
		}
		$q = $PDO->prepare("SELECT * FROM utenti WHERE user_id = ?");
		$q->execute([$userID]);
		$error = $q->errorInfo();
		if ($error[0] !== "00000") {
			call_error("PDO Error\n<b>INPUT:</> " . code($q) . "\n<b>OUTPUT:</> " . code(json_encode($error)));
			$db_query = [
				'ok' => false,
				'error_code' => $error[0],
				'description' => "[PGSQL]" . $error[2],
				'result' => "Deleted account"
			];
		} else {
			$u = $q->fetchAll()[0];
			if ($u['cognome']) $u['nome'] .= " " . $u['cognome'];
			if (isset($u['nome'])) {
				$rr = htmlspecialchars($u['nome']);
			} else {
				$rr = "Deleted account";
			}
			if ($config['usa_redis']) {
				$res = rset("name$userID", $rr);
			}
			$db_query = [
				'ok' => true,
				'redis' => $res,
				'result' => $rr
			];
		}
		return $db_query;
	}
} else {
	$error = 500;
	$error_description = $config['errors'][$error];
	done();
	die;
}
function checkIfLocalIP() {
	global $_SERVER;
	if ($_SERVER['REMOTE_ADDR'] == $_SERVER['SERVER_ADDR']) {
		return true;
	} else {
		return false;
	}
}

if (checkIfLocalIP()) {
	$access = true;
	$islocal = true;
} else {
	$islocal = false;
	if (strpos($request, "/user") === 0) {
		$q = explode("/", $_SERVER['REQUEST_URI']);
		$token = str_replace("user", '', $q[1]);
		$request = str_replace(strtolower("/user$token"), '', $request);
		$userID = explode(":", $token)[0];
		$password = explode(":", $token)[1];
		if (!$password or !is_numeric($userID)) {
			$access = false;
			$error = 401;
			$error_description = $config['errors'][$error] . ": invalid user key";
		} else {
			if (!$PDO) {
				try {
					$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
				} catch (PDOException $e) {
					$error = 500;
					$error_description = $config['errors'][$error] . " database connection lost";
					done();
					die;
				}
			}
			$user = db_query("SELECT * FROM utenti WHERE user_id = ?", [$userID], true);
			if ($user['ok']) {
				$user = $user['result'];
			} else {
				$error = 500;
				$error_description = $config['errors'][$error] . " database error";
				done();
				die;
			}
			if ($user['user_id'] == $userID) {
				$userID = $user['user_id'];
				$user['settings'] = json_decode($user['settings'], true);
				$user['status'] = json_decode($user['status'], true);
				if (strpos($user['status'][689942156], "ban") === 0) {
					$error = 401;
					$error_description = $config['errors'][$error] . ": user banned";
					done();
					die;
				} elseif ($password == $user['settings']['token']) {
					$access = true;
				} else {
					$access = false;
					$error = 401;
					$error_description = $config['errors'][$error] . ": wrong user key";
				}
			} else {
				$access = false;
				$error = 401;
				$error_description = $config['errors'][$error] . ": user not exists";
			}
		}
	} else {
		$access = 'limited';
	}
}
if ($config['class_work']) {
	// Array Datas to Client Data
	function pollToArray ($poll = false)
	{
		if (!isset($poll)) {
			cell_error("<b>Warning:</b> la variabile \$poll non è stata settata nella funzione pollToArray");
			return [];
		}
		if ($poll['anonymous']) {
			$privacy = "anonymous";
		} else {
			$privacy = "personal";
		}
		$choices = [];
		if ($poll['usersvotes']) {
			if ($poll['type'] == "board") {
				$ischoices = "comments";
				$choices = [];
				foreach ($poll['choice'] as $id => $comment) {
					if ($poll['anonymous']) {
						$choices[] = $comment;
					} else {
						$choices[getName($id)['result']] = $comment;
					}
				}
			} elseif ($poll['type'] == "participation") {
				$ischoices = "participants";
				foreach ($poll['usersvotes']['participants'] as $id) {
					$choices[] = getName($id)['result'];
				}
			} else {
				$ischoices = "choices";
				foreach ($poll['usersvotes'] as $choice => $users) {
					if ($poll['anonymous']) {
						$choices[$choice] = 0;
					} else {
						$choices[$choice] = [];
					}
					foreach ($users as $id) {
						if ($poll['anonymous']) {
							if (!$choices[$choice]) {
								$choices[$choice] = 0;
							}
							$choices[$choice] = $choices[$choice] + 1;
						} else {
							$choices[$choice][] = getName($id)['result'];
						}
					}
					if (!isset($choices[$choice])) {
						$choices[$choice] = [];
					}
				}
			}
		} else {
			if ($poll['type'] == "board") {
				$ischoices = "comments";
			} elseif ($poll['type'] == "participation") {
				$ischoices = "participants";
			} else {
				$ischoices = "choices";
			}
		}
		$array = [
			'poll_id' => round($poll['poll_id']),
			'owner_id' => round($poll['creator']),
			'title' => $poll['title'],
		];
		if (isset($poll['description'])) {
			$array['description'] = $poll['description'];
		}
		$array['privacy'] = $privacy;
		$array['type'] = $poll['type'];
		if ($poll['type'] == "limited doodle") $array['max_choices'] = $poll['settings']['max_choices'];
		$array[$ischoices] = $choices;
		return $array;
	}

	// Encode function
	function bot_encode ($datas)
	{
		global $config;
		if (is_array($datas)) {
			$datas = json_encode($datas);
		}
		$key = hash('sha256', $config['secret_key']);
		$iv = substr(hash('sha256', $config['secret_iv']), 0, 8);
		$string = str_replace('=', '', base64_encode(openssl_encrypt($datas, $config['encrypt_method'], $key, 0, $iv)));
		return $string;
	}

	// Decode function
	function bot_decode ($string)
	{
		global $config;
		$key = hash('sha256', $config['secret_key']);
		$iv = substr(hash('sha256', $config['secret_iv']), 0, 8);
		$datas = openssl_decrypt(base64_decode($string), $config['encrypt_method'], $key, 0, $iv);
		return $datas;
	}

	// Get ID for create a new poll
	function getID ($creator = false) 
	{
		if (!$creator) {
			call_error("<b>Errore:</b> la variabile \$creator non è stata settata nella funzione getID");
			return 1;
		} elseif(!is_numeric($creator)) {
			call_error("<b>Error:</b> la variabile \$creator non è numerica nella funzione getID"); 
			return 1;
		}
		$getTotalPoll = getTotalPoll($creator);
		if ($getTotalPoll['ok']) {
			$getTotalPoll = $getTotalPoll['result'];
		} else {
			return 1;
		}
		$id = count($getTotalPoll);
		if ($id == 0) {
			return 1;
		} elseif ($id == 1) {
			return 2;
		} else {
			return $id + 1;
		}
	}

	// Delete poll
	function cancelPoll ($poll_id = false, $creator = false) 
	{
		if(!$poll_id) {
			call_error("<b>Error:</b> la variabile \$poll_id non è stata settata nella funzione cancelPoll"); 
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$poll_id was not found"];
		} elseif(!$creator) {
			call_error("<b>Error:</b> la variabile \$creator non è stata settata nella funzione cancelPoll"); 
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator was not found"];
		} elseif(!is_numeric($poll_id)) {
			call_error("<b>Error:</b> la variabile \$poll_id non è numerica nella funzione cancelPoll"); 
			return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$poll_id must be a numeric value"];
		} elseif(!is_numeric($creator)) {
			call_error("<b>Error:</b> la variabile \$creator non è numerica nella funzione cancelPoll"); 
			return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$creator must be a numeric value"];
		} else {
			return db_query("UPDATE polls SET title = ?, description = ?, choices = ?, status = ?, settings = ?, anonymous = ? WHERE user_id = ? and poll_id = ?", ["false", "", "[]", 'deleted', '[]', "", $creator, $poll_id], "no");
		}
	}

	// Get poll data
	function sendPoll ($poll_id = false, $creator = false) 
	{
		if(!$poll_id) {
			call_error("<b>Error:</b> la variabile \$poll_id non è stata settata nella funzione sendPoll"); 
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$poll_id was not found"];
		} elseif(!$creator) {
			call_error("<b>Error:</b> la variabile \$creator non è stata settata nella funzione sendPoll"); 
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator was not found"];
		} elseif(!is_numeric($poll_id)) {
			call_error("<b>Error:</b> la variabile \$poll_id non è numerica nella funzione sendPoll"); 
			return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$poll_id must be a numeric value"];
		} elseif(!is_numeric($creator)) {
			call_error("<b>Error:</b> la variabile \$creator non è numerica nella funzione sendPoll"); 
			return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$creator must be a numeric value"];
		} else {
			$q = db_query("SELECT * FROM polls WHERE user_id = ? and poll_id = ? LIMIT 1", [round($creator), round($poll_id)], true);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				return $q;
			}
			$q['choices'] = json_decode($q['choices'], true);
			if (is_array($q['choices'])) {
				$choicesArray = $q['choices'];
			} else {
				return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: poll choices must be an array"];
			}
			$q['settings'] = json_decode($q['settings'], true);
			if (!is_array($q['settings'])) {
				return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: poll settings must be an array"];
			}
			if (isset($q['settings']['closetime'])) {
				if (time() >= $q['settings']['closetime']) {
					$q['status'] = "closed";
					unset($q['settings']['closetime']);
					db_query("UPDATE polls SET status = ?, settings = ? WHERE user_id = ? and poll_id = ?", ['closed', json_encode($q['settings']), $creator, $poll_id], "no");
				}
			}
			if (isset($q['settings']['opentime'])) {
				if (time() >= $q['settings']['opentime']) {
					$q['status'] = "open";
					unset($q['settings']['opentime']);
					db_query("UPDATE polls SET status = ?, settings = ? WHERE user_id = ? and poll_id = ?", ['open', json_encode($q['settings']), $creator, $poll_id], "no");
				}
			}
			if ($q['settings']['type'] !== "board") {
				$users = [];
				$chosenChoice = [];
				if ($q['choices']) {
					foreach ($q['choices'] as $optionableChoice => $keys) {
						$or = count($keys);
						$chosenChoice[$optionableChoice] = $or;
						$votes = $or + $votes;
						foreach ($keys as $id) {
							$users[$id] = true;
						}
					}
				}
			} else {
				$chosenChoice = $q['choices'];
				if ($q['choices']) {
					foreach ($q['choices'] as $user => $text) {
						$users[$user] = true;
					}
					$q['choices'] = count($users);
				} else {
					$users = [];
					$q['choices'] = 0;
				}
			}
			if (!count($users)) {
				$usercount = "0";
			} else {
				$usercount = count($users);
			}
			if ($q['settings']['admins']) {
				$admins = $q['settings']['admins'];
			} else {
				$admins = [];
			}
			$result = [
				"poll_id" => $poll_id, 
				"creator" => $creator,
				"title" => $q['title'],
				"description" => $q['description'], 
				"type" => $q['settings']['type'],
				"admins" => $admins,
				"usersvotes" => $q['choices'],
				"choice" => $chosenChoice,
				"choices" => $choicesArray,
				"votes" => $usercount,
				"status" => $q['status'],
				"settings" => $q['settings'],
				"anonymous" => $q['anonymous'],
				"messages" => json_decode($q['messages'], true)
			];
			return ['ok' => true, 'result' => $result];
		}
	}

	// Get poll data from database data
	function doPoll ($p = false) 
	{
		if (!$p) {
			call_error("<b>Error:</b> la variabile \$poll non è stata settata nella funzione doPoll");
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
		}
		$poll_id = $p['poll_id'];
		$creator = $p['user_id'];
		$p['choices'] = json_decode($p['choices'], true);
		if (is_array($p['choices'])) {
			$choicesArray = $p['choices'];
		} else {
			return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: poll choices must be an array"];
		}
		$p['settings'] = json_decode($p['settings'], true);
		if (!is_array($p['settings'])) {
			return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: poll settings must be an array"];
		}
		if (isset($p['settings']['closetime'])) {
			if (time() >= $p['settings']['closetime']) {
				$p['status'] = "closed";
				unset($p['settings']['closetime']);
				db_query("UPDATE polls SET status = ?, settings = ? WHERE user_id = ? and poll_id = ?", ['closed', json_encode($p['settings']), $creator, $poll_id], "no");
			}
		}
		if (isset($p['settings']['opentime'])) {
			if (time() >= $p['settings']['opentime']) {
				$p['status'] = "open";
				unset($p['settings']['opentime']);
				db_query("UPDATE polls SET status = ?, settings = ? WHERE user_id = ? and poll_id = ?", ['open', json_encode($p['settings']), $creator, $poll_id], "no");
			}
		}
		if ($p['settings']['type'] !== "board") {
			$users = [];
			$chosenChoice = [];
			if ($p['choices']) {
				foreach ($p['choices'] as $optionableChoice => $keys) {
					$or = count($keys);
					$chosenChoice[$optionableChoice] = $or;
					$votes = $or + $votes;
					foreach ($keys as $id) {
						$users[$id] = true;
					}
				}
			}
		} else {
			$chosenChoice = $p['choices'];
			if ($p['choices']) {
				foreach ($p['choices'] as $user => $text) {
					$users[$user] = true;
				}
				$p['choices'] = count($p['choices']);
			} else {
				$users = [];
				$p['choices'] = 0;
			}
		}
		if (!count($users)) {
			$usercount = "0";
		} else {
			$usercount = count($users);
		}
		if ($p['settings']['admins']) {
			$admins = $p['settings']['admins'];
		} else {
			$admins = [];
		}
		$result = [
			"poll_id" => $p['poll_id'], 
			"creator" => $p['user_id'],
			"title" => $p['title'],
			"description" => $p['description'], 
			"type" => $p['settings']['type'],
			"admins" => $admins,
			"usersvotes" => $p['choices'],
			"choice" => $chosenChoice,
			"choices" => $choicesArray,
			"votes" => $usercount,
			"status" => $p['status'],
			"settings" => $p['settings'],
			"anonymous" => $p['anonymous'],
			"messages" => json_decode($p['messages'], true)
		];
		return ['ok' => true, 'result' => $result];
	}

	// Get all poll
	function getAllPoll ($creator = false, $limit = 999)
	{
		if(!$creator) {
			call_error("<b>Error:</b> la variabile \$creator non è stata settata nella funzione getAllPoll");
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator was not found"];
		} elseif(!is_numeric($creator)) {
			call_error("<b>Error:</b> la variabile \$creator non è numerica nella funzione getAllPoll");
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator must be a numeric value"];
		} else {
			return db_query("SELECT * FROM polls WHERE user_id = ? and status != ? ORDER BY poll_id LIMIT ?", [$creator, 'deleted', $limit], false);
		}
	}

	// Get open poll
	function getActivePoll ($creator = false, $limit = 999) 
	{
		if(!$creator) {
			call_error("<b>Error:</b> la variabile \$creator non è stata settata nella funzione getActivePoll");
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator was not found"];
		} elseif(!is_numeric($creator)) {
			call_error("<b>Error:</b> la variabile \$creator non è numerica nella funzione getActivePoll");
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator must be a numeric value"];
		} else {
			return db_query("SELECT * FROM polls WHERE user_id = ? and status = ? ORDER BY poll_id LIMIT ?", [$creator, 'open', $limit], false);
		}
	}
	
	// Get total polls
	function getTotalPoll ($creator = false, $limit = 999) {
		if(!$creator) {
			call_error("<b>Error:</b> la variabile \$creator non è stata settata nella funzione getTotalPoll");
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator was not found"];
		} elseif(!is_numeric($creator)) {
			call_error("<b>Error:</b> la variabile \$creator non è numerica nella funzione getTotalPoll");
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator must be a numeric value"];
		} else {
			return db_query("SELECT * FROM polls WHERE user_id = ? ORDER BY poll_id LIMIT ?", [$creator, $limit], false);
		}
	}

	// Is Admin of poll
	function isAdmin ($user_id = false, $p = false) {
		if ($p === false) {
			call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione isAdmin");
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
		} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
			$p = sendPoll($p['poll_id'], $p['creator']);
			if ($p['ok']) {
				$p = $p['result'];
			} else {
				return $p;
			}
		} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
		} else {
			call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione isAdmin");
			return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$p is invalid"];
		}
		if (!isset($p['status']) ) {
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
		} elseif ($p['status'] == "deleted") {
			return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
		}
		if(!$user_id) {
			call_error("<b>Error:</b> la variabile \$user_id non è stata settata nella funzione isAdmin"); 
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$user_id was not found"];
		} elseif(!is_numeric($user_id)) {
			call_error("<b>Error:</b> la variabile \$user_id non è numerica nella funzione isAdmin"); 
			return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$user_id must be a numeric value"];
		} else {
			if (round($user_id) === round($p['creator'])) {
				return ['ok' => true, 'result' => true];
			} elseif ($p['settings']['admins'][$user_id]['status'] == "administrator") {
				return ['ok' => true, 'result' => true];
			} else {
				return ['ok' => true, 'result' => false];
			}
		}
	}

} else {
	$error = 502;
	done();
	die;
}

if ($access and !isset($error)) {
	
	if (!$methods[str_replace("/", '', $request)]) {
		$error = 405;
		done();
		die;
	}
	
	if ($access == 'limited' and !$userID and !$islocal) {
		if (!in_array(strtolower(str_replace('/', '', $request)), ['status'])) {
			$error = 401;
			$error_description = $config['errors'][$error] . ": Limited user can't use other methods";
			done();
			die;
		}
	}

	$time = time();
	$ip = $_SERVER['REMOTE_ADDR'];
	$method = strtolower(str_replace("/", '', $request));
	if ($config['limits'][$method]) {
		$ex = $redis->get($ip . $method);
		if ($ex >= time() and is_numeric($ex)) {
			header("Retry-After: " . date("r"));
			header("Retry-After: " . round($ex - time() + 1));
			$error = 429;
			$error_description = $config['errors'][$error] . ": retry after " . round($ex - time() + 1);
			done();
			log_api(['ok' => false, 'error_code' => $error, 'description' => $error_description], true);
			die;
		} elseif (!is_numeric($ex)) {
			$redis->set($ip . $method, $time + $config['limits'][$method]);
		} else {
			$redis->set($ip . $method, $time + $config['limits'][$method]);
		}
	}
	
	if ($request == "/status") {
		$ok = true;
		done();
	} elseif ($request == "/graph") {
		if (!$PDO) {
			try {
				$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
			} catch (PDOException $e) {
				$error = 500;
				$error_description = $config['errors'][$error] . ": database connection lost";
				done();
				die;
			}
		}
		if (isset($query['id'])) {
			if (is_numeric($query['id'])) {
				$poll_id = $query['id'];
				if (isset($query['owner_id'])) {
					if (is_numeric($query['owner_id'])) {
						if ($query['owner_id'] == $userID or $islocal) {
							$creator = $query['owner_id'];
						} else {
							$error = 401;
							$error_description = $config['errors'][$error] . ": owner_id must be yours";
							done();
							die;
						}
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": owner_id must be a numeric value";
						done();
						die;
					}
				} else {
					$creator = $query['owner_id'] = $userID;
				}
				$time = date("d-m-y-h-i");
				$p = sendPoll($poll_id, $creator);
				if (!$p['status']) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": poll not found";
					done();
					die;
				} elseif (!$p['status'] or $p['status'] == "deleted") {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll deleted";
					done();
					die;
				} elseif (!in_array($p['type'], ['vote', 'limited doodle', 'doodle', 'rating'])) {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll type not supported";
					done();
					die;
				} elseif (!$config['types'][$p['type']]) {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll type in maintenance";
					done();
					die;
				} else {
					$ute = db_query("SELECT * FROM utenti WHERE user_id = ?", [$creator], true);
					if ($ute['ok']) {
						$ute = $ute['result'];
					} else {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database error";
						done();
						die;
					}
					$ute['settings'] = json_decode($ute['settings'], true);
					require('/home/masterpoll-documents/lib/chart/jpgraph.php');
					require_once('/home/masterpoll-documents/lib/chart/jpgraph_iconplot.php');
					if ($query['type'] == "bar") {
						require('/home/masterpoll-documents/lib/chart/jpgraph_bar.php');
					} elseif ($query['type'] == "pie") {
						require('/home/masterpoll-documents/lib/chart/jpgraph_pie.php');
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": Type of chart not found";
						done();
						die;
					}
					if (!class_exists("Graph")) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": Graph class not work";
						done();
						die;
					}
					$totchoices = count($p['choice']);
					if ($query['type'] == "bar") {
						$graph = new Graph(800, 600, 'auto');
						$max = round(100 / $totchoices) - 1;
					} elseif ($query['type'] == "pie") {
						$graph = new PieGraph(800, 600, 'auto');
						$max = round(100 / 5) - 1;
					}
					$graph->SetScale("textlin");
					foreach ($p['choice'] as $key => $users) {
						if (is_numeric($users)) {
							$data1y[] = round($users);
						} else {
							$data1y[] = 0;
						}
						if (strlen($key) < $max) {
							$names[] = $key;
						} else {
							$names[] = substr($key, 0, $max - 1) . "...";
						}
					}
					$theme_class = new SoftyTheme;
					$graph->SetTheme($theme_class);
					if ($query['type'] == "bar") {
						$graph->SetBox(false);
						$graph->ygrid->SetFill(false);
						$graph->xaxis->SetTickLabels($names);
						$graph->yaxis->HideLine(false);
						$graph->yaxis->HideTicks(false, true);
						$b1plot = new BarPlot($data1y);
						$graph->Add($b1plot);
						$b1plot->value->SetFormat('%01.2f');
						$b1plot->SetColor("white");
						$b1plot->SetFillColor("#cc1111");
					} elseif ($query['type'] == "pie") {
						$p1 = new PiePlot(array_values($data1y));
						$p1->SetLegends($names);
						$graph->Add($p1);
					}
					if (strlen($p['title']) <= 75) {
						$graph->title->Set(substr($p['title'], 0, 75));
					} else {
						$graph->title->Set(substr($p['title'], 0, 75) . "...");
					}
					if ($ute['settings']['premium'] === false or !$ute['settings']['premium']) {
						$icon = new IconPlot("/home/masterpoll-documents/thumb_logo.jpeg", 10, 10, 0.2, 30);
						$graph->Add($icon);
					}
					$graph->stroke("cache-graph-$poll.jpeg");
					if (!file_exists("cache-graph-$poll.jpeg")) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": Image not created";
						done();
						die;
					}
					$args = ['file' => curl_file_create("cache-graph-$poll.jpeg", "image/jpeg")];
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "https://telegra.ph/upload");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
					$res = curl_exec($ch);
					curl_close($ch);
					$res = json_decode($res, true);
					if (isset($res[0]['src'])) {
						$ok = true;
						$link = "https://telegra.ph" . $res[0]['src'];
						$result = $link;
						done();
						fastcgi_finish_request();
						if (file_exists("cache-graph-$poll.jpeg")) {
							unlink("cache-graph-$poll.jpeg");
						}
					} else {
						$error = 500;
						$error_description = $config['errors'][$error] . ": Telegraph not work at the moment.";
					}
				}
			} else {
				$error = 400;
				$error_description = $config['errors'][$error] . ": id must be a numeric value";
			}
		} else {
			$error = 400;
			$error_description = $config['errors'][$error] . ": id is empty";
		}
	} elseif ($request == "/getsubscribers") {
		if (!$PDO) {
			try {
				$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
			} catch (PDOException $e) {
				$error = 500;
				$error_description = $config['errors'][$error] . ": database connection lost";
				done();
				die;
			}
		}
		$ok = true;
		$ex = $redis->get('subs-' . time());
		if ($ex) {
			$subs = round($ex);
		} else {
			$subs = round(count(db_query("SELECT lang FROM utenti", false, false)['result']));
			$redis->set('subs-' . time(), $subs);
			$redis->set('subs-' . time(), $subs + 1);
			$redis->set('subs-' . time(), $subs + 2);
		}
		$result = $subs;
	} elseif ($request == "/getactivepolls" and !$islocal) {
		if (!$PDO) {
			try {
				$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
			} catch (PDOException $e) {
				$error = 500;
				$error_description = $config['errors'][$error] . ": database connection lost";
				done();
				die;
			}
		}
		$polls = getActivePoll($userID, 50);
		if ($polls['ok']) {
			$polls = $polls['result'];
		} else {
			$error = 500;
			done();
			die;
		}
		$result = [];
		foreach ($polls as $poll) {
			$p = doPoll($poll);
			if ($p['ok']) {
				$p = $p['result'];
			} else {
				$error = 500;
				done();
				die;
			}
			if ($config['types'][$p['type']]) $result[] = pollToArray($p);
		}
		$ok = true;
	} elseif ($request == "/getallpolls" and !$islocal) {
		if (!$PDO) {
			try {
				$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
			} catch (PDOException $e) {
				$error = 500;
				$error_description = $config['errors'][$error] . ": database connection lost";
				done();
				die;
			}
		}
		$polls = getAllPoll($userID, 50);
		if ($polls['ok']) {
			$polls = $polls['result'];
		} else {
			$error = 500;
			done();
			die;
		}
		$result = [];
		foreach ($polls as $poll) {
			$p = doPoll($poll);
			if ($p['ok']) {
				$p = $p['result'];
			} else {
				$error = 500;
				done();
				die;
			}
			if ($config['types'][$p['type']]) $result[] = pollToArray($p);
		}
		$ok = true;
	} elseif ($request == "/getme" and !$islocal) {
		if (!$PDO) {
			try {
				$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
			} catch (PDOException $e) {
				$error = 500;
				$error_description = $config['errors'][$error] . ": database connection lost";
				done();
				die;
			}
		}
		$u = db_query("SELECT * FROM utenti WHERE user_id = ?", [$userID], true);
		if ($u['ok']) {
			$u = $u['result'];
		} else {
			$error = 500;
			$error_description = $config['errors'][$error] . ": database error";
			done();
			die;
		}
		$u['settings'] = json_decode($u['settings'], true);
		unset($u['page']);
		$result = [
			'first_name' => $u['nome'],
			'last_name' => $u['cognome'],
			'username' => $u['username'],
			'language_code' => $u['lang']
		];
		$ok = true;
	} elseif ($request == "/getpoll" and !$islocal) {
		if (isset($query['id'])) {
			if (is_numeric($query['id'])) {
				if (isset($query['owner_id'])) {
					if (is_numeric($query['owner_id'])) {
						$creator = $query['owner_id'];
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": owner_id must be a numeric value";
						done();
						die;
					}
				} else {
					$creator = $userID;
				}
				if (!$PDO) {
					try {
						$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
					} catch (PDOException $e) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database connection lost";
						done();
						die;
					}
				}
				$poll = sendPoll($query['id'], $creator);
				if ($poll['ok']) {
					$poll = $poll['result'];
				} else {
					$error = 500;
					done();
					die;
				}
				if (!$poll['status']) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": poll not found";
				} elseif ($poll['status'] == "deleted") {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll deleted";
				} elseif (!$poll['settings']['sharable'] and $creator !== $userID) {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll data must be shareable";
				} elseif (!$config['types'][$poll['type']]) {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll type in maintenance";
				} else {
					$ok = true;
					$result = pollToArray($poll);
				}
			} else {
				$error = 400;
				$error_description = $config['errors'][$error] . ": id must be a numeric value";
			}
		} else {
			$error = 400;
			$error_description = $config['errors'][$error] . ": id is empty";
		}
	} elseif ($request == "/deletepoll" and !$islocal) {
		if (isset($query['id'])) {
			if (is_numeric($query['id'])) {
				if (!$PDO) {
					try {
						$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
					} catch (PDOException $e) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database connection lost";
						done();
						die;
					}
				}
				$poll = sendPoll($query['id'], $userID);
				if ($poll['ok']) {
					$poll = $poll['result'];
				} else {
					$error = 500;
					done();
					die;
				}
				if (!$poll['status']) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": poll not found";
				} elseif ($poll['status'] == "deleted") {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll already deleted";
				} elseif (!$config['types'][$poll['type']]) {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll type in maintenance";
				} else {
					$ok = true;
					$result = true;
					cancelPoll($query['id'], $userID);
				}
			} else {
				$error = 400;
				$error_description = $config['errors'][$error] . ": id must be a numeric value";
			}
		} else {
			$error = 400;
			$error_description = $config['errors'][$error] . ": id is empty";
		}
	} elseif ($request == "/closepoll" and !$islocal) {
		if (isset($query['id'])) {
			if (is_numeric($query['id'])) {
				if (!$PDO) {
					try {
						$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
					} catch (PDOException $e) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database connection lost";
						done();
						die;
					}
				}
				$p = sendPoll($query['id'], $userID);
				if ($p['ok']) {
					$p = $p['result'];
				} else {
					$error = 500;
					done();
					die;
				}
				if (!$p['status']) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": poll not found";
				} elseif ($p['status'] == "deleted") {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll deleted";
				} elseif (!$config['types'][$p['type']]) {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll type in maintenance";
				} elseif ($p['status'] == 'open') {
					db_query("UPDATE polls SET status = ? WHERE user_id = ? and poll_id = ?", ['closed', $userID, $query['id']], 'no');
					$ok = true;
					$result = true;
				} else {
					$error = 400;
					$error_description = $config['errors'][$error] . ": the poll is closed";
				}
			} else {
				$error = 400;
				$error_description = $config['errors'][$error] . ": id must be a numeric value";
			}
		} else {
			$error = 400;
			$error_description = $config['errors'][$error] . ": id is empty";
		}
	} elseif ($request == "/openpoll" and !$islocal) {
		if (isset($query['id'])) {
			if (is_numeric($query['id'])) {
				if (!$PDO) {
					try {
						$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
					} catch (PDOException $e) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database connection lost";
						done();
						die;
					}
				}
				$p = sendPoll($query['id'], $userID);
				if ($p['ok']) {
					$p = $p['result'];
				} else {
					$error = 500;
					done();
					die;
				}
				if (!$p['status']) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": poll not found";
				} elseif ($p['status'] == "deleted") {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll deleted";
				} elseif (!$config['types'][$p['type']]) {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll type in maintenance";
				} elseif ($p['status'] == 'closed') {
					db_query("UPDATE polls SET status = ? WHERE user_id = ? and poll_id = ?", ['open', $userID, $query['id']], 'no');
					$ok = true;
					$result = true;
				} else {
					$error = 400;
					$error_description = $config['errors'][$error] . ": the poll is open";
				}
			} else {
				$error = 400;
				$error_description = $config['errors'][$error] . ": id must be a numeric value";
			}
		} else {
			$error = 400;
			$error_description = $config['errors'][$error] . ": id is empty";
		}
	} elseif ($request == "/editpoll" and !$islocal) {
		if (isset($query['id'])) {
			if (is_numeric($query['id'])) {
				if ($query['settings']) {
					$query['settings'] = json_decode($query['settings'], true);
					if (is_array($query['settings'])) {
						if (!$PDO) {
							try {
								$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
							} catch (PDOException $e) {
								$error = 500;
								$error_description = $config['errors'][$error] . ": database connection lost";
								done();
								die;
							}
						}
						$p = sendPoll($query['id'], $userID);
						if ($p['ok']) {
							$p = $p['result'];
						} else {
							$error = 500;
							done();
							die;
						}
						if (!$p['status']) {
							$error = 404;
							$error_description = $config['errors'][$error] . ": poll not found";
							done();
							die;
						} elseif ($p['status'] == "deleted") {
							$error = 400;
							$error_description = $config['errors'][$error] . ": poll deleted";
							done();
							die;
						} elseif (!$config['types'][$p['type']]) {
							$error = 400;
							$error_description = $config['errors'][$error] . ": poll type in maintenance";
							done();
							die;
						}
						$stringsetting = [
							"disable_web_page_preview",
							"disable_web_content_preview",
							"sort",
							"appendable",
							"notification",
							"votes_limit"
						];
						$newsettings = [];
						foreach (array_keys($query['settings']) as $string) {
							if (in_array($string, array_keys($stringsetting))) {
								if ($string == "disable_web_page_preview") {
									if (!is_bool($query['settings'][$string])) {
										$ok = false;
										$error = 400;
										$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
										done();
										die;
									}
									if ($query['settings']['disable_web_page_preview']) {
										$p['settings']['web_page'] = true;
										$newsettings['disable_web_page_preview'] = true;
									} else {
										unset($p['settings']['web_content']);
										unset($p['settings']['web_page']);
										$newsettings['disable_web_page_preview'] = false;
										if ($p['settings']['web_page'] or $newsettings['disable_web_content_preview']) $newsettings['disable_web_content_preview'] = false;
									}
								} elseif ($string == "disable_web_content_preview") {
									if (strlen($query['settings']['disable_web_content_preview']) > 1024) {
									} elseif ($query['settings']['disable_web_content_preview']) {
										if (!$p['settings']['web_page']) $newsettings['disable_web_page_preview'] = true;
										$newsettings['disable_web_content_preview'] = $query['settings']['disable_web_content_preview'];
										$p['settings']['web_page'] = true;
										$p['settings']['web_content'] = $query['settings']['disable_web_content_preview'];
									} else {
										unset($p['settings']['web_content']);
										$newsettings['disable_web_content_preview'] = false;
									}
								} elseif ($string == "sort") {
									if (!is_bool($query['settings'][$string])) {
										$ok = false;
										$error = 400;
										$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
										done();
										die;
									}
									if (!in_array($p['type'], ['vote', 'doodle', 'limited doodle'])) {
										
									} elseif ($query['settings']['sort']) {
										$p['settings']['sort'] = true;
										$newsettings['sort'] = true;
									} else {
										unset($p['settings']['sort']);
										$newsettings['sort'] = false;
									}
								} elseif ($string == "appendable") {
									if (!is_bool($query['settings'][$string])) {
										$ok = false;
										$error = 400;
										$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
										done();
										die;
									}
									if (!in_array($p['type'], ['vote', 'doodle', 'limited doodle'])) {
										
									} elseif ($query['settings']['appendable']) {
										$p['settings']['appendable'] = true;
										$newsettings['appendable'] = true;
									} else {
										unset($p['settings']['appendable']);
										$newsettings['appendable'] = false;
									}
								} elseif ($string == "notification") {
									if (!is_bool($query['settings'][$string])) {
										$ok = false;
										$error = 400;
										$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
										done();
										die;
									}
									if (!in_array($p['type'], ['vote', 'doodle', 'limited doodle', 'board'])) {
										
									} elseif ($query['settings']['notification']) {
										$p['settings']['notification'] = true;
										$newsettings['notification'] = true;
									} else {
										unset($p['settings']['notification']);
										$newsettings['notification'] = false;
									}
								} elseif ($string == "votes_limit") {
									if ($query['settings']['votes_limit'] or $query['settings']['votes_limit'] === 0) {
										if (!is_numeric($query['settings']['votes_limit']) and $query['settings']['votes_limit'] > 0) {
											$error = 400;
											$error_description = $config['errors'][$error] . ": votes_limit must be a numeric value";
											done();
											die;
										}
										if ($query['settings']['votes_limit'] === 0) {
											unset($p['settings']['max_voters']);
											$newsettings['votes_limit'] = false;
										} elseif ($query['settings']['votes_limit'] < $p['votes']) {
										} else {
											$p['settings']['max_voters'] = round($query['settings']['votes_limit']);
											$newsettings['votes_limit'] = round($query['settings']['votes_limit']);
										}
									} else {
										unset($p['settings']['max_voters']);
										$newsettings['votes_limit'] = false;
									}
								} else {

								}
							}
						}
						$ok = true;
						$result = $newsettings;
						done();
						fastcgi_finish_request();
						db_query("UPDATE polls SET settings = ? WHERE poll_id = ? and user_id = ?", [json_encode($p['settings']), $query['id'], $userID], 'no');
					} else {
						$ok = false;
						$error = 400;
						$error_description = $config['errors'][$error] . ": settings must be an array";
					}
				} else {
					$error = 400;
					$error_description = $config['errors'][$error] . ": settings is empty";
				}
			} else {
				$ok = false;
				$error = 400;
				$error_description = $config['errors'][$error] . ": id must be a numeric value";
			}
		} else {
			$ok = false;
			$error = 400;
			$error_description = $config['errors'][$error] . ": id is empty";
		}
	} elseif ($request == "/addvote" and !$islocal) {
		if (isset($query['id'])) {
			if (is_numeric($query['id'])) {
				if (isset($query['owner_id'])) {
					if (is_numeric($query['owner_id'])) {
						$creator = $query['owner_id'];
						if (!$PDO) {
							try {
								$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
							} catch (PDOException $e) {
								$error = 500;
								$error_description = $config['errors'][$error] . ": database connection lost";
								done();
								die;
							}
						}
						$p = sendPoll($query['id'], $creator);
						if ($p['ok']) {
							$p = $p['result'];
						} else {
							$error = 500;
							done();
							die;
						}
						if (!$p['status']) {
							$error = 404;
							$error_description = $config['errors'][$error] . ": poll not found";
							done();
							die;
						} elseif ($p['status'] == "closed") {
							$error = 401;
							$error_description = $config['errors'][$error] . ": poll closed";
							done();
							die;
						} elseif ($p['status'] == "deleted") {
							$error = 400;
							$error_description = $config['errors'][$error] . ": poll deleted";
							done();
							die;
						} elseif (!$config['types'][$p['type']]) {
							$error = 400;
							$error_description = $config['errors'][$error] . ": poll type in maintenance";
							done();
							die;
						}
						if (!isAdmin($userID, $p)['result']) {
							$banlist = db_query("SELECT banlist FROM utenti WHERE user_id = ?", [$creator], true);
							if ($banlist['ok']) {
								$banlist = $banlist['result'];
							} else {
								$error = 500;
								$error_description = $config['errors'][$error] . ": error to get the banlist";
								done();
								die;
							}
							$banlist = json_decode($banlist['banlist'], true);
							if (isset($banlist)) {
								if (isset($banlist[$userID])) {
									if (is_numeric($banlist[$userID])) {
										if ($banlist[$userID] <= time()) {
											unset($banlist[$userID]);
											db_query("UPDATE utenti SET banlist = ? WHERE user_id = ?", [json_encode($banlist), $creator], 'no');
										} else {
											$error = 401;
											$error_description = $config['errors'][$error] . ": user_id banned from owner_id";
											done();
											die;
										}
									} else {
										$error = 401;
										$error_description = $config['errors'][$error] . ": user_id banned from owner_id";
										done();
										die;
									}
								}
							}
						} else {
							$admin = true;
						}
						if (isset($p['settings']['max_voters'])) {
							if ($p['votes'] == $p['settings']['max_voters']) {
								$haveChoices = haveChoices($p, $userID);
								if ($haveChoices['ok']) {
									$haveChoices = $haveChoices['result'];
								} else {
									$error = 500;
									$error_description = $config['errors'][$error] . ": database error";
									done();
									die;
								}
								if ($haveChoices) {} else {
									$error = 400;
									$error_description = $config['errors'][$error] . ": poll full";
									done();
									die;
								}
							}
						}
						if ($p['type'] == "vote") {
							$r = haveChoices($p, $userID);
							if ($r['ok']) {
								$r = $r['result'];
							} else {
								$error = 500;
								$error_description = $config['errors'][$error] . ": database error";
								done();
							}
						} elseif ($p['type'] == "doodle") {
							
						} elseif ($p['type'] == "limited doodle") {
							
						} elseif ($p['type'] == "board") {
							
						} elseif ($p['type'] == "quiz") {
							$error = 400;
							$error_description = $config['errors'][$error] . ": poll type not allowed";
						} elseif ($p['type'] == "participation") {
							
						} elseif ($p['type'] == "rating") {
							
						}
						done();
						fastcgi_finish_request();
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": owner_id must be a numeric value";
						done();
						die;
					}
				} else {
					$error = 400;
					$error_description = $config['errors'][$error] . ": owner_id is empty";
					done();
					die;
				}
			} else {
				$ok = false;
				$error = 400;
				$error_description = $config['errors'][$error] . ": id must be a numeric value";
			}
		} else {
			$ok = false;
			$error = 400;
			$error_description = $config['errors'][$error] . ": id is empty";
		}
	} elseif ($request == "/createpoll" and !$islocal) {
		if (isset($query['title']) and isset($query['type']) and isset($query['privacy'])) {
			if (strlen($query['title']) <= 512 and strlen($query['description']) <= 512 and strlen($query['type']) <= 64 and strlen($query['privacy']) <= 32 and strlen($query['start_rating_range']) <= 1 and strlen($query['end_rating_range']) <= 2) {
				if (!in_array($query['type'], array_keys($config['types']))) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": type not found";
				} elseif (!$config['types'][$query['type']]) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": type in maintenance";
				} elseif (!in_array($query['privacy'], array_keys($config['poll_privacy']))) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": privacy not found";
				} else {
					if (in_array($query['type'], ["participation"]) and in_array($query['privacy'], ["anonymous"])) {
						$error = 400;
						$error_description = $config['errors'][$error] . ": privacy not supported for this type of poll";
						done();
						die;
					}
					if (!$PDO) {
						try {
							$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
						} catch (PDOException $e) {
							$error = 500;
							$error_description = $config['errors'][$error] . ": database connection lost";
							done();
							die;
						}
					}
					$poll = [
						'poll_id' => getID($userID),
						'creator' => $userID,
						'title' => $query['title'],
					];
					if (isset($query['description'])) $poll['description'] = $query['description'];
					$poll['settings'] = [];
					$poll['settings']['type'] = $query['type'];
					$poll['type'] = $poll['settings']['type'];
					$poll['votes'] = 0;
					$poll['anonymous'] = $config['poll_privacy'][$query['privacy']];
					if ($poll['type'] == "vote") {
						if ($query['choices']) {
							$query['choices'] = json_decode($query['choices'], true);
							if (is_array($query['choices'])) {
								if (!$query['choices']) {
									$error = 400;
									$error_description = $config['errors'][$error] . ": choices is empty";
									done();
									die;
								}
								foreach ($query['choices'] as $key => $value) {
									if (!$value and $value != "0") $value = $key;
									$poll['choice'][$value] = [];
								}
								$poll['usersvotes'] = $poll['choice'];
								$poll['choices'] = $poll['choice'];
								$chosenChoice = $poll['choices'];
								if ($poll['choices']) {
									foreach ($poll['choices'] as $user => $text) {
										$users[$user] = true;
									}
									$poll['choices'] = count($poll['choices']);
								} else {
									$users = [];
									$poll['choices'] = 0;
								}
							} else {
								$error = 400;
								$error_description = $config['errors'][$error] . ": choices must be an array";
								done();
								die;
							}
						} else {
							$error = 400;
							$error_description = $config['errors'][$error] . ": choices is empty";
							done();
							die;
						}
					} elseif ($poll['type'] == "doodle") {
						if ($query['choices']) {
							$query['choices'] = json_decode($query['choices'], true);
							if (is_array($query['choices'])) {
								if (!$query['choices']) {
									$error = 400;
									$error_description = $config['errors'][$error] . ": choices is empty";
									done();
									die;
								}
								foreach ($query['choices'] as $key => $value) {
									if (!$value and $value != "0") $value = $key;
									$poll['choice'][$value] = [];
								}
								$poll['usersvotes'] = $poll['choice'];
								$poll['choices'] = $poll['choice'];
								$chosenChoice = $poll['choices'];
								if ($poll['choices']) {
									foreach ($poll['choices'] as $user => $text) {
										$users[$user] = true;
									}
									$poll['choices'] = count($poll['choices']);
								} else {
									$users = [];
									$poll['choices'] = 0;
								}
							} else {
								$error = 400;
								$error_description = $config['errors'][$error] . ": choices must be an array";
								done();
								die;
							}
						} else {
							$error = 400;
							$error_description = $config['errors'][$error] . ": choices is empty";
							done();
							die;
						}
					} elseif ($poll['type'] == "limited doodle") {
						if ($query['choices']) {
							$query['choices'] = json_decode($query['choices'], true);
							if (is_array($query['choices'])) {
								if (!$query['choices']) {
									$error = 400;
									$error_description = $config['errors'][$error] . ": choices is empty";
									done();
									die;
								}
								foreach ($query['choices'] as $key => $value) {
									if (!$value and $value != "0") $value = $key;
									$poll['choice'][$value] = [];
								}
								$poll['usersvotes'] = $poll['choice'];
								$poll['choices'] = $poll['choice'];
								$chosenChoice = $poll['choices'];
								if ($poll['choices']) {
									foreach ($poll['choices'] as $user => $text) {
										$users[$user] = true;
									}
									$poll['choices'] = count($poll['choices']);
								} else {
									$users = [];
									$poll['choices'] = 0;
								}
							} else {
								$error = 400;
								$error_description = $config['errors'][$error] . ": choices must be an array";
								done();
								die;
							}
						} else {
							$error = 400;
							$error_description = $config['errors'][$error] . ": choices is empty";
							done();
							die;
						}
						if ($query['max_choices']) {
							if (is_numeric($query['max_choices'])) {
								$query['max_choices'] = round(str_replace("-", '', str_replace(".", '', $query['max_choices'])));
								if ($query['max_choices'] <= count(array_keys($poll['choice'])) and $query['max_choices'] >= 1) {
									$poll['settings']['max_choices'] = $query['max_choices'];
								} else {
									$error = 400;
									$error_description = $config['errors'][$error] . ": max_choices must be minus or equal to the count of choices";
									done();
									die;
								}
							} else {
								$error = 400;
								$error_description = $config['errors'][$error] . ": max_choices must be a numeric value";
								done();
								die;
							}
						} else {
							$error = 400;
							$error_description = $config['errors'][$error] . ": max_choices is empty";
							done();
							die;
						}
					} elseif ($poll['type'] == "board") {
						$poll['choice'] = [];
						$poll['choices'] = [];
						$poll['usersvotes'] = [];
					} elseif ($poll['type'] == "participation") {
						$poll['choice'] = ['participants' => []];
						$poll['choices'] = ['participants' => []];
						$poll['usersvotes'] = ['participants' => []];
					} elseif ($poll['type'] == "rating") {
						if (isset($query['start_rating_range']) and isset($query['end_rating_range'])) {
							if (is_numeric($query['start_rating_range']) and is_numeric($query['end_rating_range'])) {
								$query['start_rating_range'] = round(str_replace("-", '', str_replace(".", '', $query['start_rating_range'])));
								$query['end_rating_range'] = round(str_replace("-", '', str_replace(".", '', $query['end_rating_range'])));
								if ($query['start_rating_range'] > 0 and $query['start_rating_range'] <= 9 and $query['end_rating_range'] > $query['start_rating_range'] and $query['end_rating_range'] <= 10) {
									$range = range(round($query['start_rating_range']), round($query['end_rating_range']));
									foreach($range as $number) {
										$poll['choice'][$number] = [];
									}
									$poll['choices'] = $poll['choice'];
									$poll['usersvotes'] = $poll['choice'];
								} else {
									if ($query['start_rating_range'] > 1 and $query['start_rating_range'] <= 9) {} else {
										if (!$field) {
											$field = 'start_rating_range';
											$min = 1; $max = 9;
										}
									} 
									if ($query['end_rating_range'] > $query['start_rating_range'] and $query['end_rating_range'] <= 10) {} else {
										if (!$field) {
											$field = 'end_rating_range';
											$min = $query['start_rating_range']; $max = 10;
										}
									}
									$error = 400;
									$error_description = $config['errors'][$error] . ": $field must be plus $min and minus or equal to $max";
									done();
									die;
								}
							} else {
								$argsnull = ['start_rating_range', 'end_rating_range'];
								foreach($argsnull as $str) {
									if (!is_numeric($query[$str])) {
										if (!$field) $field = $str;
									}
								}
								$error = 400;
								$error_description = $config['errors'][$error] . ": $field must be a numeric value";
								done();
								die;
							}
						} else {
							$argsnull = ['start_rating_range', 'end_rating_range'];
							foreach($argsnull as $str) {
								if (!$query[$str]) {
									if (!$field) $field = $str;
								}
							}
							$error = 400;
							$error_description = $config['errors'][$error] . ": $field is empty";
							done();
							die;
						}
					} else {
						$error = 404;
						$error_description = $config['errors'][$error] . ": poll type not found";
						done();
						die;
					}
					if (isset($query['settings'])) {
						$query['settings'] = json_decode($query['settings'], true);
						if (is_array($query['settings'])) {
							$stringsetting = [
								"disable_web_page_preview",
								"disable_web_content_preview",
								"sort",
								"appendable",
								"notification",
								"votes_limit"
							];
							$newsettings = [];
							foreach (array_keys($query['settings']) as $string) {
								if (in_array($string, array_keys($stringsetting))) {
									if ($string == "disable_web_page_preview") {
										if (!is_bool($query['settings'][$string])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
											done();
											die;
										}
										if ($query['settings']['disable_web_page_preview']) {
											$poll['settings']['web_page'] = true;
											$newsettings['disable_web_page_preview'] = true;
										} else {
											unset($poll['settings']['web_content']);
											unset($poll['settings']['web_page']);
											$newsettings['disable_web_page_preview'] = false;
											if ($poll['settings']['web_page'] or $newsettings['disable_web_content_preview']) $newsettings['disable_web_content_preview'] = false;
										}
									} elseif ($string == "disable_web_content_preview") {
										if (strlen($query['settings']['disable_web_content_preview']) > 1024) {
										} elseif ($query['settings']['disable_web_content_preview']) {
											if (!$poll['settings']['web_page']) $newsettings['disable_web_page_preview'] = true;
											$newsettings['disable_web_content_preview'] = $query['settings']['disable_web_content_preview'];
											$poll['settings']['web_page'] = true;
											$poll['settings']['web_content'] = $query['settings']['disable_web_content_preview'];
										} else {
											unset($poll['settings']['web_content']);
											$newsettings['disable_web_content_preview'] = false;
										}
									} elseif ($string == "sort") {
										if (!is_bool($query['settings'][$string])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
											done();
											die;
										}
										if (!in_array($poll['type'], ['vote', 'doodle', 'limited doodle'])) {
											
										} elseif ($query['settings']['sort']) {
											$poll['settings']['sort'] = true;
											$newsettings['sort'] = true;
										} else {
											unset($poll['settings']['sort']);
											$newsettings['sort'] = false;
										}
									} elseif ($string == "appendable") {
										if (!is_bool($query['settings'][$string])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
											done();
											die;
										}
										if (!in_array($poll['type'], ['vote', 'doodle', 'limited doodle'])) {
											
										} elseif ($query['settings']['appendable']) {
											$poll['settings']['appendable'] = true;
											$newsettings['appendable'] = true;
										} else {
											unset($poll['settings']['appendable']);
											$newsettings['appendable'] = false;
										}
									} elseif ($string == "notification") {
										if (!is_bool($query['settings'][$string])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
											done();
											die;
										}
										if (!in_array($poll['type'], ['vote', 'doodle', 'limited doodle', 'board'])) {
											
										} elseif ($query['settings']['notification']) {
											$poll['settings']['notification'] = true;
											$newsettings['notification'] = true;
										} else {
											unset($poll['settings']['notification']);
											$newsettings['notification'] = false;
										}
									} elseif ($string == "votes_limit") {
										if ($query['settings']['votes_limit'] or $query['settings']['votes_limit'] === 0) {
											if (!is_numeric($query['settings']['votes_limit']) and $query['settings']['votes_limit'] > 0) {
												$error = 400;
												$error_description = $config['errors'][$error] . ": votes_limit must be a numeric value";
												done();
												die;
											}
											if ($query['settings']['votes_limit'] === 0) {
												unset($poll['settings']['max_voters']);
												$newsettings['votes_limit'] = false;
											} elseif ($query['settings']['votes_limit'] < $poll['votes']) {
											} else {
												$poll['settings']['max_voters'] = round($query['settings']['votes_limit']);
												$newsettings['votes_limit'] = round($query['settings']['votes_limit']);
											}
										} else {
											unset($poll['settings']['max_voters']);
											$newsettings['votes_limit'] = false;
										}
									}
								}
							}
						} else {
							$ok = false;
							$error = 400;
							$error_description = $config['errors'][$error] . ": settings must be an array";
							done();
							die;
						}
					}
					$poll['settings'] = json_encode($poll['settings']);
					$poll['choices'] = json_encode($poll['choice']);
					if (!$poll['description']) $poll['description'] = "";
					$q = db_query("INSERT INTO polls (poll_id, user_id, status, anonymous, settings, title, description, choices) VALUES (?,?,?,?,?,?,?,?)", [$poll['poll_id'], $poll['creator'], 'open', $poll['anonymous'], $poll['settings'], $poll['title'], $poll['description'], $poll['choices']], 'no');
					if (!$q['ok']) {
						$error = 500;
						$error_description = "Internal server error: database error";
						done();
						die;
					} else {
						$poll_id = $poll['poll_id'];
						unset($poll);
						$result = pollToArray(sendPoll($poll_id, $userID));
						$ok = true;
					}
				}
			} else {
				$argsnull = ['title' => 512, 'description' => 512, 'type' => 64, 'privacy' => 32, 'start_rating_range' => 1, 'end_rating_range' => 2];
				foreach($argsnull as $str => $limit) {
					if (strlen($query[$str]) >= $limit) {
						if (!$field) $field = $str;
					}
				}
				$error = 400;
				$error_description = $config['errors'][$error] . ": $field too long";
			}
		} else {
			$argsnull = ['title', 'type', 'privacy'];
			foreach($argsnull as $str) {
				if (!$query[$str]) {
					if (!$field) $field = $str;
				}
			}
			$error = 400;
			$error_description = $config['errors'][$error] . ": $field is empty";
		}
	} elseif ($islocal) {
		$error = 404;
		$error_description = $config['errors'][$error] . ": Unknow method";
	} else {
		$error = 404;
		$error_description = $config['errors'][$error] . ": Method not found";
	}
} else {
	if (!isset($error)) $error = 405;
}

done();
