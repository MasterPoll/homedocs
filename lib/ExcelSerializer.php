<?php

function doXLS ($array = [], $file_name = "test.xlsx") {
	ob_end_clean();
	ob_start();
	//header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8');
	//header('Content-Disposition: attachment; filename=' . $file_name);
	$array = doXLSArray($array);
	foreach ($array as $num => $c) {
		$content .= mb_convert_encoding($c, "UTF-8") . "\n";
	}
	echo "\xEF\xBB\xBF" . $content;
	$output = ob_get_contents();
	ob_end_clean();
	ob_start();
	return $output;
}

function doXLSArray($array = []) {
	$ar[] = "Title:;" . $array['title'];
	if (isset($array['description'])) {
		foreach(explode("\n", $array['description']) as $desc) {
			$num = $num + 1;
			if ($num == 1) {
				$p = "Description:";
			} else {
				$p = "";
			}
			$ar[] = "$p;" . $desc;
		}
		unset($num);
	}
	$ar[] = "Privacy:;" . $array['privacy'];
	if (in_array($array['type'], ['vote', 'doodle', 'limited doodle', 'rating'])) {
		if (is_array($array['choices'])) {
			$fig = [
				"start" => "┌",
				"line" => "┆",
				"list" => "├",
				"last" => "└"
			];
			foreach($array['choices'] as $choice => $users) {
				if (is_array($users)) {
					$ar[] = "$choice;" . count($users);
					foreach($users as $name) {
						$num = $num + 1;
						if ($num == count($users)) {
							$segn = $fig['last'];
						} else {
							$segn = $fig['list'];
						}
						if ($array['privacy'] == "personal") {
							$ok = $num . ";";
						}
						$ar[] = "$segn;$ok$name";
					}
					unset($num);
				} else {
					$ar[] = "$choice;No votes";
				}
			}
			
		} else {
			$ar[] = "Choices:;No choices.";
		}
	} elseif ($array['type'] == "participation") {
		if (is_array($array['participants'])) {
			foreach($array['participants'] as $name) {
				$num = $num + 1;
				if ($num == 1) {
					$p = "Participants:";
				} else {
					$p = "";
				}
				$ar[] = "$p;$num;" . $name;
			}
		} else {
			$ar[] = "Participants:;No participants.";
		}
	} elseif ($array['type'] == "board") {
		if (is_array($array['comments'])) {
			foreach($array['comments'] as $name => $comment) {
				$num = $num + 1;
				if ($num == 1) {
					$p = "Comments:";
				} else {
					$p = "";
				}
				if ($array['privacy'] == "anonymous") {
					$name = $comment;
					unset($comment);
				}
				$ar[] = "$p;$num;$name;$comment";
			}
			unset($num);
		} else {
			$ar[] = "Comments:;No comments.";
		}
	} else {
		$ar[] = "Type of poll not supported for Excel export";
	}
	return $ar;
}