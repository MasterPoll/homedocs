<?php

namespace Amp\Postgres;

use Amp\Sql\CommandResult;
use pq;
use pq\Result;

final class PqCommandResult implements CommandResult
{
    /** @var Result PostgreSQL result object. */
    private $result;

    /**
     * @param Result $result PostgreSQL result object.
     */
    public function __construct(Result $result)
    {
        $this->result = $result;
    }

    /**
     * @return int Number of rows affected by the INSERT, UPDATE, or DELETE query.
     */
    public function getAffectedRowCount(): int
    {
        return $this->result->affectedRows;
    }
}
