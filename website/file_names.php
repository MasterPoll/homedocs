<?php

# Files componenti del Sito
$f = [
	# Dominio principale
	'main' => [
		'index' => "/var/www/masterpoll/_index.php",
		'testing' => "/var/www/masterpoll/_testing.php",
		'thread' => "/var/www/masterpoll/_thread.php"
	],
	# Sotto dominio web
	'web' => [
		'index' => "/var/www/masterpoll-web/_index.php"
	],
	'config' => "/home/masterpoll-documents/site-config.php",
	# Database
	'database' => [
		'connection' => "/home/masterpoll-documents/website/databaseConnection.php"
	],
	# File di log per php
	'logs' => "/var/log/masterpoll/site.log",
	# Display error web
	'display_web_errors' => "/var/www/masterpoll/_errors.php"
];

?>