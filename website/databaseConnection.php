<?php
	error_reporting(0);
	if (!isset($_SESSION)) {
		include("/home/masterpoll-documents/website/file_names.php");
		$error = 401;
		if (substr(php_sapi_name(), 0, 3) == 'cgi') {
			header("Status: 401 Access denied");
		} else {
			header("HTTP/1.1 401 Access denied");
		}
		http_response_code(401);
		@require($f['display_web_errors']);
		die;
	}
	
	if ($config['site']['usa_il_db']) {
		if (strtolower($config['site']['database']['type']) == 'postgre') {
			try {
				$PDO = new PDO("pgsql:host=" . $config['site']['database']['host'] . ";dbname=" . $config['site']['database']['nome_database'], $config['site']['database']['utente'], $config['site']['database']['password']);
			} catch (PDOException $e) {
				if (!isset($f)) include("/home/masterpoll-documents/website/file_names.php");
				$error = 500;
				$error_description = "Database connection error...";
				if (substr(php_sapi_name(), 0, 3) == 'cgi') {
					header("Status: 500 Internal Server Error");
				} else {
					header("HTTP/1.1 500 Internal Server Error");
				}
				http_response_code($error);
				@require($f['display_web_errors']);
				die;
			}
		} else {
			if (!isset($f)) include("/home/masterpoll-documents/website/file_names.php");
			$error = 500;
			$error_description = "Database error...";
			if (substr(php_sapi_name(), 0, 3) == 'cgi') {
				header("Status: 500 Internal Server Error");
			} else {
				header("HTTP/1.1 500 Internal Server Error");
			}
			http_response_code($error);
			@require($f['display_web_errors']);
			die;
		}
	} else {
		if (!isset($f)) include("/home/masterpoll-documents/website/file_names.php");
		$error = 500;
		$error_description = "Database offline...";
		if (substr(php_sapi_name(), 0, 3) == 'cgi') {
			header("Status: 500 Internal Server Error");
		} else {
			header("HTTP/1.1 500 Internal Server Error");
		}
		http_response_code($error);
		@require($f['display_web_errors']);
		die;
	}

?>