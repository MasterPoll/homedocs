<?php
if (file_exists("/home/masterpoll-documents/site-config.php")) {
    ini_set('display_startup_errors', 0);
    ini_set('display_errors', 0);
    ini_set('error_reporting', E_ALL & ~E_NOTICE);
    ini_set('error_log', "../../log/masterpoll/thread.log");
    ini_set('ignore_repeated_errors', 1);
    ini_set('log_errors_max_len', 256);
    require("/home/masterpoll-documents/site-config.php");
    if ($_SESSION['thread']) {
        if ($config['site']['database']) {
            $config['database'] = $config['site']['database'];
            try {
                $PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'],
                    $config['database']['user'], $config['database']['password']);
            } catch (PDOException $e) {
				echo 500;
                die;
            }
            function db_query($query, $prepare = false, $fetch = false) {
				global $PDO;
				if (!$PDO) {
					return false;
				}
				$q = $PDO->prepare($query);
				if (is_array($prepare)) {
					$q->execute($prepare);
					$exec = true;
				} else {
					$q->execute();
					$exec = false;
				}
				$err = $q->errorInfo();
				if ($err[0] !== "00000") {
					$rr = $err;
				} elseif ($fetch) {
					$rr = $q->fetch(\PDO::FETCH_ASSOC);
				} elseif ($fetch == "no") {
					$rr = true;
				} else {
					$rr = $q->fetchAll();
				}
				return $rr;
			}
        	function getName($userID) {
				global $PDO;
				global $redis;
				if (!$PDO) {
					call_error("#PDOError \nQuery: " . code($query) . " \nDatabase non avviato.");
					return false;
				}
				if ($redis) {
					$nomer = $redis->get("name$userID");
					if ($nomer) return $nomer;
				}
				$query = "SELECT * FROM utenti WHERE user_id = $userID";
				$q = $PDO->prepare($query);
				$q->execute();
				$err = $PDO->errorInfo();
				if ($err[0] !== "00000") {
					call_error("PDO Error\n<b>INPUT:</> " . code($q) . "\n<b>OUTPUT:</> " . code(json_encode($err)));
					$rr = $err;
				} else {
					$u = $q->fetchAll()[0];
					if ($u['cognome']) $u['nome'] .= " " . $u['cognome'];
					if (isset($u['nome'])) {
						$rr = htmlspecialchars($u['nome']);
					} else {
						$rr = "Deleted account";
					}
				}
				if ($redis) {
					$redis->set("name$userID", $rr);
				}
				return $rr;
			}
		} else {
            echo 500;
			die;
        }
        if ($config['site']['class_work']) {
            function bot_decode($string) {
                global $config;
                $key = hash('sha256', $config['site']['secret_key']);
                $iv = substr(hash('sha256', $config['site']['secret_iv']), 0, 8);
                $datas = openssl_decrypt(base64_decode($string), $config['site']['encrypt_method'], $key, 0, $iv);
                return $datas;
            }
            function bot_encode($datas) {
                global $config;
                if (is_array($datas)) {
                    $datas = json_encode($datas);
                }
                $key = hash('sha256', $config['site']['secret_key']);
                $iv = substr(hash('sha256', $config['site']['secret_iv']), 0, 8);
                $string = str_replace('=', '', base64_encode(openssl_encrypt($datas, $config['site']['encrypt_method'], $key, 0, $iv)));
                return $string;
            }
            if (explode("_", $_SERVER['LANG'])[0]) {
                $lang = explode("-", explode("_", $_SERVER['LANG'])[0])[0];
            } else {
                $lang = "en";
            }
			function getLanguage($userID = false) {
				if (!$userID) return false;
				return db_query("SELECT lang FROM utenti WHERE user_id = ? LIMIT 1", [$userID], true)['lang'];
			}
            $linguebot = json_decode(file_get_contents("/home/masterpoll-documents/languages_beta.json"), true);
			function getTranslate($testo = 'start', $arr = [], $langp = 'def') {
				global $config;
				global $lang;
				global $linguebot;
				global $traduttore;
				if (!is_array($linguebot)) {
					die;
				}
				if ($langp == 'def') {
					$langp = $lang;
				}
				$testo = str_replace(' ', '', $testo);
				if (isset($linguebot[$langp][$testo])) {
					$sas = $linguebot[$langp][$testo];
				} elseif (isset($linguebot['en'][$testo])) {
					$sas = $linguebot['en'][$testo];
				} else {
					$sas = "🤖";
				}
				if (is_array($arr) and $arr) {
					$arr = array_values($arr);
					$e[0] = htmlspecialchars($arr[0]);
					$sas = str_replace("[0]", $e[0], $sas);
					$range = range(1, 10);
					foreach($range as $num) {
						if (isset($arr[$num])) {
							$e[$num] = htmlspecialchars($arr[$num]);
							$sas = str_replace("[$num]", $e[$num], $sas);
						}
					}
				}
				return mb_convert_encoding($sas, "UTF-8");
			}
            function sendPoll($poll_id = false, $creator = false) {
				if(!$poll_id) { 
					return false;
				} elseif(!$creator) { 
					return false;
				} else {
					$ce = db_query("SELECT * FROM polls WHERE user_id = ? and poll_id = ? LIMIT 1", [round($creator), round($poll_id)], true);
					$ce['choices'] = json_decode($ce['choices'], true);
					if (is_array($ce['choices'])) {
						$choicesArray = $ce['choices'];
					} else {
						$choicesArray = ['error' => []];
					}
					$ce['settings'] = json_decode($ce['settings'], true);
					if (isset($ce['settings']['closetime'])) {
						if (time() >= $ce['settings']['closetime']) {
							$ce['status'] = "closed";
							unset($ce['settings']['closetime']);
							db_query("UPDATE polls SET status = ?, settings = ? WHERE user_id = ? and poll_id = ?", ['closed', json_encode($ce['settings']), $creator, $poll_id], "no");
						}
					}
					if (isset($ce['settings']['opentime'])) {
						if (time() >= $ce['settings']['opentime']) {
							$ce['status'] = "open";
							unset($ce['settings']['opentime']);
							db_query("UPDATE polls SET status = ?, settings = ? WHERE user_id = ? and poll_id = ?", ['open', json_encode($ce['settings']), $creator, $poll_id], "no");
						}
					}
					if ($ce['settings']['type'] !== "board") {
						$users = [];
						$chosenChoice = [];
						if ($ce['choices']) {
							foreach ($ce['choices'] as $optionableChoice => $keys) {
								$or = count($keys);
								$chosenChoice[$optionableChoice] = $or;
								$votes = $or + $votes;
								foreach ($keys as $id) {
									$users[$id] = true;
								}
							}
						}
					} else {
						$chosenChoice = $ce['choices'];
						if ($ce['choices']) {
							foreach ($ce['choices'] as $user => $text) {
								$users[$user] = true;
							}
							$ce['choices'] = count($users);
						} else {
							$users = [];
							$ce['choices'] = 0;
						}
					}
					if (!count($users)) {
						$usercount = "0";
					} else {
						$usercount = count($users);
					}
					if ($ce['settings']['admins']) {
						$admins = $ce['settings']['admins'];
					} else {
						$admins = [];
					}
					$result = [
						"poll_id" => $poll_id, 
						"creator" => $creator,
						"title" => $ce['title'],
						"description" => $ce['description'], 
						"type" => $ce['settings']['type'],
						"admins" => $admins,
						"usersvotes" => $ce['choices'],
						"choice" => $chosenChoice,
						"choices" => $choicesArray,
						"votes" => $usercount,
						"status" => $ce['status'],
						"settings" => $ce['settings'],
						"anonymous" => $ce['anonymous'],
						"messages" => json_decode($ce['messages'], true)
					];
					return $result;
				}
			}
            function bars ($p = 0, $type = 0) {
				if ($p == "NAN") $p = 0;
				if ($type == "like") {
					if ($p <= 10) {
						return "👍";
					} elseif ($p >=11 && $p <= 20) {
						return "👍👍";
					} elseif ($p >= 21 && $p <= 30) {
						return "👍👍👍";
					} elseif ($p >= 31 && $p <= 40) {
						return "👍👍👍👍";
					} elseif ($p >= 41 && $p <= 50) {
						return "👍👍👍👍👍";
					} elseif ($p >= 51 && $p <= 60) {
						return "👍👍👍👍👍👍";
					} elseif ($p >= 61 && $p <= 70) {
						return "👍👍👍👍👍👍👍";
					} elseif ($p >= 71 && $p <= 80) {
						return "👍👍👍👍👍👍👍👍";
					} elseif ($p >= 81 && $p <= 90) {
						return "👍👍👍👍👍👍👍👍👍";
					} elseif ($p >= 91 && $p <= 100) {
						return "👍👍👍👍👍👍👍👍👍👍";
					} else {
						return "👍👍👍👍👍👍👍👍👍👍";
					}
				}
				if ($type == "quad") {
					if ($p == 0) {
						return "⚪⚪⚪⚪⚪⚪⚪⚪⚪⚪";
					} elseif ($p <= 10) {
						return "⚫⚪⚪⚪⚪⚪⚪⚪⚪⚪";
					} elseif ($p >=11 && $p <= 20) {
						return "⚫⚫⚪⚪⚪⚪⚪⚪⚪⚪";
					} elseif ($p >= 21 && $p <= 30) {
						return "⚫⚫⚫⚪⚪⚪⚪⚪⚪⚪";
					} elseif ($p >= 31 && $p <= 40) {
						return "⚫⚫⚫⚫⚪⚪⚪⚪⚪⚪";
					} elseif ($p >= 41 && $p <= 50) {
						return "⚫⚫⚫⚫⚫⚪⚪⚪⚪⚪";
					} elseif ($p >= 51 && $p <= 60) {
						return "⚫⚫⚫⚫⚫⚫⚪⚪⚪⚪";
					} elseif ($p >= 61 && $p <= 70) {
						return "⚫⚫⚫⚫⚫⚫⚫⚪⚪⚪";
					} elseif ($p >= 71 && $p <= 80) {
						return "⚫⚫⚫⚫⚫⚫⚫⚫⚪⚪";
					} elseif ($p >= 81 && $p <= 90) {
						return "⚫⚫⚫⚫⚫⚫⚫⚫⚫⚪";
					} elseif ($p >= 91 && $p <= 100) {
						return "⚫⚫⚫⚫⚫⚫⚫⚫⚫⚫";
					} else {
						return "⚫⚫⚫⚫⚫⚫⚫⚫⚫⚫";
					}
				}
				if ($type == "dot") {
					if ($p == 0) {
						return "○○○○○○○○○○";
					} elseif ($p <= 10) {
						return "●○○○○○○○○○";
					} elseif ($p >=11 && $p <= 20) {
						return "●●○○○○○○○○";
					} elseif ($p >= 21 && $p <= 30) {
						return "●●●○○○○○○○";
					} elseif ($p >= 31 && $p <= 40) {
						return "●●●●○○○○○○";
					} elseif ($p >= 41 && $p <= 50) {
						return "●●●●●○○○○○";
					} elseif ($p >= 51 && $p <= 60) {
						return "●●●●●●○○○○";
					} elseif ($p >= 61 && $p <= 70) {
						return "●●●●●●●○○○";
					} elseif ($p >= 71 && $p <= 80) {
						return "●●●●●●●●○○";
					} elseif ($p >= 81 && $p <= 90) {
						return "●●●●●●●●●○";
					} elseif ($p >= 91 && $p <= 100) {
						return "●●●●●●●●●●";
					} else {
						return "●●●●●●●●●●";
					}
				}
				return "❌";
			}
            function nn($n = 0) {
                if ($n == "NAN") {
                    $n = "0";
                }
                $n = round($n);
                if ($n === 'NAN') {
                    $n = "0";
                }
                if (is_numeric($n)) {
                    return explode(".", $n)[0];
                } else {
                    $n = "0";
                }
                return $n;
            }
			function maiuscolo($text) {
				if (strpos($text, " ") === false) {
					$text[0] = strtoupper($text[0]);
				} else {
					$spazi = explode(" ", $text);
					$text = "";
					foreach($spazi as $testo) {
						if ($testo !== $spazi[0]) $text .= " ";
						$testo[0] = strtoupper($testo[0]);
						$text .= $testo;
					}
				}
				return $text;
			}
            function getPollPercentage($poll_id = false, $creator = false) {
                if (!$poll_id) {
                    return false;
                } elseif (!$creator) {
                    return false;
                } else {
                    $c = db_query("SELECT * FROM polls WHERE user_id = ? and poll_id = ?", [$creator, $poll_id], true);
                    foreach (json_decode($c['choices'], true) as $choice => $keys) {
                        $total = count($keys) + $total;
                    }
                    $result = ["poll_id" => $poll_id, "creator" => $creator, "total" => $total];
                    foreach (json_decode($c['choices'], true) as $choice => $keys) {
                        $choiceCount = count($keys);
                        if ($total === 0) {
                            $n = 0;
                        } else {
                            $n = nn($choiceCount * 100 / $total);
                        }
                        $result[0][$choice] = nn($n);
                    }
                    return $result;
                }
            }
            function sorta($choices = []) {
                if (is_array($choices)) {
                    if (count($choices) !== 1) {
                        foreach ($choices as $choice => $value) {
                            $array[$value][] = $choice;
                        }
                        foreach ($array as $value => $choicess) {
                            sort($choicess, SORT_STRING);
                            $array[$value] = $choicess;
                        }
                        ksort($array, SORT_NUMERIC);
                        $array = array_reverse($array, true);
                        foreach ($array as $value => $choices) {
                            foreach ($choices as $choice) {
                                $newarray[$choice] = $value;
                            }
                        }
                        return $newarray;
                    }
                }
                return false;
            }
            function pollToHTML($poll = false, $admin = false) {
                global $config;
                global $isadmin;
                $menu = false;
                if (!isset($poll)) {
                    cell_error("<b>Warning:</b> la variabile \$poll non � stata settata nella funzione pollToMessage");
                    return ['ok' => false, 'text' => "Bot error..."];
                }
                if (!is_array($poll)) {
                    cell_error("<b>Warning:</b> la variabile \$poll non � un array nella funzione pollToMessage");
                    return ['ok' => false, 'text' => "Bot error..."];
                }
                $r = "<title>" . $poll['title'] . "</title>";
                if (isset($poll['settings']['web_content'])) {
                    $allegat = 0;
                    $r .= "<a href='" . $poll['settings']['web_content'] . "'><img src=\"" . $poll['settings']['web_content'] . "\" alt=\"" . getTranslate('webContentPreview',['']) . "\"></a><br>";
                } elseif (isset($poll['settings']['web_page'])) {
                    $allegat = $poll['settings']['web_page'];
                } else {
                    $allegat = $config['disabilita_anteprima_link'];
                }
                $poll_id = round($poll['poll_id']);
                $creator = round($poll['creator']);
                if (isset($poll['settings']['language'])) {
                    $lang = $poll['settings']['language'];
                } else {
                    $lang = getLanguage($creator);
                }
                if ($poll['status'] == "deleted") {
                    $r = getTranslate('voteDoesntExist', null, $lang);
                    return ['ok' => false, 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
                } elseif (!$poll['title']) {
                    cell_error("<b>Warning:</b> il sondaggio $poll_id-$creator non � stato trovato nella funzione pollToMessage");
                    $r = getTranslate('voteDoesntExist', null, $lang);
                    return ['ok' => false, 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
                } elseif ($poll['status'] == "closed") {
                    if ($poll['type'] == "board") {
                        if ($admin === 'moderate') {
                            $r .= "<h1>? " . bold($poll['title']) . "</h1>\n";
                        } else {
                            $r .= "<h1>?? " . bold($poll['title']) . "</h1>\n";
                        }
                        if ($poll['description']) {
                            $r .= htmlspecialchars($poll['description']) . "\n";
                        }
                        if ($poll['choice']) {
                            if ($admin) {
                                foreach ($poll['choice'] as $user_id => $text) {
                                    if ($poll['anonymous']) {
                                        $name = "??";
                                    } else {
                                        $name = htmlspecialchars_decode(getName($user_id)) . ":";
                                    }
                                    $r .= "\n" . bold($name) . htmlspecialchars(" $text \n");
                                    if ($admin === 'moderate') {
                                        $r .= "/delete_" . str_replace('=', '',
                                                bot_encode("$poll_id-$creator-$user_id")) . "\n";
                                    }
                                }
                            } elseif ($poll['settings']['hide_voters'] == false) {
                                foreach ($poll['choice'] as $user_id => $text) {
                                    if ($poll['anonymous']) {
                                        $name = "??";
                                    } else {
                                        $name = htmlspecialchars_decode(getName($user_id)) . ":";
                                    }
                                    $r .= "\n" . bold($name) . htmlspecialchars(" $text \n");
                                }
                            }
                        }
                        if ($admin === 'moderate') {
                            $menu[] = [
                                [
                                    "text" => "?? " . getTranslate('done', null, $lang),
                                    "callback_data" => "/option $poll_id-$creator"
                                ]
                            ];
                        } elseif ($admin) {
                            $menu[0] = [
                                [
                                    "text" => getTranslate('commPageOptions', null, $lang),
                                    "callback_data" => "/option $poll_id-$creator"
                                ],
                                [
                                    "text" => getTranslate('commPageReopen', null, $lang),
                                    "callback_data" => "/open $poll_id-$creator"
                                ],
                                [
                                    "text" => getTranslate('commPageDelete', null, $lang),
                                    "callback_data" => "/delete $poll_id-$creator"
                                ]
                            ];
                        }
                        if ($poll['votes'] === 0) {
                            $r .= "\n" . getTranslate('rendererZeroVotedSoFar', [$poll['votes']], $lang);
                        } elseif ($poll['votes'] === 1) {
                            $r .= "\n" . getTranslate('rendererSingleVotedSoFar', [$poll['votes']], $lang);
                        } else {
                            $r .= "\n" . getTranslate('rendererMultiVotedSoFar', [$poll['votes']], $lang);
                        }
                        if ($poll['anonymous']) {
                            $r .= "\n?? " . italic(getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['type']),
                                    null, $lang));
                        } else {
                            $r .= "\n?? " . italic(getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['type']),
                                    null, $lang));
                        }
                    } elseif ($poll['type'] == "participation") {
                        if ($admin === 'moderate') {
                            return "??";
                        }
                        $r .= "<h1>?? " . bold($poll['title']) . "</h1>\n";
                        if ($poll['description']) {
                            $r .= htmlspecialchars($poll['description']) . "\n";
                        }
                        if ($admin) {
                            $menu[0] = [
                                [
                                    "text" => getTranslate('commPageOptions', null, $lang),
                                    "callback_data" => "/option $poll_id-$creator"
                                ],
                                [
                                    "text" => getTranslate('commPageReopen', null, $lang),
                                    "callback_data" => "/open $poll_id-$creator"
                                ],
                                [
                                    "text" => getTranslate('commPageDelete', null, $lang),
                                    "callback_data" => "/delete $poll_id-$creator"
                                ]
                            ];
                        }
                        if ($poll['votes'] === 0) {
                            $r .= "\n" . getTranslate('rendererZeroVotedSoFar', [$poll['votes']], $lang);
                        } elseif ($poll['votes'] === 1) {
                            $r .= "\n" . getTranslate('rendererSingleVotedSoFar', [$poll['votes']], $lang);
                        } else {
                            $r .= "\n" . getTranslate('rendererMultiVotedSoFar', [$poll['votes']], $lang);
                        }
                        if ($poll['anonymous']) {
                            $r .= "\n?? " . italic(getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['type']),
                                    null, $lang));
                        } else {
                            $r .= "\n?? " . italic(getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['type']),
                                    null, $lang));
                        }
                    } elseif ($poll['type'] == "rating") {
                        if ($admin === 'moderate') {
                            return "??";
                        }
                        $r .= "?? " . bold($poll['title']) . "\n";
                        if ($poll['description']) {
                            $r .= htmlspecialchars($poll['description']) . "\n";
                        }
                        $percentage = getPollPercentage($poll_id, $creator);
                        foreach ($poll['choice'] as $key => $value) {
                            if ($value == "NAN") {
                                $value = 0;
                            }
                            if (!isset($min_val)) {
                                $min_val = $key;
                            }
                            $all = $all + ($key * $value);
                            $emoji = "??";
                            if (isset($numero)) {
                                $numero = $numero + 1;
                            } else {
                                $numero = 0;
                            }
                            if (!$poll['anonymous'] and $admin !== false) {
                                $fig = [
                                    "start" => "+",
                                    "line" => "?",
                                    "list" => "+",
                                    "last" => "+"
                                ];
                                if ($poll['choice'][$key]) {
                                    $r .= "\n" . $fig['start'] . " " . bold($key) . " [" . $value . "]\n";
                                } else {
                                    $r .= "\n" . bold($key) . " [" . $value . "]\n";
                                }
                            }
                            if ($admin) {
                                if ($poll['settings']['bars'] == "dot" and $poll['usersvotes'][$key]) {
                                    if (!$poll['anonymous']) {
                                        $r .= $fig['line'];
                                    }
                                    $r .= bars($percentage[0][$key], "dot") . " (" . nn($percentage[0][$key]) . "%) \n";
                                } elseif ($poll['settings']['bars'] == "like" and $value !== 0) {
                                    if (!$poll['anonymous']) {
                                        $r .= $fig['line'];
                                    }
                                    $r .= bars($percentage[0][$key],
                                            "like") . " (" . nn($percentage[0][$key]) . "%) \n";
                                }
                            }
                            if (!$poll['anonymous'] and $admin !== false) {
                                $ids = array_values($poll['usersvotes'][$key]);
                                if (count($ids) === 1) {
                                    $r .= $fig['last'] . " " . getName($ids[0]) . "\n";
                                } elseif (count($ids) === 0) {
                                } else {
                                    unset($die);
                                    unset($nums);
                                    $nums = range(0, count($ids) - 1);
                                    foreach ($nums as $num) {
                                        $id = $ids[$num];
                                        $name = getName($id);
                                        if ($die) {
                                            $r .= $fig['last'] . " " . $name . "\n";
                                        } else {
                                            $r .= $fig['list'] . " " . $name . "\n";
                                            if ($id == $ids[count($ids) - 2]) {
                                                $die = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        unset($numero);
                        if ($poll['votes']) {
                            $val = $all / $poll['votes'] / count($poll['choice']) * 5;
                            if ($val == 'NAN') {
                                $val = 0;
                            }
                            foreach (range($min_val, explode(".", $val)[0]) as $n) {
                                $stars .= $emoji;
                            }
                            $r .= "\n" . $stars . " [" . round($all / $poll['votes'], 1) . "] \n";
                        }
                        if ($admin) {
                            $menu[] = [
                                [
                                    "text" => getTranslate('commPageOptions', null, $lang),
                                    "callback_data" => "/option $poll_id-$creator"
                                ],
                                [
                                    "text" => getTranslate('commPageReopen', null, $lang),
                                    "callback_data" => "/open $poll_id-$creator"
                                ],
                                [
                                    "text" => getTranslate('commPageDelete', null, $lang),
                                    "callback_data" => "/delete $poll_id-$creator"
                                ]
                            ];
                        } else {
                            foreach ($percentage[0] as $key => $value) {
                                if (isset($num)) {
                                    if (count($menu[$num]) >= 5) {
                                        $num = $num + 1;
                                    }
                                } else {
                                    $num = 0;
                                }
                                if (isset($numero)) {
                                    $numero = $numero + 1;
                                } else {
                                    $numero = 0;
                                }
                                if ($value == "NAN") {
                                    $value = 0;
                                }
                            }
                        }
                        if ($poll['votes'] === 0) {
                            $r .= "\n" . getTranslate('rendererZeroVotedSoFar', [$poll['votes']], $lang);
                        } elseif ($poll['votes'] === 1) {
                            $r .= "\n" . getTranslate('rendererSingleVotedSoFar', [$poll['votes']], $lang);
                        } else {
                            $r .= "\n" . getTranslate('rendererMultiVotedSoFar', [$poll['votes']], $lang);
                        }
                        if ($poll['anonymous']) {
                            $r .= "\n?? " . italic(getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['type']),
                                    null, $lang));
                        } else {
                            $r .= "\n?? " . italic(getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['type']),
                                    null, $lang));
                        }
                    } else {
                        if ($admin === 'moderate') {
                            $r .= "<h1>? " . bold($poll['title']) . "</h1>\n";
                        } else {
                            $r .= "<h1>?? " . bold($poll['title']) . "</h1>\n";
                        }
                        if ($poll['description']) {
                            $r .= htmlspecialchars($poll['description']) . "\n";
                        }
                        $percentage = getPollPercentage($poll_id, $creator);
                        if (count($poll['choice']) === 1) {
                            unset($poll['settings']['bars']);
                        } else {
                            if ($poll['settings']['sort'] and $admin !== 'moderate') {
                                $poll['choice'] = sorta($poll['choice']);
                            }
                        }
                        foreach ($poll['choice'] as $key => $value) {
                            if ($value == "NAN") {
                                $value = 0;
                            }
                            if (isset($numero)) {
                                $numero = $numero + 1;
                            } else {
                                $numero = 0;
                            }
                            if (!$poll['anonymous'] and $admin !== 'moderate' and $poll['usersvotes'][$key]) {
                                if ($poll['settings']['group'] == 1) {
                                    $fig = [
                                        "start" => "+",
                                        "list" => "+",
                                        "last" => "+"
                                    ];
                                } elseif ($poll['settings']['group'] == 2) {
                                    $fig = [
                                        "start" => "+",
                                        "list" => "�",
                                        "last" => "+"
                                    ];
                                } elseif ($poll['settings']['group'] == "no") {
                                    $fig = [
                                        "start" => "",
                                        "list" => "-",
                                        "last" => "-"
                                    ];
                                } else {
                                    $fig = [
                                        "start" => "+",
                                        "list" => "+",
                                        "last" => "+"
                                    ];
                                }
                                $r .= "\n" . $fig['start'] . " " . bold($key) . " [" . $value . "]\n";
                            } else {
                                $r .= "\n" . bold($key) . " [" . $value . "]\n";
                            }
                            if ($poll['settings']['bars'] == "dot" and $admin !== 'moderate' and $poll['usersvotes'][$key]) {
                                if (!$poll['anonymous'] and $admin !== 'moderate') {
                                    $r .= "?";
                                }
                                $r .= bars($percentage[0][$key], "dot") . " (" . nn($percentage[0][$key]) . "%) \n";
                            } elseif ($poll['settings']['bars'] == "like" and $value !== 0) {
                                if (!$poll['anonymous'] and $admin !== 'moderate') {
                                    $r .= "?";
                                }
                                $r .= bars($percentage[0][$key], "like") . " (" . nn($percentage[0][$key]) . "%) \n";
                                if ($admin === 'moderate') {
                                    $r .= "/delete_" . str_replace('=', '',
                                            bot_encode("$poll_id-$creator-$numero")) . "\n";
                                }
                            } else {
                                if ($admin === 'moderate') {
                                    $r .= "/delete_" . str_replace('=', '',
                                            bot_encode("$poll_id-$creator-$numero")) . "\n";
                                }
                            }
                            if (!$poll['anonymous'] and $poll['usersvotes'][$key]) {
                                if ($admin === 'moderate') {
                                } elseif ($poll['settings']['hide_voters'] and !$admin) {
                                } else {
                                    $ids = array_values($poll['usersvotes'][$key]);
                                    if (count($ids) === 1) {
                                        $r .= $fig['last'] . " " . getName($ids[0]) . "\n";
                                    } else {
                                        unset($die);
                                        unset($nums);
                                        $nums = range(0, count($ids) - 1);
                                        foreach ($nums as $num) {
                                            $id = $ids[$num];
                                            $name = getName($id);
                                            if ($die) {
                                                $r .= $fig['last'] . " " . $name . "\n";
                                            } else {
                                                $r .= $fig['list'] . " " . $name . "\n";
                                                if ($id == $ids[count($ids) - 2]) {
                                                    $die = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        unset($numero);
                        if ($admin === 'moderate') {
                            $menu[] = [
                                [
                                    "text" => "?? " . getTranslate('done', null, $lang),
                                    "callback_data" => "/option $poll_id-$creator"
                                ]
                            ];
                        } elseif ($admin) {
                            $menu[0] = [
                                [
                                    "text" => getTranslate('commPageOptions', null, $lang),
                                    "callback_data" => "/option $poll_id-$creator"
                                ],
                                [
                                    "text" => getTranslate('commPageReopen', null, $lang),
                                    "callback_data" => "/open $poll_id-$creator"
                                ],
                                [
                                    "text" => getTranslate('commPageDelete', null, $lang),
                                    "callback_data" => "/delete $poll_id-$creator"
                                ]
                            ];
                        }
                        if ($poll['votes'] === 0) {
                            $r .= "\n" . getTranslate('rendererZeroVotedSoFar', [$poll['votes']], $lang);
                        } elseif ($poll['votes'] === 1) {
                            $r .= "\n" . getTranslate('rendererSingleVotedSoFar', [$poll['votes']], $lang);
                        } else {
                            $r .= "\n" . getTranslate('rendererMultiVotedSoFar', [$poll['votes']], $lang);
                        }
                        if ($poll['type'] == "limited doodle") {
                            $total = count($poll['choice']);
                            $max = $poll['settings']['max_choices'];
                            $r .= "\n?? " . getTranslate('limitedDoodleYouCanChooseSoMany', [$max, $total], $lang);
                        }
                        if ($poll['anonymous']) {
                            $r .= "\n?? " . italic(getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['type']),
                                    null, $lang));
                        } else {
                            $r .= "\n?? " . italic(getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['type']),
                                    null, $lang));
                        }
                    }
                    if ($admin === 'moderate') {
                    } else {
                        $r .= "\n\n" . getTranslate('pollClosed', null, $lang);
                    }
                    return ['ok' => true, 'text' => $r, 'menu' => $menu, 'disable_web_preview' => $allegat];
                } elseif ($poll['type'] == "board") {
                    if ($admin === 'moderate') {
                        $r .= "<h1>? " . bold($poll['title']) . "</h1>\n";
                    } else {
                        $r .= "<h1>?? " . bold($poll['title']) . "</h1>\n";
                    }
                    if ($poll['description']) {
                        $r .= htmlspecialchars($poll['description']) . "\n";
                    }
                    if ($poll['choice']) {
                        if ($admin) {
                            foreach ($poll['choice'] as $user_id => $text) {
                                if ($poll['anonymous']) {
                                    $name = "??";
                                } else {
                                    $name = htmlspecialchars_decode(getName($user_id)) . ":";
                                }
                                $r .= "\n" . bold($name) . htmlspecialchars(" $text \n");
                                if ($admin === 'moderate') {
                                    $r .= "/delete_" . str_replace('=', '',
                                            bot_encode("$poll_id-$creator-$user_id")) . "\n";
                                }
                            }
                        } elseif ($poll['settings']['hide_voters'] == false) {
                            foreach ($poll['choice'] as $user_id => $text) {
                                if ($poll['anonymous']) {
                                    $name = "??";
                                } else {
                                    $name = htmlspecialchars_decode(getName($user_id)) . ":";
                                }
                                $r .= "\n" . bold($name) . htmlspecialchars(" $text \n");
                            }
                        }
                    }
                    if ($admin === 'moderate') {
                        $menu[] = [
                            [
                                "text" => "?? " . getTranslate('done', null, $lang),
                                "callback_data" => "/option $poll_id-$creator"
                            ]
                        ];
                    } elseif ($admin) {
                        $menu[] = [
                            [
                                "text" => getTranslate('publish', null, $lang),
                                "switch_inline_query" => $poll['title']
                            ]
                        ];
                        $menu[] = [
                            [
                                "text" => getTranslate('buttonVote', null, $lang),
                                "callback_data" => "cburl-" . bot_encode("board_" . $poll['poll_id'] . "-" . $poll['creator'])
                            ],
                            [
                                "text" => getTranslate('commPageRefresh', null, $lang),
                                "callback_data" => "/update $poll_id-$creator-update"
                            ]
                        ];
                        $menu[] = [
                            [
                                "text" => getTranslate('commPageOptions', null, $lang),
                                "callback_data" => "/option $poll_id-$creator"
                            ],
                            [
                                "text" => getTranslate('commPageClose', null, $lang),
                                "callback_data" => "/close $poll_id-$creator"
                            ],
                            [
                                "text" => getTranslate('commPageDelete', null, $lang),
                                "callback_data" => "/delete $poll_id-$creator"
                            ]
                        ];
                    } else {
                        $menu[] = [
                            [
                                "text" => getTranslate('buttonVote', null, $lang),
                                "callback_data" => "cburl-" . bot_encode("board_" . $poll['poll_id'] . "-" . $poll['creator'])
                            ],
                        ];
                        if ($poll['settings']['sharable']) {
                            $menu[] = [
                                [
                                    "text" => getTranslate('share', null, $lang),
                                    "callback_data" => "cburl-" . bot_encode("share_" . $poll['poll_id'] . "-" . $poll['creator'])
                                ],
                            ];
                        }
                    }
                    if ($poll['votes'] === 0) {
                        $r .= "\n" . getTranslate('rendererZeroVotedSoFar', [$poll['votes']], $lang);
                    } elseif ($poll['votes'] === 1) {
                        $r .= "\n" . getTranslate('rendererSingleVotedSoFar', [$poll['votes']], $lang);
                    } else {
                        $r .= "\n" . getTranslate('rendererMultiVotedSoFar', [$poll['votes']], $lang);
                    }
                    if ($poll['anonymous']) {
                        $r .= "\n?? " . italic(getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['type']),
                                null, $lang));
                    } else {
                        $r .= "\n?? " . italic(getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['type']),
                                null, $lang));
                    }
                    return ['ok' => true, 'text' => $r, 'menu' => $menu, 'disable_web_preview' => $allegat];
                } elseif ($poll['type'] == "participation") {
                    if ($admin === 'moderate') {
                        return "??";
                    }
                    $r .= "<h1>?? " . bold($poll['title']) . "</h1>\n";
                    if ($poll['description']) {
                        $r .= htmlspecialchars($poll['description']) . "\n";
                    }
                    if ($admin) {
                        if ($admin === true) {
                            $publish = $poll['title'];
                        } else {
                            $publish = "share " . bot_encode("$poll_id-$creator");
                        }
                        $menu[] = [
                            [
                                "text" => getTranslate('publish', null, $lang),
                                "switch_inline_query" => $publish
                            ]
                        ];
                        $menu[] = [
                            [
                                "text" => getTranslate('buttonVote', null, $lang),
                                "callback_data" => "/vote $poll_id-$creator"
                            ],
                            [
                                "text" => getTranslate('commPageRefresh', null, $lang),
                                "callback_data" => "/update $poll_id-$creator-update"
                            ]
                        ];
                        $menu[] = [
                            [
                                "text" => getTranslate('commPageOptions', null, $lang),
                                "callback_data" => "/option $poll_id-$creator"
                            ],
                            [
                                "text" => getTranslate('commPageClose', null, $lang),
                                "callback_data" => "/close $poll_id-$creator"
                            ],
                            [
                                "text" => getTranslate('commPageDelete', null, $lang),
                                "callback_data" => "/delete $poll_id-$creator"
                            ]
                        ];
                    } else {
                        $menu[] = [
                            [
                                "text" => getTranslate('buttonParticipate', null, $lang),
                                "callback_data" => "v:$poll_id-$creator-0"
                            ],
                        ];
                        if ($poll['settings']['sharable']) {
                            $menu[] = [
                                [
                                    "text" => getTranslate('share', null, $lang),
                                    "callback_data" => "cburl-" . bot_encode("share_" . $poll['poll_id'] . "-" . $poll['creator'])
                                ],
                            ];
                        }
                    }
                    if ($poll['votes'] === 0) {
                        $r .= "\n" . getTranslate('participationZeroVotedSoFar', [$poll['votes']], $lang);
                    } elseif ($poll['votes'] === 1) {
                        $r .= "\n" . getTranslate('participationSingleVotedSoFar', [$poll['votes']], $lang);
                    } else {
                        $r .= "\n" . getTranslate('participationMultiVotedSoFar', [$poll['votes']], $lang);
                    }
                    if ($poll['anonymous']) {
                        $r .= "\n?? " . italic(getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['type']),
                                null, $lang));
                    } else {
                        $r .= "\n?? " . italic(getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['type']),
                                null, $lang));
                    }
                    return ['ok' => true, 'text' => $r, 'menu' => $menu, 'disable_web_preview' => $allegat];
                } elseif ($poll['type'] == "rating") {
                    if ($admin === 'moderate') {
                        return "??";
                    }
                    $r .= "<h1>?? " . bold($poll['title']) . "</h1>\n";
                    if ($poll['description']) {
                        $r .= htmlspecialchars($poll['description']) . "\n";
                    }
                    $percentage = getPollPercentage($poll_id, $creator);
                    if (count($poll['choice']) === 1) {
                        unset($poll['settings']['bars']);
                    } else {
                        if ($poll['settings']['sort'] and $admin !== 'moderate') {
                            $poll['choice'] = sorta($poll['choice']);
                        }
                    }
                    foreach ($poll['choice'] as $key => $value) {
                        if ($value == "NAN") {
                            $value = 0;
                        }
                        if (!isset($min_val)) {
                            $min_val = $key;
                        }
                        $all = $all + ($key * $value);
                        $emoji = "??";
                        if (isset($numero)) {
                            $numero = $numero + 1;
                        } else {
                            $numero = 0;
                        }
                        if (!$poll['anonymous'] and $admin !== false) {
                            $fig = [
                                "start" => "+",
                                "line" => "?",
                                "list" => "+",
                                "last" => "+"
                            ];
                            if ($poll['choice'][$key]) {
                                $r .= "\n" . $fig['start'] . " " . bold($key) . " [" . $value . "]\n";
                            } else {
                                $r .= "\n" . bold($key) . " [" . $value . "]\n";
                            }
                        }
                        if ($admin) {
                            if ($poll['settings']['bars'] == "dot" and $poll['usersvotes'][$key]) {
                                if (!$poll['anonymous']) {
                                    $r .= $fig['line'];
                                }
                                $r .= bars($percentage[0][$key], "dot") . " (" . nn($percentage[0][$key]) . "%) \n";
                            } elseif ($poll['settings']['bars'] == "like" and $value !== 0) {
                                if (!$poll['anonymous']) {
                                    $r .= $fig['line'];
                                }
                                $r .= bars($percentage[0][$key], "like") . " (" . nn($percentage[0][$key]) . "%) \n";
                            }
                        }
                        if (!$poll['anonymous'] and $admin !== false) {
                            $ids = array_values($poll['usersvotes'][$key]);
                            if (count($ids) === 1) {
                                $r .= $fig['last'] . " " . getName($ids[0]) . "\n";
                            } elseif (count($ids) === 0) {
                            } else {
                                unset($die);
                                unset($nums);
                                $nums = range(0, count($ids) - 1);
                                foreach ($nums as $num) {
                                    $id = $ids[$num];
                                    $name = getName($id);
                                    if ($die) {
                                        $r .= $fig['last'] . " " . $name . "\n";
                                    } else {
                                        $r .= $fig['list'] . " " . $name . "\n";
                                        if ($id == $ids[count($ids) - 2]) {
                                            $die = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    unset($numero);
                    if ($poll['votes']) {
                        $val = $all / $poll['votes'] / count($poll['choice']) * 5;
                        if ($val == 'NAN') {
                            $val = 0;
                        }
                        foreach (range($min_val, explode(".", $val)[0]) as $n) {
                            $stars .= $emoji;
                        }
                        $r .= "\n" . $stars . " [" . round($all / $poll['votes'], 1) . "] \n";
                    }
                    if ($admin) {
                        if ($admin === true) {
                            $publish = $poll['title'];
                        } else {
                            $publish = "share " . bot_encode("$poll_id-$creator");
                        }
                        $menu[] = [
                            [
                                "text" => getTranslate('publish', null, $lang),
                                "switch_inline_query" => $publish
                            ]
                        ];
                        $menu[] = [
                            [
                                "text" => getTranslate('buttonVote', null, $lang),
                                "callback_data" => "/vote $poll_id-$creator"
                            ],
                            [
                                "text" => getTranslate('commPageRefresh', null, $lang),
                                "callback_data" => "/update $poll_id-$creator-update"
                            ]
                        ];
                        $menu[] = [
                            [
                                "text" => getTranslate('commPageOptions', null, $lang),
                                "callback_data" => "/option $poll_id-$creator"
                            ],
                            [
                                "text" => getTranslate('commPageClose', null, $lang),
                                "callback_data" => "/close $poll_id-$creator"
                            ],
                            [
                                "text" => getTranslate('commPageDelete', null, $lang),
                                "callback_data" => "/delete $poll_id-$creator"
                            ]
                        ];
                    } else {
                        foreach ($percentage[0] as $key => $value) {
                            if (isset($num)) {
                                if (count($menu[$num]) >= 5) {
                                    $num = $num + 1;
                                }
                            } else {
                                $num = 0;
                            }
                            if (isset($numero)) {
                                $numero = $numero + 1;
                            } else {
                                $numero = 0;
                            }
                            if ($value == "NAN") {
                                $value = 0;
                            }
                            if (isset($poll['settings']['num_style'])) {
                                $key = $config['numbers'][$poll['settings']['num_style']][$key];
                            }
                            $menu[$num][] = [
                                'text' => $key . ' - ' . $value . "%",
                                'callback_data' => "v:$poll_id-$creator-$numero"
                            ];
                        }
                        if (!isset($poll['settings']['sharable'])) {
                            $poll['settings']['sharable'] = false;
                        }
                        if ($poll['settings']['sharable']) {
                            $menu[] = [
                                [
                                    "text" => getTranslate('share', null, $lang),
                                    "callback_data" => "cburl-" . bot_encode("share_" . $poll['poll_id'] . "-" . $poll['creator'])
                                ],
                            ];
                        }
                    }
                    if ($poll['votes'] === 0) {
                        $r .= "\n" . getTranslate('rendererZeroVotedSoFar', [$poll['votes']], $lang);
                    } elseif ($poll['votes'] === 1) {
                        $r .= "\n" . getTranslate('rendererSingleVotedSoFar', [$poll['votes']], $lang);
                    } else {
                        $r .= "\n" . getTranslate('rendererMultiVotedSoFar', [$poll['votes']], $lang);
                    }
                    if ($poll['anonymous']) {
                        $r .= "\n?? " . italic(getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['type']),
                                null, $lang));
                    } else {
                        $r .= "\n?? " . italic(getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['type']),
                                null, $lang));
                    }
                    return ['ok' => true, 'text' => $r, 'menu' => $menu, 'disable_web_preview' => $allegat];
                } else {
                    if ($admin === 'moderate') {
                        $r .= "<h1>? " . bold($poll['title']) . "</h1>\n";
                    } else {
                        $r .= "<h1>?? " . bold($poll['title']) . "</h1>\n";
                    }
                    if ($poll['description']) {
                        $r .= htmlspecialchars($poll['description']) . "\n";
                    }
                    $percentage = getPollPercentage($poll_id, $creator);
                    if (count($poll['choice']) === 1) {
                        unset($poll['settings']['bars']);
                    } else {
                        if ($poll['settings']['sort'] and $admin !== 'moderate') {
                            $poll['choice'] = sorta($poll['choice']);
                        }
                    }
                    foreach ($poll['choice'] as $key => $value) {
                        if ($value == "NAN") {
                            $value = 0;
                        }
                        if (isset($numero)) {
                            $numero = $numero + 1;
                        } else {
                            $numero = 0;
                        }
                        if (!$poll['anonymous'] and $admin !== 'moderate' and $poll['usersvotes'][$key]) {
                            if ($poll['settings']['group'] == 1) {
                                $fig = [
                                    "start" => "+",
                                    "line" => "?",
                                    "list" => "+",
                                    "last" => "+"
                                ];
                            } elseif ($poll['settings']['group'] == 2) {
                                $fig = [
                                    "start" => "+",
                                    "line" => "|",
                                    "list" => "�",
                                    "last" => "+"
                                ];
                            } elseif ($poll['settings']['group'] == "no") {
                                $fig = [
                                    "start" => "",
                                    "line" => "",
                                    "list" => "-",
                                    "last" => "-"
                                ];
                            } else {
                                $fig = [
                                    "start" => "+",
                                    "line" => "?",
                                    "list" => "+",
                                    "last" => "+"
                                ];
                            }
                            $r .= "\n" . $fig['start'] . " " . bold($key) . " [" . $value . "]\n";
                        } else {
                            $r .= "\n" . bold($key) . " [" . $value . "]\n";
                        }
                        if ($poll['settings']['bars'] == "dot" and $admin !== 'moderate' and $poll['usersvotes'][$key]) {
                            if (!$poll['anonymous'] and $admin !== 'moderate') {
                                $r .= $fig['line'];
                            }
                            $r .= bars($percentage[0][$key], "dot") . " (" . nn($percentage[0][$key]) . "%) \n";
                        } elseif ($poll['settings']['bars'] == "like" and $value !== 0) {
                            if (!$poll['anonymous'] and $admin !== 'moderate') {
                                $r .= $fig['line'];
                            }
                            $r .= bars($percentage[0][$key], "like") . " (" . nn($percentage[0][$key]) . "%) \n";
                            if ($admin === 'moderate') {
                                $r .= "/delete_" . str_replace('=', '', bot_encode("$poll_id-$creator-$numero")) . "\n";
                            }
                        } else {
                            if ($admin === 'moderate') {
                                $r .= "/delete_" . str_replace('=', '', bot_encode("$poll_id-$creator-$numero")) . "\n";
                            }
                        }
                        if (!$poll['anonymous'] and $poll['usersvotes'][$key]) {
                            if ($admin === 'moderate') {
                            } elseif ($poll['settings']['hide_voters'] and !$admin) {
                            } else {
                                $ids = array_values($poll['usersvotes'][$key]);
                                if (count($ids) === 1) {
                                    $r .= $fig['last'] . " " . getName($ids[0]) . "\n";
                                } else {
                                    unset($die);
                                    unset($nums);
                                    $nums = range(0, count($ids) - 1);
                                    foreach ($nums as $num) {
                                        $id = $ids[$num];
                                        $name = getName($id);
                                        if ($die) {
                                            $r .= $fig['last'] . " " . $name . "\n";
                                        } else {
                                            $r .= $fig['list'] . " " . $name . "\n";
                                            if ($id == $ids[count($ids) - 2]) {
                                                $die = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    unset($numero);
                    if ($admin === 'moderate') {
                        $menu[] = [
                            [
                                "text" => "?? " . getTranslate('done', null, $lang),
                                "callback_data" => "/option $poll_id-$creator"
                            ]
                        ];
                    } elseif ($admin === true) {
                        $menu[] = [
                            [
                                "text" => getTranslate('publish', null, $lang),
                                "switch_inline_query" => $poll['title']
                            ]
                        ];
                        $menu[] = [
                            [
                                "text" => getTranslate('publishWithLink', null, $lang),
                                "switch_inline_query" => '$c:' . $poll['title']
                            ]
                        ];
                        $menu[] = [
                            [
                                "text" => getTranslate('buttonVote', null, $lang),
                                "callback_data" => "/vote $poll_id-$creator"
                            ],
                            [
                                "text" => getTranslate('commPageRefresh', null, $lang),
                                "callback_data" => "/update $poll_id-$creator-update"
                            ]
                        ];
                        $menu[] = [
                            [
                                "text" => getTranslate('commPageOptions', null, $lang),
                                "callback_data" => "/option $poll_id-$creator"
                            ],
                            [
                                "text" => getTranslate('commPageClose', null, $lang),
                                "callback_data" => "/close $poll_id-$creator"
                            ],
                            [
                                "text" => getTranslate('commPageDelete', null, $lang),
                                "callback_data" => "/delete $poll_id-$creator"
                            ]
                        ];
                    } elseif ($admin and isAdmin($admin, $poll)) {
                        $menu[] = [
                            [
                                "text" => getTranslate('buttonVote', null, $lang),
                                "callback_data" => "/vote $poll_id-$creator"
                            ],
                            [
                                "text" => getTranslate('commPageRefresh', null, $lang),
                                "callback_data" => "/update $poll_id-$creator-update"
                            ]
                        ];
                        $menu[] = [
                            [
                                "text" => getTranslate('commPageOptions', null, $lang),
                                "callback_data" => "/option $poll_id-$creator"
                            ],
                            [
                                "text" => getTranslate('commPageClose', null, $lang),
                                "callback_data" => "/close $poll_id-$creator"
                            ],
                            [
                                "text" => getTranslate('commPageDelete', null, $lang),
                                "callback_data" => "/delete $poll_id-$creator"
                            ]
                        ];
                    } else {
                        foreach ($percentage[0] as $key => $value) {
                            if (isset($numero)) {
                                $numero = $numero + 1;
                            } else {
                                $numero = 0;
                            }
                            if ($value == "NAN") {
                                $value = 0;
                            }
                            $menu[] = [
                                [
                                    'text' => $key . ' - ' . $value . "%",
                                    'callback_data' => "v:$poll_id-$creator-$numero"
                                ]
                            ];
                        }
                        if (!isset($poll['settings']['appendable'])) {
                            $poll['settings']['appendable'] = false;
                        }
                        if ($poll['settings']['appendable']) {
                            $menu[] = [
                                [
                                    "text" => getTranslate('buttonAppend', [], $lang),
                                    "callback_data" => "cburl-" . bot_encode("append_$poll_id-$creator")
                                ]
                            ];
                        }
                        if (!isset($poll['settings']['sharable'])) {
                            $poll['settings']['sharable'] = false;
                        }
                        if ($poll['settings']['sharable']) {
                            $menu[] = [
                                [
                                    "text" => getTranslate('share', null, $lang),
                                    "callback_data" => "cburl-" . bot_encode("share_$poll_id-$creator")
                                ]
                            ];
                        }
                    }
                    if (!isset($poll['votes'])) {
                        $poll['votes'] = 0;
                    }
                    if ($poll['votes'] === 0) {
                        $r .= "\n" . getTranslate('rendererZeroVotedSoFar', [$poll['votes']], $lang);
                    } elseif ($poll['votes'] === 1) {
                        $r .= "\n" . getTranslate('rendererSingleVotedSoFar', [$poll['votes']], $lang);
                    } else {
                        $r .= "\n" . getTranslate('rendererMultiVotedSoFar', [$poll['votes']], $lang);
                    }
                    if ($poll['type'] == "limited doodle" or $poll['settings']['type'] == "limited doodle") {
                        $total = count($poll['choice']);
                        $max = $poll['settings']['max_choices'];
                        $r .= "\n?? " . getTranslate('limitedDoodleYouCanChooseSoMany', [$max, $total], $lang);
                    }
                    if ($poll['anonymous']) {
                        $r .= "\n?? " . italic(getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['type']),
                                null, $lang));
                    } else {
                        $r .= "\n?? " . italic(getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['type']),
                                null, $lang));
                    }
                    return ['ok' => true, 'text' => $r, 'menu' => $menu, 'disable_web_preview' => $allegat];
                }
            }
            function pollToArray ($poll = false) {
				if (!isset($poll)) {
					cell_error("<b>Warning:</b> la variabile \$poll non è stata settata nella funzione pollToArray");
					return $array;
				}
				if ($poll['anonymous']) {
					$privacy = "anonymous";
				} else {
					$privacy = "personal";
				}
				$choices = [];
				if ($poll['usersvotes']) {
					if ($poll['type'] == "board") {
						$ischoices = "comments";
						$choices = [];
						foreach ($poll['choice'] as $id => $comment) {
							if ($poll['anonymous']) {
								$choices[] = $comment;
							} else {
								$choices[getName($id)] = $comment;
							}
						}
					} elseif ($poll['type'] == "participation") {
						$ischoices = "participants";
						foreach ($poll['usersvotes']['participants'] as $id) {
							$choices[] = getName($id);
						}
					} else {
						$ischoices = "choices";
						foreach ($poll['usersvotes'] as $choice => $users) {
							if ($poll['anonymous']) {
								$choices[$choice] = 0;
							} else {
								$choices[$choice] = [];
							}
							foreach ($users as $id) {
								if ($poll['anonymous']) {
									if (!$choices[$choice]) $choices[$choice] = 0;
									$choices[$choice] = $choices[$choice] + 1;
								} else {
									$choices[$choice][] = getName($id);
								}
							}
							if (!isset($choices[$choice])) $choices[$choice] = [];
						}
					}
				} else {
					if ($poll['type'] == "board") {
						$ischoices = "comments";
					} elseif ($poll['type'] == "participation") {
						$ischoices = "participants";
					} else {
						$ischoices = "choices";
					}
				}
				$array = [
					'poll_id' => round($poll['poll_id']),
					'owner_id' => round($poll['creator']),
					'title' => $poll['title'],
				];
				if (isset($poll['description'])) $array['description'] = $poll['description'];
				$array['privacy'] = $privacy;
				$array['type'] = $poll['type'];
				if ($poll['type'] == "limited doodle") $array['max_choices'] = $poll['max_choices'];
				$array[$ischoices] = $choices;
				return $array;
			}
		} else {
			echo 500;
			die;
		}
        $_SESSION['thread'] = bot_decode($_SESSION['thread']);
        $e = explode("-", $_SESSION['thread']);
        $poll_id = $e[0];
        $creator = $e[1];
        $p = sendPoll($poll_id, $creator);
		if (!$p['status'] or $p['status'] == "deleted") {
			echo 404;
		} elseif ($p['settings']['sharable']) {
			$poll = pollToArray($p);
			if ($p['type'] == "participation") $poll['participants'] = count($poll['participants']);
            echo json_encode($poll);
        } else {
            echo 401;
        }
    } else {
        echo 404;
    }
} else {
    echo 500;
}
?>