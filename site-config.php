<?php

$config['site'] = [
    'usernames' => [
        689942156 => "MasterPollBot",
        757060205 => "MasterPoll2Bot",
        754307698 => "NewGroupAgreeBot",
        937893103 => "MasterPollBetaBot",
        753396760 => "GoldenPollBot"
    ],
    'beta_bots' => [754307698, 937893103],
    'encrypt_method' => "BF-OFB",
    'secret_key' => 'a3bgv563w34s45',
    'secret_iv' => 'es56w453es45',
    'usa_il_db' => true,
    'usa_redis' => true,
    'class_work' => true,
	"errors" => [
		400 => "Bad Request",
		401 => "Unauthorized",
		403 => "Forbidden",
		404 => "Not Found",
		405 => "Method Not Allowed",
		429 => "Too Many Requests",
		500 => "Internal Server Error",
		502 => "Bad Gateway"
	],
    'redis' => [
        'database' => 498,
        'host' => "localhost",
        'port' => 6379,
        'password' => false,
    ],
    'database' => [
        "host" => "localhost",
        "user" => "pollbot",
        "password" => "=HEdK=k5/yXFXBpd",
        "database" => "pollbot"
    ]
];
$config['username_bot'] = $config['site']['usernames'][$botID];