<?php

if ($config['usa_il_db']) {
	if (strtolower($database['type']) == 'postgre') {
		$PG = pg_connect("host=" . $database['host'] . " dbname=" . $database['nome_database'] . " user=" . $database['utente'] . " password=" . $database['password'], PGSQL_CONNECT_FORCE_NEW );
		if (!$PG) {
			if (isset($update->message) and $isadmin and $update->message->getCommand() == "/database") {
				$bot->sendMessage([
					'chat_id' => $update->message->chat->id,
					'text' => "❌ Database Error"
				]);
			}
			die;
		}
		if (isset($update->message) and $isadmin and $update->message->getCommand() == "/database") {
			$bot->sendMessage([
				'chat_id' => $update->message->chat->id,
				'text' => "✅ Database online"
			]);
			die;
		}
	} else {
		if (isset($update->message) and $isadmin and $update->message->getCommand() == "/database") {
			$bot->sendMessage([
				'chat_id' => $update->message->chat->id,
				'text' => "❌ Database Error: unknown database type"
			]);
		}
		die;
	}
} else {
	if (isset($update->message) and $isadmin and $update->message->getCommand() == "/database") {
		$bot->sendMessage([
			'chat_id' => $update->message->chat->id,
			'text' => "💤 Database disabilitato"
		]);
	}
	die;
}

?>
