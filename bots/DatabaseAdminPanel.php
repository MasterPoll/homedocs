<?php

if (isset($update->message) and $update->message->getCommand()) {
	if ($update->message->getCommand() == "/iscritti") {
		$MasterPoll = new MasterPoll();
		$q[0] = $MasterPoll->db_query("SELECT COUNT(*) FROM utenti WHERE strpos(status,$1)>0", [$botID], false);
		$q[1] = $MasterPoll->db_query("SELECT COUNT(*) FROM utenti", false, false);
		$q[2] = $MasterPoll->db_query("SELECT COUNT(*) FROM gruppi WHERE strpos(status,$1)>0", [$botID], false);
		$q[3] = $MasterPoll->db_query("SELECT COUNT(*) FROM gruppi", false, false);
		$q[4] = $MasterPoll->db_query("SELECT COUNT(*) FROM canali WHERE strpos(status,$1)>0", [$botID], false);
		$q[5] = $MasterPoll->db_query("SELECT COUNT(*) FROM canali", false, false);
		if (!$q[0]['ok'] or !$q[1]['ok'] or !$q[2]['ok'] or !$q[3]['ok'] or !$q[4]['ok'] or !$q[5]['ok']) {
			$t = "Errore: " . json_encode($q);
		} else {
			$menu[] = [
				[
					'text' => "Più informazioni ➕",
					'callback_data' => 'ap_subs+'
				]
			];
			$t = $MasterPoll->bold("ISCRITTI 👥") . "\n" . $MasterPoll->bold("👤 Utenti: ") . round($q[0]['result'][0]['count']) . "/" . round($q[1]['result'][0]['count']) . "\n" . $MasterPoll->bold("👥 Gruppi: ") . round($q[2]['result'][0]['count']) . "/" . round($q[3]['result'][0]['count']) . "\n" . $MasterPoll->bold("📢 Canali: ") . round($q[4]['result'][0]['count']) . "/" . round($q[5]['result'][0]['count']);
		}
		$bot->sendMessage([
			'chat_id'				=> $update->message->chat->id,
			'text'					=> $t,
			'parse_mode'			=> "html",
			'reply_markup'			=> json_encode(['inline_keyoard' => $menu])
		]);
		die;
	} elseif ($update->message->getCommand() == "/me") {
		if (!isset($MasterPoll)) $MasterPoll = new MasterPoll();
		$q = $MasterPoll->db_query("SELECT * FROM utenti WHERE user_id = $1", [$update->message->from->id], false);
		if ($q['ok']) {
			$n = $MasterPoll->getName($update->message->from->id);
			if ($n['ok']) {
				$name = $n['result'];
			} else {
				$name = json_encode($n);
			}
			$t = $name . ": " . $MasterPoll->code(json_encode($q['result'], JSON_PRETTY_PRINT));
		} else {
			$t = "❌ Errore: " . $MasterPoll->code(json_encode($q, JSON_PRETTY_PRINT));
		}
		$bot->sendMessage([
			'chat_id'				=> $update->message->chat->id,
			'text'					=> $t,
			'parse_mode'			=> "html"
		]);
		die;
	} elseif ($update->message->getCommand() == "/bl_add") {
		// To do
		die;
	}
}
if (isset($update->callback_query)) {
	if ($update->callback_query->data == "close") {
		$bot->deleteMessage(
			$update->callback_query->message->chat->id,
			$update->callback_query->message->message_id
		);
		$bot->answerCallbackQuery([
			'callback_query_id' => $update->callback_query->id,
			'text' => '👍🏻'
		]);
		die;
	}
}


?>