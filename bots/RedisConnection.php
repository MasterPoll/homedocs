<?php

if ($config['redis']) {
	$redisc = $config['redis'];
	try {
		$redis = new Redis();
		$redis->connect($redisc['host'], $redisc['port']);
	} catch (Exception $e) {
		if (isset($update->message) and $isadmin and $update->message->getCommand() == "/redis") {
			$bot->sendMessage([
				'chat_id' => $update->message->chat->id,
				'text' => "❌ Redis Error: " . $e->getMessage()
			]);
		}
		die;
	}
	if ($redisc['password'] !== false) {
		try {
			$redis->auth($redisc['password']);
		} catch (Exception $e) {
			if (isset($update->message) and $isadmin and $update->message->getCommand() == "/redis") {
				$bot->sendMessage([
					'chat_id' => $update->message->chat->id,
					'text' => "❌ Redis Error: " . $e->getMessage()
				]);
			}
			die;
		}
	}
	if ($redisc['database'] !== false) {
		try {
			$redis->select($redisc['database']);
		} catch (Exception $e) {
			if (isset($update->message) and $isadmin and $update->message->getCommand() == "/redis") {
				$bot->sendMessage([
					'chat_id' => $update->message->chat->id,
					'text' => "❌ Redis Error: " . $e->getMessage()
				]);
			}
			die;
		}
	}
	if (isset($update->message) and $isadmin and $update->message->getCommand() == "/redis") {
		$bot->sendMessage([
			'chat_id' => $update->message->chat->id,
			'text' => "✅ Redis online"
		]);
		die;
	}
} else {
	if (isset($update->message) and $isadmin and $update->message->getCommand() == "/redis") {
		$bot->sendMessage([
			'chat_id' => $update->message->chat->id,
			'text' => "💤 Redis disabilitato"
		]);
	}
	die;
}

?>