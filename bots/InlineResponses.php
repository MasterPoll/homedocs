<?php

$inline_result = [];
if ($update->inline_query->query) {
	$operation = "Something";
} else {
	if (!isset($MasterPoll)) $MasterPoll = new MasterPoll();
	$operation = "Poll list";
}

$bot->answerInlineQuery([
	'inline_query_id' => $update->inline_query->id,
	'results' => json_encode($inline_result),
	'cache_time' => 5,
	'switch_pm_text' => $operation,
	'switch_pm_parameter' => "inline-options"
]);

?>