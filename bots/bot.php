<?php
$pluginp = "bot.php";

if (!isset($_GET['key']) or !isset($_GET['password'])) {
	$error = 400;
	if (substr(php_sapi_name(), 0, 3) == 'cgi') {
		header("Status: $error Bad Request");
	} else {
		header("HTTP/1.1 $error Bad Request");
	}
	http_response_code($error);
	die;
}

$password = urldecode($_GET['password']);
if (!file_exists("/var/log/masterpoll/bot$botID.log")) file_put_contents("/var/log/masterpoll/bot$botID.log", '');
ini_set('error_log', "/var/log/masterpoll/bot$botID.log");
set_error_handler("errorHandler");
register_shutdown_function("shutdownHandler");
function errorHandler($error_level, $error_message, $error_file, $error_line, $error_context) {
    global $config;
    $error = $error_message . " \nLine: " . $error_line;
    switch ($error_level) {
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_PARSE:
            if ($config['log_report']['FATAL']) {
                call_error("FATAL: " . $error, $error_file);
            }
            break;
        case E_USER_ERROR:
        case E_RECOVERABLE_ERROR:
            if ($config['log_report']['ERROR']) {
                call_error("ERROR: " . $error, $error_file);
            }
            break;
        case E_WARNING:
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
        case E_USER_WARNING:
            if ($config['log_report']['WARN']) {
                call_error("WARNING: " . $error, $error_file);
            }
            break;
        case E_NOTICE:
        case E_USER_NOTICE:
            if ($config['log_report']['INFO']) {
                call_error("INFO: " . $error, $error_file);
            }
            break;
        case E_STRICT:
            if ($config['log_report']['DEBUG']) {
                call_error("DEBUG: " . $error, $error_file);
            }
            break;
        default:
            if ($config['log_report']['WARN']) {
                call_error("WARNING: " . $error, $error_file);
            }
    }
}
function shutdownHandler() {
    global $config;
    global $redis;
    global $PDO;
    if (isset($PDO)) {
		unset($PDO);
    }
    if (isset($redis)) {
		unset($redis);
    }
    $lasterror = error_get_last();
    switch ($lasterror['type']) {
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_USER_ERROR:
        case E_RECOVERABLE_ERROR:
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
        case E_PARSE:
			fastcgi_finish_request();
            if ($config['log_report']['SHUTDOWN']) {
                $error = $lasterror['message'] . " \nLine: " . $lasterror['line'];
                call_error($error, $lasterror['file']);
            }
    }
}

require("/home/masterpoll-documents/bots/telegrambot-php/vendor/autoload.php");
if (class_exists("TelegramBot\TelegramBot")) {
	fastcgi_finish_request();
	try {
		$bot = new TelegramBot\TelegramBot($token);
		$update = $bot->getWebhookUpdate();
		if (isset($update->message->from->id)) {
			if (in_array($update->message->from->id, $config['admins'])) {
				$isadmin = true;
			} else {
				$isadmin = false;
			}
		} elseif (isset($update->callback_query->from->id)) {
			if (in_array($update->callback_query->from->id, $config['admins'])) {
				$isadmin = true;
			} else {
				$isadmin = false;
			}
		} else {
			$isadmin = false;
		}
		if ($isadmin) require("AdminPanel.php");
		$status = json_decode(file_get_contents("/home/masterpoll-documents/status-bots.json"), true);
		if ($status[$botID]) {
			require("RedisConnection.php");
			require("DatabaseConnection.php");
			require("MasterPoll_Class.php");
			if (class_exists("MasterPoll")) {
				if ($isadmin) require("DatabaseAdminPanel.php");
				if (isset($update->callback_query->id)) {
					require("CallbackResponses.php");
				} elseif (isset($update->message->chat->id)) {
					require("Commands.php");
				} elseif (isset($update->inline_query->id)) {
					require("InlineResponses.php");
				}
			} else {
				call_error("MasterPoll Class not found");
				die;
			}
		} else {
			if (isset($update->callback_query->id)) {
				$bot->answerCallbackQuery([
					'callback_query_id' => $update->callback_query->id,
					'text' => '⚠ Bot offline'
				]);
			} elseif (isset($update->message->chat->id)) {
				$bot->sendMessage([
					'chat_id' => $update->message->chat->id,
					'text' => '⚠ Bot offline, try to use other @MasterPollBots.'
				]);
			} elseif (isset($update->inline_query->id)) {
				$bot->answerInlineQuery([
					'inline_query_id' => $update->inline_query->id,
					'results' => '[]',
					'cache_time' => 5,
					'switch_pm_text' => "⚠ Bot offline",
					'switch_pm_parameter' => "start"
				]);
			}
		}
	} catch (TelegramBot\TelegramException $e) {
		call_error($e);
		die;
	}
} else {
	$error = 500;
	if (substr(php_sapi_name(), 0, 3) == 'cgi') {
		header("Status: $error Internal Server Error");
	} else {
		header("HTTP/1.1 $error Internal Server Error");
	}
	http_response_code($error);
	die;
}

?>
