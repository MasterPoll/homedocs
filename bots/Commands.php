<?php

if (isset($update->message)) {
	$cmd = $update->message->getCommand();
	if (isset($update->message->chat->id)) {
		$chat_id = $update->message->chat->id;
	}
	if ($cmd) {
		$messageType = 'command';
		$cmd = mb_substr($cmd, 1, mb_strlen($cmd));
	} elseif (isset($update->message->text)) {
		$messageType = 'message';
	} else {
		$messageType = 'unknown';
	}
	if ($update->message->chat->type) {
		$typeChat = $update->message->chat->type;
	} else {
		$typeChat = 'unknown';
	}
}

if ($messageType == 'command' and $typeChat == 'private') {
	
	if ($cmd == "start") {
		$bot->sendMessage([
			'chat_id' => $chat_id,
			'text' => "Bot in Testing... wait for new updates in @MasterPollBeta"
		]);
		die;
	} else {
		$bot->sendMessage([
			'chat_id' => $chat_id,
			'text' => 'Unknown command',//getTranslate('unknownCommand')
		]);
		die;
	}
	
} 

?>
