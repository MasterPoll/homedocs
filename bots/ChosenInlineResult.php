<?php

if (isset($update->chosen_inline_result)) {
	$expl = explode('-', $update->chosen_inline_result->result_id);
	if ($expl[2] == "button") {
		$type = "button";
	} elseif ($expl[2] == "addadmin") {
		$type = "addadmin";
	} else {
		$type = false;
	}
	insertMessage(['poll_id' => $expl[1], 'creator' => $expl[0]], $update->chosen_inline_result->inline_message_id, $type);
	if ($update->chosen_inline_result->inline_message_id) {
		$cbmid = $update->chosen_inline_result->inline_message_id;
		if (!isset($MasterPoll)) $MasterPoll = new MasterPoll();
		$MasterPoll->updatePoll($expl[1], $expl[0]);
	}
}

?>