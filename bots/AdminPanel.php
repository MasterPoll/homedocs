<?php

if (isset($update->message) and $update->message->getCommand()) {
	if ($update->message->getCommand() == "/isadmin") {
		$bot->sendMessage([
			'chat_id'				=> $update->message->chat->id,
			'text'					=> "💎 <b>NeleBot Pro++</> 💎",
			'parse_mode'			=> "html",
			'reply_to_message_id'	=> $update->message->message_id
		]);
		die;
	} elseif ($update->message->getCommand() == "/off") {
		$file = "/home/masterpoll-documents/status-bots.json";
		if (file_exists($file)) {
			$botstatus = json_decode(file_get_contents($file), true);
		} else {
			$exjson = $config['usernames'];
			foreach($exjson as $j => $us) {
				$botstatus[$j] = true;
			}
		}
		$botstatus = json_decode(file_get_contents($file), true);
		$botstatus[$botID] = false;
		file_put_contents($file, json_encode($botstatus));
		$bot->sendMessage([
			'chat_id'		=> $update->message->chat->id,
			'text'			=> "💤 <b>Bot spento!</b>",
			'parse_mode'	=> "html"
		]);
		die;
	} elseif ($update->message->getCommand() == "/on") {
		$file = "/home/masterpoll-documents/status-bots.json";
		if (file_exists($file)) {
			$botstatus = json_decode(file_get_contents($file), true);
		} else {
			$exjson = $config['usernames'];
			foreach($exjson as $j => $us) {
				$botstatus[$j] = true;
			}
		}
		$botstatus[$botID] = true;
		file_put_contents($file, json_encode($botstatus));
		$bot->sendMessage([
			'chat_id'		=> $update->message->chat->id,
			'text'			=> "✅ <b>Bot acceso!</b>",
			'parse_mode'	=> "html"
		]);
		die;
	} elseif ($update->message->getCommand() == "/bots") {
		$file = "/home/masterpoll-documents/status-bots.json";
		if (file_exists($file)) {
			$botstatus = json_decode(file_get_contents($file), true);
		} else {
			$exjson = $config['usernames'];
			foreach($exjson as $j => $us) {
				$botstatus[$j] = true;
			}
			file_put_contents($file, json_encode($botstatus, JSON_PRETTY_PRINT));
		}
		$e = [
			true	=> false,
			false	=> true,
			null	=> true
		];
		$c = [
			true	=> "✅",
			false	=> "💤"
		];
		foreach ($botstatus as $type => $active) {
			$es = $e[$active];
			$emo = $c[$active];
			$extext = $type;
			$type = $config['usernames'][$type];
			$menu[] = [
				[
					"text" => "$type $emo",
					"callback_data" => "editBots_$extext-$es"
				]
			];
		}
		$menu[] = [
			[
				"text" => "⬇️ Off",
				"callback_data" => "editBots_all-0"
			],
			[
				"text" => "⬆️ On",
				"callback_data" => "editBots_all-1"
			]
		];
		$menu[] = [
			[
				"text" => "✅ Fatto",
				"callback_data" => "close"
			]
		];
		$bot->sendMessage([
			'chat_id'		=> $update->message->chat->id,
			'text'			=> "🤖 <b>Attiva/Disattiva i Bot:</>",
			'parse_mode'	=> "html",
			'reply_markup'	=> json_encode(['inline_keyboard' => $menu])
		]);
		$bot->deleteMessage(
			$update->message->chat->id,
			$update->message->message_id
		);
		die;
	} elseif ($update->message->getCommand() == "/methods") {
		$file = "/home/masterpoll-documents/api-methods.json";
		if (file_exists($file)) {
			$api_methods = json_decode(file_get_contents($file), true);
		} else {
			$api_methods = [];
			file_put_contents($file, json_encode($api_methods, JSON_PRETTY_PRINT));
		}
		$e = [
			true	=> false,
			false	=> true,
			null	=> true
		];
		$c = [
			true	=> "✅",
			false	=> "💤"
		];
		foreach ($api_methods as $type => $active) {
			$es = $e[$active];
			$emo = $c[$active];
			$extext = $type;
			$menu[] = [
				[
					"text" => "$type $emo",
					"callback_data" => "editMethods_$extext-$es"
				]
			];
		}
		$menu[] = [
			[
				"text" => "⬇️ Off",
				"callback_data" => "editMethods_all-0"
			],
			[
				"text" => "⬆️ On",
				"callback_data" => "editMethods_all-1"
			]
		];
		$menu[] = [
			[
				"text" => "✅ Fatto",
				"callback_data" => "close"
			]
		];
		$bot->sendMessage([
			'chat_id'		=> $update->message->chat->id,
			'text'			=> "📍 <b>Attiva/Disattiva i metodi delle API:</>",
			'parse_mode'	=> "html",
			'reply_markup'	=> json_encode(['inline_keyboard' => $menu])
		]);
		$bot->deleteMessage(
			$update->message->chat->id,
			$update->message->message_id
		);
		die;
	} elseif ($update->message->getCommand() == "/types") {
		$file = "/home/masterpoll-documents/types.json";
		if (file_exists($file)) {
			$types = json_decode(file_get_contents($file), true);
		} else {
			$exjson = $config['types'];
			foreach($exjson as $j) {
				$types[$j] = true;
			}
			file_put_contents($file, json_encode($types, JSON_PRETTY_PRINT));
		}
		$e = [
			true	=> false,
			false	=> true,
			null	=> true
		];
		$c = [
			true	=> "✅",
			false	=> "💤"
		];
		foreach ($types as $type => $active) {
			$es = $e[$active];
			$emo = $c[$active];
			$extext = $type;
			$type[0] = strtoupper($type[0]);
			$menu[] = [
				[
					"text" => "$type $emo",
					"callback_data" => "editTypes_$extext-$es"
				]
			];
		}
		$menu[] = [
			[
				"text" => "⬇️ Off",
				"callback_data" => "editTypes_all-0"
			],
			[
				"text" => "⬆️ On",
				"callback_data" => "editTypes_all-1"
			]
		];
		$menu[] = [
			[
				"text" => "✅ Fatto",
				"callback_data" => "close"
			]
		];
		$bot->sendMessage([
			'chat_id'		=> $update->message->chat->id,
			'text'			=> "🔡 <b>Attiva/Disattiva l'utilizzo generale dei tipi di sondaggio:</>",
			'parse_mode'	=> "html",
			'reply_markup'	=> json_encode(['inline_keyboard' => $menu])
		]);
		$bot->deleteMessage(
			$update->message->chat->id,
			$update->message->message_id
		);
		die;
	} elseif ($update->message->getCommand() == "/shop") {
		$file = "/home/masterpoll-documents/shop.json";
		if (file_exists($file)) {
			$shop = json_decode(file_get_contents($file), true);
		} else {
			$shop = [];
			file_put_contents($file, json_encode($shop, JSON_PRETTY_PRINT));
		}
		$e = [
			true	=> false,
			false	=> true,
			null	=> true
		];
		$c = [
			true	=> "✅",
			false	=> "💤"
		];
		foreach ($shop as $type => $active) {
			$active = $active['available'];
			$es = $e[$active];
			$emo = $c[$active];
			$extext = $type;
			$menu[] = [
				[
					"text" => "$type $emo",
					"callback_data" => "editShop_$extext-$es"
				]
			];
		}
		$menu[] = [
			[
				"text" => "⬇️ Off",
				"callback_data" => "editShop_all-0"
			],
			[
				"text" => "⬆️ On",
				"callback_data" => "editShop_all-1"
			]
		];
		$menu[] = [
			[
				"text" => "✅ Fatto",
				"callback_data" => "close"
			]
		];
		$bot->sendMessage([
			'chat_id'		=> $update->message->chat->id,
			'text'			=> "🛍 <b>Attiva/Disattiva la disponibilità degli articoli nel Negozio:</>",
			'parse_mode'	=> "html",
			'reply_markup'	=> json_encode(['inline_keyboard' => $menu])
		]);
		$bot->deleteMessage(
			$update->message->chat->id,
			$update->message->message_id
		);
		die;
	}
}
if (isset($update->callback_query)) {
	if ($update->callback_query->data == "close") {
		$bot->deleteMessage(
			$update->callback_query->message->chat->id,
			$update->callback_query->message->message_id
		);
		$bot->answerCallbackQuery([
			'callback_query_id' => $update->callback_query->id,
			'text' => '👍🏻'
		]);
		die;
	} elseif (strpos($update->callback_query->data, 'editBots_') === 0) {
		$file = "/home/masterpoll-documents/status-bots.json";
		if (file_exists($file)) {
			$botstatus = json_decode(file_get_contents($file), true);
		} else {
			$exjson = $config['usernames'];
			foreach($exjson as $j => $us) {
				$botstatus[$j] = true;
			}
		}
		$e = [
			true	=> false,
			false	=> true,
			null	=> true
		];
		$c = [
			true	=> "✅",
			false	=> "💤"
		];
		$expl = explode("-", str_replace("editBots_", '', $update->callback_query->data));
		$type = strtolower($expl[0]);
		$set = round($expl[1]);
		if ($type == "all") {
			foreach ($botstatus as $m => $s) {
				$botstatus[$m] = $set;
			}
			if (!isset($botstatus[$botID])) $botstatus[$botID] = true;
		} else {
			$botstatus[$type] = $set;
		}
		file_put_contents($file, json_encode($botstatus, JSON_PRETTY_PRINT));
		foreach ($botstatus as $type => $active) {
			$es = $e[$active];
			$emo = $c[$active];
			$extext = $type;
			$type = $config['usernames'][$type];
			$menu[] = [
				[
					"text" => "$type $emo",
					"callback_data" => "editBots_$extext-$es"
				]
			];
		}
		$menu[] = [
			[
				"text" => "⬇️ Off",
				"callback_data" => "editBots_all-0"
			],
			[
				"text" => "⬆️ On",
				"callback_data" => "editBots_all-1"
			]
		];
		$menu[] = [
			[
				"text" => "✅ Fatto",
				"callback_data" => "close"
			]
		];
		$bot->answerCallbackQuery([
			'callback_query_id' => $update->callback_query->id,
			'text' => ''
		]);
		try {
			$bot->editMessageReplyMarkup([
				'chat_id'		=> $update->callback_query->message->chat->id,
				'message_id'	=> $update->callback_query->message->message_id,
				'reply_markup'	=> json_encode(['inline_keyboard' => $menu])
			]);
		} catch(TelegramBot\TelegramException $e) {
			//call_error($e->getMessage());
		}
		die;
	} elseif (strpos($update->callback_query->data, 'editMethods_') === 0) {
		$file = "/home/masterpoll-documents/api-methods.json";
		if (file_exists($file)) {
			$api_methods = json_decode(file_get_contents($file), true);
		} else {
			$api_methods = [];
		}
		$e = [
			true	=> false,
			false	=> true,
			null	=> true
		];
		$c = [
			true	=> "✅",
			false	=> "💤"
		];
		$expl = explode("-", str_replace("editMethods_", '', $update->callback_query->data));
		$type = strtolower($expl[0]);
		$set = round($expl[1]);
		if ($type == "all") {
			foreach ($api_methods as $m => $s) {
				$api_methods[$m] = $set;
			}
		} else {
			$api_methods[$type] = $set;
		}
		file_put_contents($file, json_encode($api_methods, JSON_PRETTY_PRINT));
		foreach ($api_methods as $type => $active) {
			$es = $e[$active];
			$emo = $c[$active];
			$extext = $type;
			$menu[] = [
				[
					"text" => "$type $emo",
					"callback_data" => "editMethods_$extext-$es"
				]
			];
		}
		$menu[] = [
			[
				"text" => "⬇️ Off",
				"callback_data" => "editMethods_all-0"
			],
			[
				"text" => "⬆️ On",
				"callback_data" => "editMethods_all-1"
			]
		];
		$menu[] = [
			[
				"text" => "✅ Fatto",
				"callback_data" => "close"
			]
		];
		$bot->answerCallbackQuery([
			'callback_query_id' => $update->callback_query->id,
			'text' => ''
		]);
		try {
			$bot->editMessageReplyMarkup([
				'chat_id'		=> $update->callback_query->message->chat->id,
				'message_id'	=> $update->callback_query->message->message_id,
				'reply_markup'	=> json_encode(['inline_keyboard' => $menu])
			]);
		} catch(TelegramBot\TelegramException $e) {
			//call_error($e->getMessage());
		}
		die;
	} elseif (strpos($update->callback_query->data, 'editTypes_') === 0) {
		$file = "/home/masterpoll-documents/types.json";
		if (file_exists($file)) {
			$types = json_decode(file_get_contents($file), true);
		} else {
			$exjson = $config['types'];
			foreach($exjson as $j) {
				$types[$j] = true;
			}
		}
		$e = [
			true	=> false,
			false	=> true,
			null	=> true
		];
		$c = [
			true	=> "✅",
			false	=> "💤"
		];
		$expl = explode("-", str_replace("editTypes_", '', $update->callback_query->data));
		$type = strtolower($expl[0]);
		$set = round($expl[1]);
		if ($type == "all") {
			foreach ($types as $m => $s) {
				$types[$m] = $set;
			}
		} else {
			$types[$type] = $set;
		}
		file_put_contents($file, json_encode($types, JSON_PRETTY_PRINT));
		foreach ($types as $type => $active) {
			$es = $e[$active];
			$emo = $c[$active];
			$extext = $type;
			$type[0] = strtoupper($type[0]);
			$menu[] = [
				[
					"text" => "$type $emo",
					"callback_data" => "editTypes_$extext-$es"
				]
			];
		}
		$menu[] = [
			[
				"text" => "⬇️ Off",
				"callback_data" => "editTypes_all-0"
			],
			[
				"text" => "⬆️ On",
				"callback_data" => "editTypes_all-1"
			]
		];
		$menu[] = [
			[
				"text" => "✅ Fatto",
				"callback_data" => "close"
			]
		];
		$bot->answerCallbackQuery([
			'callback_query_id' => $update->callback_query->id,
			'text' => ''
		]);
		try {
			$bot->editMessageReplyMarkup([
				'chat_id'		=> $update->callback_query->message->chat->id,
				'message_id'	=> $update->callback_query->message->message_id,
				'reply_markup'	=> json_encode(['inline_keyboard' => $menu])
			]);
		} catch(TelegramBot\TelegramException $e) {
			//call_error($e->getMessage());
		}
		die;
	} elseif (strpos($update->callback_query->data, 'editShop_') === 0) {
		$file = "/home/masterpoll-documents/shop.json";
		if (file_exists($file)) {
			$shop = json_decode(file_get_contents($file), true);
		} else {
			$shop = [];
		}
		$e = [
			true	=> false,
			false	=> true,
			null	=> true
		];
		$c = [
			true	=> "✅",
			false	=> "💤"
		];
		$expl = explode("-", str_replace("editShop_", '', $update->callback_query->data));
		$type = $expl[0];
		$set = round($expl[1]);
		if ($type == "all") {
			foreach ($shop as $m => $s) {
				$shop[$m]['available'] = $set;
			}
		} else {
			$shop[$type]['available'] = $set;
		}
		file_put_contents($file, json_encode($shop, JSON_PRETTY_PRINT));
		foreach ($shop as $type => $active) {
			$active = $active['available'];
			$es = $e[$active];
			$emo = $c[$active];
			$menu[] = [
				[
					"text" => "$type $emo",
					"callback_data" => "editShop_$type-$es"
				]
			];
		}
		$menu[] = [
			[
				"text" => "⬇️ Off",
				"callback_data" => "editShop_all-0"
			],
			[
				"text" => "⬆️ On",
				"callback_data" => "editShop_all-1"
			]
		];
		$menu[] = [
			[
				"text" => "✅ Fatto",
				"callback_data" => "close"
			]
		];
		$bot->answerCallbackQuery([
			'callback_query_id' => $update->callback_query->id,
			'text' => ''
		]);
		try {
			$bot->editMessageReplyMarkup([
				'chat_id'		=> $update->callback_query->message->chat->id,
				'message_id'	=> $update->callback_query->message->message_id,
				'reply_markup'	=> json_encode(['inline_keyboard' => $menu])
			]);
		} catch(TelegramBot\TelegramException $e) {
			//call_error($e->getMessage());
		}
		die;
	}
}

?>
