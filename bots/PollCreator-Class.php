<?php

$classwork = true;

function getPollEmoji ($type = "null") {
	if ($type == "vote") {
		return "📊";
	} elseif ($type == "doodle") {
		return "📊";
	} elseif ($type == "limited doodle") {
		return "📊";
	} elseif ($type == "board") {
		return "📝";
	} elseif ($type == "participation") {
		return "🗳";
	} elseif ($type == "quiz") {
		return "🎲";
	} elseif ($type == "rating") {
		return "⭐️";
	} elseif ($type == "moderate") {
		return "✏️";
	} else {
		return "🤖";
	}
}

function do_id($ids = []) {
	if ($ids) {
		foreach ($ids as $a) {
			$allids[] = $a['id'];
		}
		return $allids;
	} else {
		return [];
	}
}

function insertMessage($p = false, $msgID, $type = false, $chat = false) {
	global $botID;
	if (!isset($botID)) {
		call_error("<b>Warning:</b> la variabile \$botID non è presente nella funzione insertMessage");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$botID was not found"];
	}
	if ($p === false) {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione insertMessage");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
		$np = sendPoll($p['poll_id'], $p['creator']);
		if ($np['ok']) {
			$p = $np['result'];
		} else {
			if (isset($p['method'])) {
				$np['method']["insertMessage"] = $np['method'];
			} else {
				$np['method'] = "insertMessage";
			}
			return $np;
		}
	} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
	} else {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione insertMessage");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p are invalid"];
	}
	if (!isset($p['status']) ) {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll not found"];
	} elseif ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted"];
	}
	if (!in_array($msgID, do_id($p['messages'][$botID]))) {
		$new = [
			"id" => $msgID,
			"type" => $type
		];
		if (is_numeric($chat)) $new['chat'] = $chat;
		$p['messages'][$botID][] = $new;
	} else {
		$new = [];
		foreach($p['messages'][$botID] as $m) {
			if ($msgID == $m['id'] and $chat == $m['chat']) {
				$new = [
					"id" => $msgID,
					"type" => $type
				];
				if (is_numeric($chat)) $new['chat'] = $chat;
			} else {
				$new = [
					"id" => $m['id'],
					"type" => $m['type']
				];
				if (is_numeric($m['chat'])) $new['chat'] = $m['chat'];
			}
			if ($new) $newarray[] = $new;
		}
		$p['messages'][$botID] = $newarray;
	}
	db_query("UPDATE polls SET messages = ? WHERE poll_id = ? and user_id = ?", [json_encode($p['messages']), $p['poll_id'], $p['creator']], "no");
	return ['ok' => true, 'result' => true];
}

function pollToMessage ($poll = false, $admin = false, $moptions = []) {
	global $config;
	global $isadmin;
	$menu = false;
	if ($config['devmode']) {
		//$r = "ℹ️ Beta Testing | " . date("c") . "\n";
	}
	if (!isset($poll)) {
		cell_error("<b>Warning:</b> la variabile \$poll non è stata settata nella funzione pollToMessage");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$poll was not found", 'text' => "🤖 Bot error..."];
	}
	if (!is_array($poll)) {
		cell_error("<b>Warning:</b> la variabile \$poll non è un array nella funzione pollToMessage");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$poll was not found", 'text' => "🤖 Bot error..."];
	}
	if (isset($poll['settings']['web_content'])) {
		$allegat = 0;
		$r .= "<a href='" . $poll['settings']['web_content'] . "'>&#8203;</>";
	} elseif (isset($poll['settings']['web_page'])) {
		$allegat = $poll['settings']['web_page'];
	} else {
		$allegat = $config['disabilita_anteprima_link'];
	}
	$poll_id = round($poll['poll_id']);
	$creator = round($poll['creator']);
	if (isset($poll['settings']['language'])) {
		$lang = $poll['settings']['language'];
	} else {
		$lang = getLanguage($creator);
	}
	if (!isset($moptions['lang'])) {
		$moptions['lang'] = $lang;
	}
	if (!$poll['type'] or is_array($poll['type'])) {
		$poll['type'] = "vote";
	}
	if ($admin === "moderate") {
		$emoji = getPollEmoji('moderate') . " ";
	} else {
		if ($poll['type']) {
			$emoji = getPollEmoji($poll['type']) . " ";
		} else {
			$emoji = "🤖 ";
		}
	}
	if (empty($allegat)) $poll['disable_web_preview'] = true;
	if ($poll['status'] == "deleted") {
		$r .= getTranslate('voteDoesntExist', false, $moptions['lang']);
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif (!$poll['status']) {
		//call_error("<b>Warning:</b> il sondaggio $poll_id-$creator non è stato trovato nella funzione pollToMessage");
		$r .= getTranslate('voteDoesntExist', false, $moptions['lang']);
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif ($poll['status'] == "closed") {
		if ($poll['type'] == "quiz") {
			if ($admin === 'moderate') return ['ok' => false, 'error_code' => 401, "Access denied: can't moderate participation poll"];
			$r .= $emoji . bold($poll['title']) . "\n";
			if ($poll['description']) $r .= htmlspecialchars($poll['description']) . "\n";
			$percentage = getPollPercentage($poll);
			if ($percentage['ok']) {
				$percentage = $percentage['result'];
			} else {
				return $percentage;
			}
			if (!is_array($poll['settings']['right_answers'])) {
				return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: right answer not found", 'text' => false, 'menu' => false, 'disable_web_preview' => $allegat];
			}
			foreach($poll['settings']['right_answers'] as $ra) {
				if (!in_array($ra, array_keys($poll['choice']))) {
					return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: right answer not exists", 'text' => false, 'menu' => false, 'disable_web_preview' => $allegat];
				}
			}
			foreach($poll['choice'] as $key => $value) {
				if ($value == "NAN") $value = 0;
				if (in_array($key, $poll['settings']['right_answers'])) {
					$emo = "✅";
				} else {
					$emo = "❌";
				}
				if (!$poll['anonymous']) {
					$fig = [
						"start" => "┌",
						"line" => "┆",
						"list" => "├",
						"last" => "└"
					];
					if ($poll['choice'][$key]) {
						$r .= "\n" . $fig['start'] . " " . bold($key) . " [" . $value . "] $emo\n";
					} else {
						$r .= "\n" . bold($key) . " [" . $value . "] $emo\n";
					}
				} else {
					$r .= "\n" . bold($key) . " [" . $value . "] $emo\n";
				}
				if ($poll['settings']['bars'] == "dot" and $poll['usersvotes'][$key]) {
					if (!$poll['anonymous']) $r .= $fig['line'];
					$r .= bars($percentage[0][$key], "dot") . " (" . nn($percentage[0][$key]) . "%) \n";
				} elseif ($poll['settings']['bars'] == "like" and $value !== 0) {
					if (!$poll['anonymous']) $r .= $fig['line'];
					$r .= bars($percentage[0][$key], "like") . " (" . nn($percentage[0][$key]) . "%) \n";
				}
				if (!$poll['anonymous']) {
					$ids = array_values($poll['usersvotes'][$key]);
					if (count($ids) === 1) {
						$r .= $fig['last'] . " " . getName($ids[0])['result'] . "\n";
					} elseif (count($ids) === 0) {
					} else {
						unset($die);
						unset($nums);
						$nums = range(0, count($ids) - 1);
						foreach ($nums as $num) {
							$id = $ids[$num];
							$name = getName($id)['result'];
							if ($die) {
								$r .= $fig['last'] . " " . $name . "\n";
							} else {
								$r .= $fig['list'] . " " . $name . "\n";
								if ($id == $ids[count($ids) - 2])$die = true;
							}
						}
					}
				}
			}
			if ($admin) {
				$menu[] = [
					[
						"text" => getTranslate('commPageOptions', false, $moptions['lang']),
						"callback_data" => "/option $poll_id-$creator"
					],
					[
						"text" => getTranslate('commPageReopen', false, $moptions['lang']),
						"callback_data" => "popen_$poll_id-$creator"
					],
					[
						"text" => getTranslate('commPageDelete', false, $moptions['lang']),
						"callback_data" => "delete_ $poll_id-$creator"
					]
				];
			} else {
				if (!$poll['anonymous'] and !$poll['settings']['hide_voters']) {
					$menu[] = [
						[
							"text" => getTranslate('boardShowMore', false, $lang),
							"callback_data" => "cburl-" . bot_encode("list_" . $poll['poll_id'] . "-" . $poll['creator'])
						]
					];
				}
			}
			unset($numero);
			if ($poll['settings']['max_voters']) {
				$limited = "Limited";
			} else {
				$limited = false;
			}
			if ($poll['votes'] === 0) {
				$r .= "\n" . getTranslate('renderer' . $limited . 'ZeroVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
			} elseif ($poll['votes'] === 1) {
				$r .= "\n" . getTranslate('renderer' . $limited . 'SingleVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
			} else {
				$r .= "\n" . getTranslate('renderer' . $limited . 'MultiVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
			}
			if (count($poll['settings']['right_answers']) > 1) {
				$max = count($poll['settings']['right_answers']);
				$r .= "\nℹ️ " . getTranslate('multiResponseQuizYouCanChooseSoMany', [$max], $lang);
			}
			if ($poll['anonymous']) {
				$r .= "\n📖 " . italic(getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['type']), false, $lang));
			} else {
				$r .= "\n📖 " . italic(getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['type']), false, $lang));
			}
		} elseif ($poll['type'] == "board") {
			$r .= $emoji . bold($poll['title']) . "\n";
			if ($poll['description']) $r .= htmlspecialchars($poll['description']) . "\n";
			if ($poll['choice']) {
				if (count($poll['choice']) <= 500) {
					if ($admin) {
						if (!isset($moptions['board_page'])) $moptions['board_page'] = 1;
						$tcomments = "";
						$cmpage = 1;
						foreach($poll['choice'] as $user_id => $text) {
							$numusers = $numusers + 1;
							if ($cmpage == $moptions['board_page']) {
								if (!isset($tpf)) $tpf = $numusers;
							}
							if (strlen($tcomments) <= 2560 or !isset($tcomments)) {
								if ($poll['anonymous']) {
									$name = "👤";
								} else {
									$name = htmlspecialchars_decode(getName($user_id)['result']) . ":";
								}
								if ($admin === 'moderate') {
									$tcomments .= "\n" . bold($name) . " " . code($text) . "\n";
									$tcomments .= "/delete_" . str_replace('=', '', bot_encode("$poll_id-$creator-$user_id")) . "\n";
								} else {
									$tcomments .= "\n" . bold($name) . htmlspecialchars(" $text \n");
								}
							} else {
								$otherpages = true;
								if (isset($moptions['board_page'])) {
									if ($admin === "moderate") {
										if ($moptions['board_page'] == $cmpage) {
											$smenup[] = [
												"text" => "• $cmpage •",
												"callback_data" => "moderate_$poll_id-$creator-$cmpage"
											];
										} elseif ($cmpage < ($moptions['board_page'] + 3) and $moptions['board_page'] < $cmpage) {
											$smenup[] = [
												"text" => "• $cmpage »",
												"callback_data" => "moderate_$poll_id-$creator-$cmpage"
											];
										} elseif ($cmpage > ($moptions['board_page'] - 3) and $moptions['board_page'] > $cmpage) {
											$smenup[] = [
												"text" => "« $cmpage •",
												"callback_data" => "moderate_$poll_id-$creator-$cmpage"
											];
										}
									}
									if (is_numeric($moptions['board_page'])) {
										if ($moptions['board_page'] == $cmpage) {
											$tpu = $numusers -1;
											$r .= $tcomments;
										}
										unset($tcomments);
										if (($cmpage + 1) == $moptions['board_page']) $tpf = $numusers;
										if ($poll['anonymous']) {
											$name = "👤";
										} else {
											$name = htmlspecialchars_decode(getName($user_id)['result']) . ":";
										}
										if ($admin === 'moderate') {
											$tcomments .= "\n" . bold($name) . " " . code($text) . "\n";
											$tcomments .= "/delete_" . str_replace('=', '', bot_encode("$poll_id-$creator-$user_id")) . "\n";
										} else {
											$tcomments .= "\n" . bold($name) . htmlspecialchars(" $text \n");
										}
									}
								}
								$cmpage = $cmpage + 1;
							}
						}
						if (strlen($tcomments) <= 2560) {
							if ($admin === "moderate") {
								if ($moptions['board_page'] == $cmpage) {
									$smenup[] = [
										"text" => "• $cmpage •",
										"callback_data" => "moderate_$poll_id-$creator-$cmpage"
									];
								} elseif ($cmpage < ($moptions['board_page'] + 3) and $moptions['board_page'] < $cmpage) {
									$smenup[] = [
										"text" => "• $cmpage »",
										"callback_data" => "moderate_$poll_id-$creator-$cmpage"
									];
								} elseif ($cmpage > ($moptions['board_page'] - 3) and $moptions['board_page'] > $cmpage) {
									$smenup[] = [
										"text" => "« $cmpage •",
										"callback_data" => "moderate_$poll_id-$creator-$cmpage"
									];
								}
							}
							if ($moptions['board_page'] == $cmpage) {
								$r .= $tcomments;
							}
							if (!isset($tpu)) $tpu = $numusers;
						}
						if (isset($smenup)) $menu[] = $smenup;
						$moptions['board_page_count'] = $cmpage;
						if ($moptions['board_page'] > $moptions['board_page_count']) {
							$moptions['board_page'] = $moptions['board_page_count'];
						}
					} elseif ($poll['settings']['hide_voters'] == false) {
						if (!isset($moptions['board_page'])) $moptions['board_page'] = 1;
						$tcomments = "";
						$cmpage = 1;
						foreach($poll['choice'] as $user_id => $text) {
							$numusers = $numusers + 1;
							if ($cmpage == $moptions['board_page']) {
								if (!isset($tpf)) $tpf = $numusers;
							}
							if (strlen($tcomments) <= 2560 or !isset($tcomments)) {
								if ($poll['anonymous']) {
									$name = "👤";
								} else {
									$name = htmlspecialchars_decode(getName($user_id)['result']) . ":";
								}
								if ($admin === 'moderate') {
									$tcomments .= "\n" . bold($name) . " " . code($text) . "\n";
									$tcomments .= "/delete_" . str_replace('=', '', bot_encode("$poll_id-$creator-$user_id")) . "\n";
								} else {
									$tcomments .= "\n" . bold($name) . htmlspecialchars(" $text \n");
								}
							} else {
								$otherpages = true;
								if (isset($moptions['board_page'])) {
									if ($admin === "moderate") {
										if ($moptions['board_page'] == $cmpage) {
											$smenup[] = [
												"text" => "• $cmpage •",
												"callback_data" => "moderate_$poll_id-$creator-$cmpage"
											];
										} elseif ($cmpage < ($moptions['board_page'] + 3) and $moptions['board_page'] < $cmpage) {
											$smenup[] = [
												"text" => "• $cmpage »",
												"callback_data" => "moderate_$poll_id-$creator-$cmpage"
											];
										} elseif ($cmpage > ($moptions['board_page'] - 3) and $moptions['board_page'] > $cmpage) {
											$smenup[] = [
												"text" => "« $cmpage •",
												"callback_data" => "moderate_$poll_id-$creator-$cmpage"
											];
										}
									}
									if (is_numeric($moptions['board_page'])) {
										if ($moptions['board_page'] == $cmpage) {
											$tpu = $numusers -1;
											$r .= $tcomments;
										}
										unset($tcomments);
										if (($cmpage + 1) == $moptions['board_page']) $tpf = $numusers;
										if ($poll['anonymous']) {
											$name = "👤";
										} else {
											$name = htmlspecialchars_decode(getName($user_id)['result']) . ":";
										}
										if ($admin === 'moderate') {
											$tcomments .= "\n" . bold($name) . " " . code($text) . "\n";
											$tcomments .= "/delete_" . str_replace('=', '', bot_encode("$poll_id-$creator-$user_id")) . "\n";
										} else {
											$tcomments .= "\n" . bold($name) . htmlspecialchars(" $text \n");
										}
									}
								}
								$cmpage = $cmpage + 1;
							}
						}
						if (strlen($tcomments) <= 2560) {
							if ($admin === "moderate") {
								if ($moptions['board_page'] == $cmpage) {
									$smenup[] = [
										"text" => "• $cmpage •",
										"callback_data" => "moderate_$poll_id-$creator-$cmpage"
									];
								} elseif ($cmpage < ($moptions['board_page'] + 3) and $moptions['board_page'] < $cmpage) {
									$smenup[] = [
										"text" => "• $cmpage »",
										"callback_data" => "moderate_$poll_id-$creator-$cmpage"
									];
								} elseif ($cmpage > ($moptions['board_page'] - 3) and $moptions['board_page'] > $cmpage) {
									$smenup[] = [
										"text" => "« $cmpage •",
										"callback_data" => "moderate_$poll_id-$creator-$cmpage"
									];
								}
							}
							if ($moptions['board_page'] == $cmpage) {
								$r .= $tcomments;
							}
							if (!isset($tpu)) $tpu = $numusers;
						}
						if (isset($smenup)) $menu[] = $smenup;
						$moptions['board_page_count'] = $cmpage;
						if ($moptions['board_page'] > $moptions['board_page_count']) {
							$moptions['board_page'] = $moptions['board_page_count'];
						}
					}
				} else {
					$r .= italic("\nToo many comments to display here...\n");
				}
			}
			if ($admin === 'moderate') {
				$menu[] = [
					[
						"text" => "💾 " . getTranslate('done', false, $moptions['lang']), 
						"callback_data" => "/option $poll_id-$creator"
					]
				];
			} elseif ($admin) {
				$menu[0] = [
					[
						"text" => getTranslate('commPageOptions', false, $moptions['lang']), 
						"callback_data" => "/option $poll_id-$creator"
					],
					[
						"text" => getTranslate('commPageReopen', false, $moptions['lang']), 
						"callback_data" => "popen_$poll_id-$creator"
					],
					[
						"text" => getTranslate('commPageDelete', false, $moptions['lang']),
						"callback_data" => "delete_ $poll_id-$creator"
					]
				];
			}
			if ($poll['settings']['max_voters']) {
				$limited = "Limited";
			} else {
				$limited = false;
			}
			if ($poll['votes'] === 0) {
				$r .= "\n" . getTranslate('renderer' . $limited . 'ZeroVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
			} elseif ($poll['votes'] === 1) {
				$r .= "\n" . getTranslate('renderer' . $limited . 'SingleVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
			} else {
				$r .= "\n" . getTranslate('renderer' . $limited . 'MultiVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
			}
			if ($poll['anonymous']) {
				$r .= "\n📖 " . italic(getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['type']), false, $lang));
			} else {
				$r .= "\n📖 " . italic(getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['type']), false, $lang));
			}
		} elseif ($poll['type'] == "participation") {
			if ($admin === 'moderate') return ['ok' => false, 'error_code' => 401, "Access denied: can't moderate participation poll"];
			$r .= $emoji . bold($poll['title']) . "\n";
			if ($poll['description']) $r .= htmlspecialchars($poll['description']) . "\n";
			if ($admin) {
				$menu[0] = [
					[
						"text" => getTranslate('commPageOptions', false, $moptions['lang']), 
						"callback_data" => "/option $poll_id-$creator"
					],
					[
						"text" => getTranslate('commPageReopen', false, $moptions['lang']), 
						"callback_data" => "popen_$poll_id-$creator"
					],
					[
						"text" => getTranslate('commPageDelete', false, $moptions['lang']),
						"callback_data" => "delete_ $poll_id-$creator"
					]
				];
			}
			if ($poll['settings']['max_voters']) {
				$limited = "Limited";
			} else {
				$limited = false;
			}
			if ($poll['votes'] === 0) {
				$r .= "\n" . getTranslate('renderer' . $limited . 'ZeroVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
			} elseif ($poll['votes'] === 1) {
				$r .= "\n" . getTranslate('renderer' . $limited . 'SingleVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
			} else {
				$r .= "\n" . getTranslate('renderer' . $limited . 'MultiVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
			}
			if ($poll['anonymous']) {
				$r .= "\n📖 " . italic(getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['type']), false, $lang));
			} else {
				$r .= "\n📖 " . italic(getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['type']), false, $lang));
			}
		} elseif ($poll['type'] == "rating") {
			if ($admin === 'moderate') return ['ok' => false, 'error_code' => 401, "Access denied: can't moderate rating poll"];
			$r .= $emoji . bold($poll['title']) . "\n";
			if ($poll['description']) $r .= htmlspecialchars($poll['description']) . "\n";
			$percentage = getPollPercentage($poll);
			if ($percentage['ok']) {
				$percentage = $percentage['result'];
			} else {
				return $percentage;
			}
			foreach($poll['choice'] as $key => $value) {
				if ($value == "NAN") $value = 0;
				if (!isset($min_val)) $min_val = $key;
				$all = $all + ($key * $value);
				$emoji = "⭐️";
				if (isset($numero)) {$numero = $numero + 1; } else {$numero = 0; }
				if (!$poll['anonymous'] and $admin !== false) {
					$fig = [
						"start" => "┌",
						"line" => "┆",
						"list" => "├",
						"last" => "└"
					];
					if ($poll['choice'][$key]) {
						$r .= "\n" . $fig['start'] . " " . bold($key) . " [" . $value . "]\n";
					} else {
						$r .= "\n" . bold($key) . " [" . $value . "]\n";
					}
				}
				if ($admin) {
					if ($poll['settings']['bars'] == "dot" and $poll['usersvotes'][$key]) {
						if (!$poll['anonymous']) $r .= $fig['line'];
						$r .= bars($percentage[0][$key], "dot") . " (" . nn($percentage[0][$key]) . "%) \n";
					} elseif ($poll['settings']['bars'] == "like" and $value !== 0) {
						if (!$poll['anonymous']) $r .= $fig['line'];
						$r .= bars($percentage[0][$key], "like") . " (" . nn($percentage[0][$key]) . "%) \n";
					}
				}
				if (!$poll['anonymous'] and $admin !== false) {
					$ids = array_values($poll['usersvotes'][$key]);
					if (count($ids) === 1) {
						$r .= $fig['last'] . " " . getName($ids[0])['result'] . "\n";
					} elseif (count($ids) === 0) {
					} else {
						unset($die);
						unset($nums);
						$nums = range(0, count($ids) - 1);
						foreach ($nums as $num) {
							$id = $ids[$num];
							$name = getName($id)['result'];
							if ($die) {
								$r .= $fig['last'] . " " . $name . "\n";
							} else {
								$r .= $fig['list'] . " " . $name . "\n";
								if ($id == $ids[count($ids) - 2])$die = true;
							}
						}
					}
				}
			}
			unset($numero);
			if ($admin) {
				$menu[] = [
					[
						"text" => getTranslate('commPageOptions', false, $moptions['lang']),
						"callback_data" => "/option $poll_id-$creator"
					],
					[
						"text" => getTranslate('commPageReopen', false, $moptions['lang']),
						"callback_data" => "popen_$poll_id-$creator"
					],
					[
						"text" => getTranslate('commPageDelete', false, $moptions['lang']),
						"callback_data" => "delete_ $poll_id-$creator"
					]
				];
			} else {
				foreach($percentage[0] as $key => $value) {
					if (isset($num)) {
						if (count($menu[$num]) >= 5) $num = $num + 1;
					} else {
						$num = 0;
					}
					if (isset($numero)) {
						$numero = $numero + 1;
					} else {
						$numero = 0;
					}
					if ($value == "NAN") $value = 0;
				}
			}
			if ($poll['settings']['max_voters']) {
				$limited = "Limited";
			} else {
				$limited = false;
			}
			if ($poll['votes'] === 0) {
				$r .= "\n" . getTranslate('renderer' . $limited . 'ZeroVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
			} elseif ($poll['votes'] === 1) {
				$r .= "\n" . getTranslate('renderer' . $limited . 'SingleVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
			} else {
				$r .= "\n" . getTranslate('renderer' . $limited . 'MultiVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
			}
			if ($poll['anonymous']) {
				$r .= "\n📖 " . italic(getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['type']), false, $lang));
			} else {
				$r .= "\n📖 " . italic(getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['type']), false, $lang));
			}
		} else {
			$r .= $emoji . bold($poll['title']) . "\n";
			if ($poll['description']) $r .= htmlspecialchars($poll['description']) . "\n";
			$percentage = getPollPercentage($poll);
			if ($percentage['ok']) {
				$percentage = $percentage['result'];
			} else {
				return $percentage;
			}
			if (count($poll['choice']) === 1) {
				unset($poll['settings']['bars']);
			} else {
				if ($poll['settings']['sort'] and $admin !== 'moderate') {
					$sort = sorta($poll['choice']);
					if ($sort['ok']) {
						$poll['choice'] = $sort['result'];
					} else {
						return $sort;
					}
				}
			}
			foreach($poll['choice'] as $key => $value) {
				if ($value == "NAN") $value = 0;
				if (isset($numero)) {$numero = $numero + 1; } else {$numero = 0; }
				if (!isset($poll['settings']['in_options']) or $poll['settings']['in_options'] == 2) {
					$int = " [" . $value . "]";
				} elseif($poll['settings']['in_options'] == 1) {
					$int = " [" . $percentage[0][$key] . "%]";
				}
				if (!$poll['anonymous'] and $admin !== 'moderate' and !$poll['settings']['hide_voters'] and $poll['usersvotes'][$key]) {
					if ($poll['settings']['group'] == 1) {
						$fig = [
							"start" => "┌",
							"line" => "┆",
							"list" => "├",
							"last" => "└"
						];
					} elseif ($poll['settings']['group'] == 2) {
						$fig = [
							"start" => "╔",
							"line" => "|",
							"list" => "╠",
							"last" => "╚"
						];
					} elseif ($poll['settings']['group'] == "no") {
						$fig = [
							"start" => "",
							"line" => "",
							"list" => "-",
							"last" => "-"
						];
					} else {
						$fig = [
							"start" => "┌",
							"line" => "┆",
							"list" => "├",
							"last" => "└"
						];
					}
					$r .= "\n" . $fig['start'] . " " . bold($key) . " $int\n";
				} else {
					$r .= "\n" . bold($key) . " $int\n";
				}
				if ($poll['settings']['bars'] == "dot" and $admin !== 'moderate' and $poll['usersvotes'][$key]) {
					if (!$poll['anonymous'] and $admin !== 'moderate') $r .= $fig['line'];
					if ($poll['settings']['in_options'] == 2) {
						$perc = " (" . nn($percentage[0][$key]) . "%)";
					} elseif ($poll['settings']['in_options'] == 1) {
						$perc = " (" . $value . ")";
					}
					$r .= bars($percentage[0][$key], "dot") . $perc . "\n";
				} elseif ($poll['settings']['bars'] == "like" and $value !== 0) {
					if (!$poll['anonymous'] and $admin !== 'moderate') $r .= $fig['line'];
					if ($poll['settings']['in_options'] == 2) {
						$perc = " (" . nn($percentage[0][$key]) . "%)";
					} elseif ($poll['settings']['in_options'] == 1) {
						$perc = " (" . $value . ")";
					}
					$r .= bars($percentage[0][$key], "like") . $perc . "\n";
					if ($admin === 'moderate') $r .= "/delete_" . str_replace('=', '', bot_encode("$poll_id-$creator-$numero")) . "\n";
				} else {
					if ($admin === 'moderate') $r .= "/delete_" . str_replace('=', '', bot_encode("$poll_id-$creator-$numero")) . "\n";
				}
				if (!$poll['anonymous'] and $poll['usersvotes'][$key]) {
					if ($admin === 'moderate') {
						// No user show for moderation of options
					} elseif ($poll['settings']['hide_voters']) {
						// Hide voters
					} else {
						$ids = array_values($poll['usersvotes'][$key]);
						if (count($ids) === 1) {
							$r .= $fig['last'] . " " . getName($ids[0])['result'] . "\n";
						} else {
							unset($die);
							unset($nums);
							$nums = range(0, count($ids) - 1);
							foreach ($nums as $num) {
								$id = $ids[$num];
								$name = getName($id)['result'];
								if ($die) {
									$r .= $fig['last'] . " " . $name . "\n";
								} else {
									$r .= $fig['list'] . " " . $name . "\n";
									if ($id == $ids[count($ids) - 2])$die = true;
								}
							}
						}
					}
				}
			}
			unset($numero);
			if (isset($ishided) and $poll['votes'] > 100) $poll['settings']['hide_voters'] = false;
			if ($admin === 'moderate') {
				$menu[] = [
					[
						"text" => getTranslate('buttonAppend', false, $moptions['lang']),
						"callback_data" => "cburl-" . bot_encode("append_$poll_id-$creator")
					]
				];
				$menu[] = [
					[
						"text" => "💾 " . getTranslate('done', false, $moptions['lang']),
						"callback_data" => "coptions_$poll_id-$creator-votes"
					]
				];
			} elseif ($admin) {
				$menu[0] = [
					[
						"text" => getTranslate('commPageOptions', false, $moptions['lang']),
						"callback_data" => "/option $poll_id-$creator"
					],
					[
						"text" => getTranslate('commPageReopen', false, $moptions['lang']),
						"callback_data" => "popen_$poll_id-$creator"
					],
					[
						"text" => getTranslate('commPageDelete', false, $moptions['lang']),
						"callback_data" => "delete_ $poll_id-$creator"
					]
				];
			} else {
				if (!$poll['anonymous'] and !$poll['settings']['hide_voters']) {
					$menu[] = [
						[
							"text" => getTranslate('boardShowMore', false, $lang),
							"callback_data" => "cburl-" . bot_encode("list_" . $poll['poll_id'] . "-" . $poll['creator'])
						]
					];
				}
			}
			if ($poll['settings']['max_voters']) {
				$limited = "Limited";
			} else {
				$limited = false;
			}
			if (isset($poll['votes'])) {
				if ($poll['votes'] > 100 and !$poll['settings']['hide_voters']) {
					$poll['settings']['hide_voters'] = true;
					$ishided = true;
				}
			}
			if ($poll['type'] == "limited doodle") {
				$total = count($poll['choice']);
				$max = $poll['settings']['max_choices'];
				$r .= "\nℹ️ " . getTranslate('limitedDoodleYouCanChooseSoMany', [$max, $total], $lang);
			}
			if ($poll['anonymous']) {
				$r .= "\n📖 " . italic(getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['type']), false, $lang));
			} else {
				$r .= "\n📖 " . italic(getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['type']), false, $lang));
			}
		}
		if ($admin === 'moderate') {} else {
			$r .= "\n\n" . getTranslate('pollClosed', false, $lang);
		}
		return ['ok' => true, 'text' => $r, 'menu' => $menu, 'disable_web_preview' => $allegat, 'options' => $moptions];
	} elseif ($poll['type'] == "quiz") {
		$r .= $emoji . bold($poll['title']) . "\n";
		if ($poll['description']) $r .= htmlspecialchars($poll['description']) . "\n";
		$percentage = getPollPercentage($poll);
		if ($percentage['ok']) {
			$percentage = $percentage['result'];
		} else {
			return $percentage;
		}
		foreach ($poll['choice'] as $choice => $users) {
			$r .= "\n" . bold($choice) . "\n";
		}
		if ($admin) {
			if ($admin === true) {
				$publish = $poll['poll_id'];
				$wlpublish = '$c:' . $poll['poll_id'];
			} else {
				$publish = "share " . bot_encode("$poll_id-$creator");
				$wlpublish = "share " . bot_encode("$poll_id-$creator");
			}
			$menu[] = [
				[
					"text" => getTranslate('publish', false, $moptions['lang']),
					"switch_inline_query" => $publish
				]
			];
			$menu[] = [
				[
					"text" => getTranslate('publishWithLink', false, $moptions['lang']),
					"switch_inline_query" => $wlpublish
				],
				[
					"text" => getTranslate('sendChat', false, $moptions['lang']),
					"callback_data" => "psend_$poll_id-$creator"
				]
			];
			$menu[] = [
				[
					"text" => getTranslate('buttonVote', false, $moptions['lang']),
					"callback_data" => "vote_$poll_id-$creator"
				],
				[
					"text" => getTranslate('commPageRefresh', false, $moptions['lang']),
					"callback_data" => "update_$poll_id-$creator-update"
				]
			];
			$menu[] = [
				[
					"text" => getTranslate('commPageOptions', false, $moptions['lang']),
					"callback_data" => "/option $poll_id-$creator"
				],
				[
					"text" => getTranslate('commPageClose', false, $moptions['lang']),
					"callback_data" => "pclose_$poll_id-$creator"
				],
				[
					"text" => getTranslate('commPageDelete', false, $moptions['lang']),
					"callback_data" => "delete_ $poll_id-$creator"
				]
			];
		} else {
			if (isset($percentage[0])) {
				unset($num);
				if (isset($poll['settings']['menu_strings'])) {
					$totstr = $poll['settings']['menu_strings'];
				} else {
					$totstr = 1;
				}
				foreach($percentage[0] as $key => $value) {
					if (isset($num)) {
						if (count($menu[$num]) >= $totstr) $num = $num + 1;
					} else {
						$num = 0;
					}
					if (isset($numero)) {
						$numero = $numero + 1;
					} else {
						$numero = 0;
					}
					if ($value == "NAN") $value = 0;
					if ($poll['settings']['percentage'] == "1") {
						$button = $key;
					} elseif ($poll['settings']['percentage'] == "2") {
						$button = $value ;
					} else {
						$button = $key;
					}
					$menu[$num][] = [
						'text' => $button, 
						'callback_data' => "v:$poll_id-$creator-$numero"
					]; 
				}
			}
			if (!isset($poll['settings']['sharable'])) {
				$poll['settings']['sharable'] = false;
			}
			if ($poll['settings']['sharable']) {
				$menu[] = [
					[
						"text" => getTranslate('share', false, $lang), 
						"callback_data" => "cburl-" . bot_encode("share_" . $poll['poll_id'] . "-" . $poll['creator'])
					]
				];
			}
		}
		if ($poll['settings']['max_voters']) {
			$limited = "Limited";
		} else {
			$limited = false;
		}
		if ($poll['votes'] === 0) {
			$r .= "\n" . getTranslate('renderer' . $limited . 'ZeroVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
		} elseif ($poll['votes'] === 1) {
			$r .= "\n" . getTranslate('renderer' . $limited . 'SingleVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
		} else {
			$r .= "\n" . getTranslate('renderer' . $limited . 'MultiVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
		}
		if (count($poll['settings']['right_answers']) > 1) {
			$max = count($poll['settings']['right_answers']);
			$r .= "\nℹ️ " . getTranslate('multiResponseQuizYouCanChooseSoMany', [$max], $lang);
		}
		if ($poll['anonymous']) {
			$r .= "\n📖 " . italic(getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['type']), false, $lang));
		} else {
			$r .= "\n📖 " . italic(getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['type']), false, $lang));
		}
		$r .= "<a href='https://web.masterpoll.xyz/assets/tgBotWebPreviews.example.php'>&#8203;</><a href='http://t.me/noautopinbot?start=pinned'>&#8203;</>";
		return ['ok' => true, 'text' => $r, 'menu' => $menu, 'disable_web_preview' => $allegat, 'moptions' => $moptions];
	} elseif ($poll['type'] == "board") {
		$r .= $emoji . bold($poll['title']) . "\n";
		if ($poll['description']) $r .= htmlspecialchars($poll['description']) . "\n";
		if ($poll['choice']) {
			if ($admin) {
				if (!isset($moptions['board_page'])) $moptions['board_page'] = 1;
				$tcomments = "";
				$cmpage = 1;
				foreach($poll['choice'] as $user_id => $text) {
					$numusers = $numusers + 1;
					if (!isset($ufinish)) {
						if ($cmpage == $moptions['board_page']) {
							if (!isset($tpf)) $tpf = $numusers;
						}
						if (strlen($tcomments) <= 2560 or !isset($tcomments)) {
							if ($poll['anonymous']) {
								$name = "👤";
							} else {
								$name = htmlspecialchars_decode(getName($user_id)['result']) . ":";
							}
							if ($admin === 'moderate') {
								$tcomments .= "\n" . bold($name) . " " . code($text) . "\n";
								$tcomments .= "/delete_" . str_replace('=', '', bot_encode("$poll_id-$creator-$user_id")) . "\n";
							} else {
								$tcomments .= "\n" . bold($name) . htmlspecialchars(" $text \n");
							}
						} else {
							$otherpages = true;
							if (isset($moptions['board_page'])) {
								if ($admin === "moderate") {
									if ($moptions['board_page'] == $cmpage) {
										$smenup[] = [
											"text" => "• $cmpage •",
											"callback_data" => "moderate_$poll_id-$creator-$cmpage"
										];
									} elseif ($cmpage < ($moptions['board_page'] + 3) and $moptions['board_page'] < $cmpage) {
										$smenup[] = [
											"text" => "• $cmpage »",
											"callback_data" => "moderate_$poll_id-$creator-$cmpage"
										];
									} elseif ($cmpage > ($moptions['board_page'] - 3) and $moptions['board_page'] > $cmpage) {
										$smenup[] = [
											"text" => "« $cmpage •",
											"callback_data" => "moderate_$poll_id-$creator-$cmpage"
										];
									}
								}
								if (is_numeric($moptions['board_page'])) {
									if ($moptions['board_page'] == $cmpage) {
										$tpu = $numusers -1;
										$r .= $tcomments;
										$ufinish = true;
									}
									unset($tcomments);
									if (($cmpage + 1) == $moptions['board_page']) $tpf = $numusers;
									if ($poll['anonymous']) {
										$name = "👤";
									} else {
										$name = htmlspecialchars_decode(getName($user_id)['result']) . ":";
									}
									if ($admin === 'moderate') {
										$tcomments .= "\n" . bold($name) . " " . code($text) . "\n";
										$tcomments .= "/delete_" . str_replace('=', '', bot_encode("$poll_id-$creator-$user_id")) . "\n";
									} else {
										$tcomments .= "\n" . bold($name) . htmlspecialchars(" $text \n");
									}
								}
							}
							$cmpage = $cmpage + 1;
						}
					}
				}
				if (strlen($tcomments) <= 2560) {
					if ($admin === "moderate") {
						if ($moptions['board_page'] == $cmpage) {
							$smenup[] = [
								"text" => "• $cmpage •",
								"callback_data" => "moderate_$poll_id-$creator-$cmpage"
							];
						} elseif ($cmpage < ($moptions['board_page'] + 3) and $moptions['board_page'] < $cmpage) {
							$smenup[] = [
								"text" => "• $cmpage »",
								"callback_data" => "moderate_$poll_id-$creator-$cmpage"
							];
						} elseif ($cmpage > ($moptions['board_page'] - 3) and $moptions['board_page'] > $cmpage) {
							$smenup[] = [
								"text" => "« $cmpage •",
								"callback_data" => "moderate_$poll_id-$creator-$cmpage"
							];
						}
					}
					if ($moptions['board_page'] == $cmpage) {
						$r .= $tcomments;
					}
					if (!isset($tpu)) $tpu = $numusers;
				}
				if (isset($smenup)) $menu[] = $smenup;
				$moptions['board_page_count'] = $cmpage;
				if ($moptions['board_page'] > $moptions['board_page_count']) {
					$moptions['board_page'] = $moptions['board_page_count'];
				}
			} elseif ($poll['settings']['hide_voters'] == false) {
				if (!isset($moptions['board_page'])) $moptions['board_page'] = 1;
				$tcomments = "";
				$cmpage = 1;
				foreach($poll['choice'] as $user_id => $text) {
					$numusers = $numusers + 1;
					if (!isset($ufinish)) {
						if ($cmpage == $moptions['board_page']) {
							if (!isset($tpf)) $tpf = $numusers;
						}
						if (strlen($tcomments) <= 2560 or !isset($tcomments)) {
							if ($poll['anonymous']) {
								$name = "👤";
							} else {
								$name = htmlspecialchars_decode(getName($user_id)['result']) . ":";
							}
							if ($admin === 'moderate') {
								$tcomments .= "\n" . bold($name) . " " . code($text) . "\n";
								$tcomments .= "/delete_" . str_replace('=', '', bot_encode("$poll_id-$creator-$user_id")) . "\n";
							} else {
								$tcomments .= "\n" . bold($name) . htmlspecialchars(" $text \n");
							}
						} else {
							$otherpages = true;
							if (isset($moptions['board_page'])) {
								if ($admin === "moderate") {
									if ($moptions['board_page'] == $cmpage) {
										$smenup[] = [
											"text" => "• $cmpage •",
											"callback_data" => "moderate_$poll_id-$creator-$cmpage"
										];
									} elseif ($cmpage < ($moptions['board_page'] + 3) and $moptions['board_page'] < $cmpage) {
										$smenup[] = [
											"text" => "• $cmpage »",
											"callback_data" => "moderate_$poll_id-$creator-$cmpage"
										];
									} elseif ($cmpage > ($moptions['board_page'] - 3) and $moptions['board_page'] > $cmpage) {
										$smenup[] = [
											"text" => "« $cmpage •",
											"callback_data" => "moderate_$poll_id-$creator-$cmpage"
										];
									}
								}
								if (is_numeric($moptions['board_page'])) {
									if ($moptions['board_page'] == $cmpage) {
										$tpu = $numusers -1;
										$r .= $tcomments;
										$ufinish = true;
									}
									unset($tcomments);
									if (($cmpage + 1) == $moptions['board_page']) $tpf = $numusers;
									if ($poll['anonymous']) {
										$name = "👤";
									} else {
										$name = htmlspecialchars_decode(getName($user_id)['result']) . ":";
									}
									if ($admin === 'moderate') {
										$tcomments .= "\n" . bold($name) . " " . code($text) . "\n";
										$tcomments .= "/delete_" . str_replace('=', '', bot_encode("$poll_id-$creator-$user_id")) . "\n";
									} else {
										$tcomments .= "\n" . bold($name) . htmlspecialchars(" $text \n");
									}
								}
							}
							$cmpage = $cmpage + 1;
						}
					}
				}
				if (strlen($tcomments) <= 2560) {
					if ($admin === "moderate") {
						if ($moptions['board_page'] == $cmpage) {
							$smenup[] = [
								"text" => "• $cmpage •",
								"callback_data" => "moderate_$poll_id-$creator-$cmpage"
							];
						} elseif ($cmpage < ($moptions['board_page'] + 3) and $moptions['board_page'] < $cmpage) {
							$smenup[] = [
								"text" => "• $cmpage »",
								"callback_data" => "moderate_$poll_id-$creator-$cmpage"
							];
						} elseif ($cmpage > ($moptions['board_page'] - 3) and $moptions['board_page'] > $cmpage) {
							$smenup[] = [
								"text" => "« $cmpage •",
								"callback_data" => "moderate_$poll_id-$creator-$cmpage"
							];
						}
					}
					if ($moptions['board_page'] == $cmpage) {
						$r .= $tcomments;
					}
					if (!isset($tpu)) $tpu = $numusers;
				}
				if (isset($smenup)) $menu[] = $smenup;
				$moptions['board_page_count'] = $cmpage;
				if ($moptions['board_page'] > $moptions['board_page_count']) {
					$moptions['board_page'] = $moptions['board_page_count'];
				}
			}
		}
		if ($admin === 'moderate') {
			$menu[] = [
				[
					"text" => "🔄" . getTranslate('commPageRefresh', false, $moptions['lang']),
					"callback_data" => "moderate_$poll_id-$creator-" . $moptions['board_page']
				]
			];
			$menu[] = [
				[
					"text" => "💾 " . getTranslate('done', false, $moptions['lang']),
					"callback_data" => "/option $poll_id-$creator"
				]
			];
		} elseif ($admin) {
			if ($admin === true) {
				$publish = $poll['poll_id'];
			} else {
				$publish = "share " . bot_encode("$poll_id-$creator");
			}
			$menu[] = [
				[
					"text" => getTranslate('publish', false, $moptions['lang']), 
					"switch_inline_query" => $publish
				]
			];
			$menu[] = [
				[
					"text" => getTranslate('sendChat', false, $moptions['lang']),
					"callback_data" => "psend_$poll_id-$creator"
				]
			];
			$menu[] = [
				[
					"text" => getTranslate('buttonVote', false, $moptions['lang']),
					"callback_data" => "cburl-" . bot_encode("board_" . $poll['poll_id'] . "-" . $poll['creator'])
				],
				[
					"text" => getTranslate('commPageRefresh', false, $moptions['lang']),
					"callback_data" => "update_$poll_id-$creator-update"
				]
			];
			$menu[] = [
				[
					"text" => getTranslate('commPageOptions', false, $moptions['lang']),
					"callback_data" => "/option $poll_id-$creator"
				],
				[
					"text" => getTranslate('commPageClose', false, $moptions['lang']),
					"callback_data" => "pclose_$poll_id-$creator"
				],
				[
					"text" => getTranslate('commPageDelete', false, $moptions['lang']),
					"callback_data" => "delete_ $poll_id-$creator"
				]
			];
		} else {
			$menu[] = [
				[
					"text" => getTranslate('buttonVote', false, $lang),
					"callback_data" => "cburl-" . bot_encode("board_" . $poll['poll_id'] . "-" . $poll['creator'])
				],
			];
			if (isset($moptions['board_page'])) {
				if (isset($smenup)) $menu[] = $smenup;
			} elseif (isset($otherpages)) {
				$menu[] = [
					[
						"text" => getTranslate('boardShowMore', false, $lang), 
						"callback_data" => "cburl-" . bot_encode("list_" . $poll['poll_id'] . "-" . $poll['creator'])
					]
				];
			}
			if ($poll['settings']['sharable']) {
				$menu[] = [
					[
						"text" => getTranslate('share', false, $lang), 
						"callback_data" => "cburl-" . bot_encode("share_" . $poll['poll_id'] . "-" . $poll['creator'])
					],
				];
			}
		}
		if ($poll['settings']['max_voters']) {
			$limited = "Limited";
		} else {
			$limited = false;
		}
		if ($poll['votes'] === 0) {
			$r .= "\n" . getTranslate('renderer' . $limited . 'ZeroVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
		} elseif ($poll['votes'] === 1) {
			$r .= "\n" . getTranslate('renderer' . $limited . 'SingleVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
		} else {
			$r .= "\n" . getTranslate('renderer' . $limited . 'MultiVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
		}
		if (isset($otherpages)) {
			$r .= getTranslate('boardPagBottomLine', [$tpf, $tpu, $poll['votes']], $lang);
		}
		if ($poll['anonymous']) {
			$r .= "\n📖 " . italic(getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['type']), false, $lang));
		} else {
			$r .= "\n📖 " . italic(getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['type']), false, $lang));
		}
		$r .= "<a href='https://web.masterpoll.xyz/assets/tgBotWebPreviews.example.php'>&#8203;</><a href='http://t.me/noautopinbot?start=pinned'>&#8203;</>";
		return ['ok' => true, 'text' => $r, 'menu' => $menu, 'disable_web_preview' => $allegat, 'moptions' => $moptions];
	} elseif ($poll['type'] == "participation") {
		if ($admin === 'moderate') return ['ok' => false, 'error_code' => 401, "Access denied: can't moderate participation poll"];
		$r .= $emoji . bold($poll['title']) . "\n";
		if ($poll['description']) $r .= htmlspecialchars($poll['description']) . "\n";
		if ($admin) {
			if ($admin === true) {
				$publish = $poll['poll_id'];
			} else {
				$publish = "share " . bot_encode("$poll_id-$creator");
			}
			$menu[] = [
				[
					"text" => getTranslate('publish', false, $moptions['lang']), 
					"switch_inline_query" => $publish
				]
			];
			$menu[] = [
				[
					"text" => getTranslate('sendChat', false, $moptions['lang']),
					"callback_data" => "psend_$poll_id-$creator"
				]
			];
			$menu[] = [
				[
					"text" => getTranslate('buttonVote', false, $moptions['lang']),
					"callback_data" => "vote_$poll_id-$creator"
				],
				[
					"text" => getTranslate('commPageRefresh', false, $moptions['lang']),
					"callback_data" => "update_$poll_id-$creator-update"
				]
			];
			$menu[] = [
				[
					"text" => getTranslate('commPageOptions', false, $moptions['lang']),
					"callback_data" => "/option $poll_id-$creator"
				],
				[
					"text" => getTranslate('commPageClose', false, $moptions['lang']),
					"callback_data" => "pclose_$poll_id-$creator"
				],
				[
					"text" => getTranslate('commPageDelete', false, $moptions['lang']),
					"callback_data" => "delete_ $poll_id-$creator"
				]
			];
		} else {
			$menu[] = [
				[
					"text" => getTranslate('buttonParticipate', false, $lang),
					"callback_data" => "v:$poll_id-$creator-0"
				],
			];
			if ($poll['settings']['sharable']) {
				$menu[] = [
					[
						"text" => getTranslate('share', false, $lang), 
						"callback_data" => "cburl-" . bot_encode("share_" . $poll['poll_id'] . "-" . $poll['creator'])
					],
				];
			}
		}
		if ($poll['settings']['max_voters']) {
			$limited = "Limited";
		} else {
			$limited = false;
		}
		if ($poll['votes'] === 0) {
			$r .= "\n" . getTranslate('participation' . $limited . 'ZeroVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
		} elseif ($poll['votes'] === 1) {
			$r .= "\n" . getTranslate('participation' . $limited . 'SingleVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
		} else {
			$r .= "\n" . getTranslate('participation' . $limited . 'MultiVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
		}
		if ($poll['anonymous']) {
			$r .= "\n📖 " . italic(getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['type']), false, $lang));
		} else {
			$r .= "\n📖 " . italic(getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['type']), false, $lang));
		}
		$r .= "<a href='https://web.masterpoll.xyz/assets/tgBotWebPreviews.example.php'>&#8203;</><a href='http://t.me/noautopinbot?start=pinned'>&#8203;</>";
		return ['ok' => true, 'text' => $r, 'menu' => $menu, 'disable_web_preview' => $allegat];
	} elseif ($poll['type'] == "rating") {
		if ($admin === 'moderate') return ['ok' => false, 'error_code' => 401, "Access denied: can't moderate rating poll"];
		$r .= $emoji . bold($poll['title']) . "\n";
		if ($poll['description']) $r .= htmlspecialchars($poll['description']) . "\n";
		$percentage = getPollPercentage($poll);
		if ($percentage['ok']) {
			$percentage = $percentage['result'];
		} else {
			return $percentage;
		}
		if (count($poll['choice']) === 1) {
			unset($poll['settings']['bars']);
		} else {
			if ($poll['settings']['sort'] and $admin !== 'moderate') {
				$sort = sorta($poll['choice']);
				if ($sort['ok']) {
					$poll['choice'] = $sort['result'];
				} else {
					return $sort;
				}
			}
		}
		foreach($poll['choice'] as $key => $value) {
			if ($value == "NAN") $value = 0;
			if (!isset($min_val)) $min_val = $key;
			$all = $all + ($key * $value);
			if (isset($poll['settings']['custom_emoji'])) {
				$remoji = $poll['settings']['custom_emoji'];
			} else {
				$remoji = "⭐️";
			}
			if (isset($numero)) {$numero = $numero + 1; } else {$numero = 0; }
			if (!$poll['anonymous'] and $admin !== false) {
				$fig = [
					"start" => "┌",
					"line" => "┆",
					"list" => "├",
					"last" => "└"
				];
				if ($poll['choice'][$key]) {
					$r .= "\n" . $fig['start'] . " " . bold($key) . " [" . $value . "]\n";
				} else {
					$r .= "\n" . bold($key) . " [" . $value . "]\n";
				}
			}
			if ($admin) {
				if ($poll['settings']['bars'] == "dot" and $poll['usersvotes'][$key]) {
					if (!$poll['anonymous']) $r .= $fig['line'];
					$r .= bars($percentage[0][$key], "dot") . " (" . nn($percentage[0][$key]) . "%) \n";
				} elseif ($poll['settings']['bars'] == "like" and $value !== 0) {
					if (!$poll['anonymous']) $r .= $fig['line'];
					$r .= bars($percentage[0][$key], "like") . " (" . nn($percentage[0][$key]) . "%) \n";
				}
			}
			if (!$poll['anonymous'] and $admin !== false) {
				$ids = array_values($poll['usersvotes'][$key]);
				if (count($ids) === 1) {
					if (!$name[$ids[0]]) $name[$ids[0]] = getName($ids[0])['result'];
					$r .= $fig['last'] . " " . $name[$ids[0]] . "\n";
				} elseif (count($ids) === 0) {
				} else {
					unset($die);
					unset($nums);
					$nums = range(0, count($ids) - 1);
					foreach ($nums as $num) {
						$id = $ids[$num];
						if (!$name[$ids[$num]]) $name[$ids[$num]] = getName($ids[$num])['result'];
						if ($die) {
							$r .= $fig['last'] . " " . $name[$ids[$num]] . "\n";
						} else {
							$r .= $fig['list'] . " " . $name[$ids[$num]] . "\n";
							if ($id == $ids[count($ids) - 2])$die = true;
						}
					}
				}
			}
		}
		unset($numero);
		if ($poll['votes']) {
			$val = $all / $poll['votes'] / count($poll['choice']) * 5;
			if ($val == 'NAN')  {
				$val = 0;
			}
			foreach (range($min_val, explode(".", $val)[0]) as $n) {
				$stars .= $remoji;
			}
			$r .= "\n" . $stars . " [" . round($all / $poll['votes'], 1) . "] \n";
		}
		if ($admin) {
			if ($admin === true) {
				$publish = $poll['poll_id'];
			} else {
				$publish = "share " . bot_encode("$poll_id-$creator");
			}
			$menu[] = [
				[
					"text" => getTranslate('publish', false, $moptions['lang']),
					"switch_inline_query" => $publish
				]
			];
			$menu[] = [
				[
					"text" => getTranslate('sendChat', false, $moptions['lang']),
					"callback_data" => "psend_$poll_id-$creator"
				]
			];
			$menu[] = [
				[
					"text" => getTranslate('buttonVote', false, $moptions['lang']),
					"callback_data" => "vote_$poll_id-$creator"
				],
				[
					"text" => getTranslate('commPageRefresh', false, $moptions['lang']),
					"callback_data" => "update_$poll_id-$creator-update"
				]
			];
			$menu[] = [
				[
					"text" => getTranslate('commPageOptions', false, $moptions['lang']),
					"callback_data" => "/option $poll_id-$creator"
				],
				[
					"text" => getTranslate('commPageClose', false, $moptions['lang']),
					"callback_data" => "pclose_$poll_id-$creator"
				],
				[
					"text" => getTranslate('commPageDelete', false, $moptions['lang']),
					"callback_data" => "delete_ $poll_id-$creator"
				]
			];
		} else {
			if (isset($percentage[0])) {
				foreach($percentage[0] as $key => $value) {
					if (isset($num)) {
						if (count($menu[$num]) >= 5) $num = $num + 1;
					} else {
						$num = 0;
					}
					if (isset($numero)) {
						$numero = $numero + 1;
					} else {
						$numero = 0;
					}
					if ($value == "NAN") $value = 0;
					if (isset($poll['settings']['num_style'])) {
						$key = $config['numbers'][$poll['settings']['num_style']][$key];
					}
					if ($poll['settings']['percentage'] == "1") {
						$button = $key;
					} elseif ($poll['settings']['percentage'] == "2") {
						$button = $value . '% - ' . $key;
					} else {
						$button = $key . ' - ' . $value . "%";
					}
					$menu[$num][] = [
						'text' => $button, 
						'callback_data' => "v:$poll_id-$creator-$numero"
					]; 
				}
			}
			if (!isset($poll['settings']['sharable'])) {
				$poll['settings']['sharable'] = false;
			}
			if ($poll['settings']['sharable']) {
				$menu[] = [
					[
						"text" => getTranslate('share', false, $lang), 
						"callback_data" => "cburl-" . bot_encode("share_" . $poll['poll_id'] . "-" . $poll['creator'])
					],
				];
			}
		}
		if ($poll['settings']['max_voters']) {
			$limited = "Limited";
		} else {
			$limited = false;
		}
		if ($poll['votes'] === 0) {
			$r .= "\n" . getTranslate('renderer' . $limited . 'ZeroVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
		} elseif ($poll['votes'] === 1) {
			$r .= "\n" . getTranslate('renderer' . $limited . 'SingleVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
		} else {
			$r .= "\n" . getTranslate('renderer' . $limited . 'MultiVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
		}
		if ($poll['anonymous']) {
			$r .= "\n📖 " . italic(getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['type']), false, $lang));
		} else {
			$r .= "\n📖 " . italic(getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['type']), false, $lang));
		}
		$r .= "<a href='https://web.masterpoll.xyz/assets/tgBotWebPreviews.example.php'>&#8203;</><a href='http://t.me/noautopinbot?start=pinned'>&#8203;</>";
		return ['ok' => true, 'text' => $r, 'menu' => $menu, 'disable_web_preview' => $allegat];
	} else {
		$r .= $emoji . bold($poll['title']) . "\n";
		if ($poll['description']) $r .= htmlspecialchars($poll['description']) . "\n";
		$percentage = getPollPercentage($poll);
		if ($percentage['ok']) {
			$percentage = $percentage['result'];
		} else {
			return $percentage;
		}
		if (count($poll['choice']) === 1) {
			unset($poll['settings']['bars']);
		} else {
			if ($poll['settings']['sort'] and $admin !== 'moderate') {
				$sort = sorta($poll['choice']);
				if ($sort['ok']) {
					$poll['choice'] = $sort['result'];
				} else {
					return $sort;
				}
			}
		}
		if (isset($poll['votes'])) {
			if ($poll['votes'] > 100 and !$poll['settings']['hide_voters']) {
				$poll['settings']['hide_voters'] = true;
				$ishided = true;
			}
		}
		foreach($poll['choice'] as $key => $value) {
			if ($value == "NAN") $value = 0;
			if (isset($numero)) {$numero = $numero + 1; } else {$numero = 0; }
			if (!isset($poll['settings']['in_options']) or $poll['settings']['in_options'] == 2) {
				$int = " [" . $value . "]";
			} elseif($poll['settings']['in_options'] == 1) {
				$int = " [" . $percentage[0][$key] . "%]";
			}
			if (!$poll['anonymous'] and $admin !== 'moderate' and !$poll['settings']['hide_voters'] and $poll['usersvotes'][$key]) {
				if ($poll['settings']['group'] == 1) {
					$fig = [
						"start" => "┌",
						"line" => "┆",
						"list" => "├",
						"last" => "└"
					];
				} elseif ($poll['settings']['group'] == 2) {
					$fig = [
						"start" => "╔",
						"line" => "|",
						"list" => "╠",
						"last" => "╚"
					];
				} elseif ($poll['settings']['group'] == "no") {
					$fig = [
						"start" => "",
						"line" => "",
						"list" => "-",
						"last" => "-"
					];
				} else {
					$fig = [
						"start" => "┌",
						"line" => "┆",
						"list" => "├",
						"last" => "└"
					];
				}
				$r .= "\n" . $fig['start'] . " " . bold($key) . " $int\n";
			} else {
				$r .= "\n" . bold($key) . " $int\n";
			}
			if ($poll['settings']['bars'] == "dot" and $admin !== 'moderate' and $poll['usersvotes'][$key]) {
				if (!$poll['anonymous'] and $admin !== 'moderate') $r .= $fig['line'];
				if ($poll['settings']['in_options'] == 2) {
					$perc = " (" . nn($percentage[0][$key]) . "%)";
				} elseif ($poll['settings']['in_options'] == 1) {
					$perc = " (" . $value . ")";
				}
				$r .= bars($percentage[0][$key], "dot") . $perc . "\n";
			} elseif ($poll['settings']['bars'] == "like" and $value !== 0) {
				if (!$poll['anonymous'] and $admin !== 'moderate') $r .= $fig['line'];
				if ($poll['settings']['in_options'] == 2) {
					$perc = " (" . nn($percentage[0][$key]) . "%)";
				} elseif ($poll['settings']['in_options'] == 1) {
					$perc = " (" . $value . ")";
				}
				$r .= bars($percentage[0][$key], "like") . $perc . "\n";
				if ($admin === 'moderate') $r .= "/delete_" . str_replace('=', '', bot_encode("$poll_id-$creator-$numero")) . "\n";
			} else {
				if ($admin === 'moderate') $r .= "/delete_" . str_replace('=', '', bot_encode("$poll_id-$creator-$numero")) . "\n";
			}
			if (!$poll['anonymous'] and $poll['usersvotes'][$key]) {
				if ($admin === 'moderate') {
					// No user show for moderation of options
				} elseif ($poll['settings']['hide_voters']) {
					// Hide voters
				} else {
					$ids = array_values($poll['usersvotes'][$key]);
					if (count($ids) === 1) {
						if (!$name[$ids[0]]) $name[$ids[0]] = getName($ids[0])['result'];
						$r .= $fig['last'] . " " . $name[$ids[0]] . "\n";
					} else {
						unset($die);
						unset($nums);
						$nums = range(0, count($ids) - 1);
						foreach ($nums as $num) {
							if (!isset($name[$ids[$num]])) $name[$ids[$num]] = getName($ids[$num])['result'];
							if ($die) {
								$r .= $fig['last'] . " " . $name[$ids[$num]] . "\n";
							} else {
								$r .= $fig['list'] . " " . $name[$ids[$num]] . "\n";
								if ($ids[$num] == $ids[count($ids) - 2])$die = true;
							}
						}
					}
				}
			}
		}
		unset($numero);
		if (isset($ishided) and $poll['votes'] > 100) $poll['settings']['hide_voters'] = false;
		if ($admin === 'moderate') {
			$menu[] = [
				[
					"text" => getTranslate('buttonAppend', false, $moptions['lang']),
					"callback_data" => "cburl-" . bot_encode("append_$poll_id-$creator")
				]
			];
			$menu[] = [
				[
					"text" => "💾 " . getTranslate('done', false, $moptions['lang']),
					"callback_data" => "coptions_$poll_id-$creator-votes"
				]
			];
		} elseif ($admin === true) {
			$menu[] = [
				[
					"text" => getTranslate('publish', false, $moptions['lang']),
					"switch_inline_query" => $poll['poll_id']
				]
			];
			$menu[] = [
				[
					"text" => getTranslate('publishWithLink', false, $moptions['lang']),
					"switch_inline_query" => '$c:' . $poll['poll_id']
				],
				[
					"text" => getTranslate('sendChat', false, $moptions['lang']),
					"callback_data" => "psend_$poll_id-$creator"
				]
			];
			$menu[] = [
				[
					"text" => getTranslate('buttonVote', false, $moptions['lang']),
					"callback_data" => "vote_$poll_id-$creator"
				],
				[
					"text" => getTranslate('commPageRefresh', false, $moptions['lang']),
					"callback_data" => "update_$poll_id-$creator-update"
				]
			];
			$menu[] = [
				[
					"text" => getTranslate('commPageOptions', false, $moptions['lang']),
					"callback_data" => "/option $poll_id-$creator"
				],
				[
					"text" => getTranslate('commPageClose', false, $moptions['lang']),
					"callback_data" => "pclose_$poll_id-$creator"
				],
				[
					"text" => getTranslate('commPageDelete', false, $moptions['lang']),
					"callback_data" => "delete_ $poll_id-$creator"
				]
			];
		} elseif ($admin and isAdmin($admin, $poll)['result']) {
			if ($admin === true) {
				$publish = $poll['poll_id'];
			} else {
				$publish = "share " . bot_encode("$poll_id-$creator");
			}
			$menu[] = [
				[
					"text" => getTranslate('publish', false, $moptions['lang']),
					"switch_inline_query" => $publish
				]
			];
			$menu[] = [
				[
					"text" => getTranslate('buttonVote', false, $moptions['lang']),
					"callback_data" => "vote_$poll_id-$creator"
				],
				[
					"text" => getTranslate('commPageRefresh', false, $moptions['lang']),
					"callback_data" => "update_$poll_id-$creator-update"
				]
			];
			$menu[] = [
				[
					"text" => getTranslate('commPageOptions', false, $moptions['lang']),
					"callback_data" => "/option $poll_id-$creator"
				],
				[
					"text" => getTranslate('commPageClose', false, $moptions['lang']),
					"callback_data" => "pclose_$poll_id-$creator"
				],
				[
					"text" => getTranslate('commPageDelete', false, $moptions['lang']),
					"callback_data" => "delete_ $poll_id-$creator"
				]
			];
		} else {
			unset($num);
			if (isset($poll['settings']['menu_strings'])) {
				$totstr = $poll['settings']['menu_strings'];
			} else {
				$totstr = 1;
			}
			foreach($percentage[0] as $key => $value) {
				if (isset($numero)) {$numero = $numero + 1; } else {$numero = 0; }
				if (isset($num)) {
					if (count($menu[$num]) >= $totstr) $num = $num + 1;
				} else {
					$num = 0;
				}
				if ($value == "NAN") $value = 0;
				if ($poll['settings']['percentage'] == "1") {
					$button = $key;
				} elseif ($poll['settings']['percentage'] == "2") {
					$button = $value . '% - ' . $key;
				} else {
					$button = $key . ' - ' . $value . "%";
				}
				$menu[$num][] = [
					'text' => $button, 
					'callback_data' => "v:$poll_id-$creator-$numero"
				];
			}
			if (!isset($poll['settings']['hide_voters'])) {
				$poll['settings']['hide_voters'] = false;
			}
			if (!$poll['settings']['hide_voters'] and !$poll['anonymous']) {
				$menu[] = [
					[
						"text" => getTranslate('boardShowMore', false, $lang),
						"callback_data" => "cburl-" . bot_encode("list_" . $poll['poll_id'] . "-" . $poll['creator'])
					]
				];
			}
			if (!isset($poll['settings']['appendable'])) {
				$poll['settings']['appendable'] = false;
			}
			if ($poll['settings']['appendable']) {
				$menu[] = [
					[
						"text" => getTranslate('buttonAppend', false, $lang),
						"callback_data" => "cburl-" . bot_encode("append_$poll_id-$creator")
					]
				];
			}
			if (!isset($poll['settings']['sharable'])) {
				$poll['settings']['sharable'] = false;
			}
			if ($poll['settings']['sharable']) {
				$menu[] = [
					[
						"text" => getTranslate('share', false, $lang),
						"callback_data" => "cburl-" . bot_encode("share_$poll_id-$creator")
					]
				];
			}
		}
		if (!isset($poll['votes'])) {
			$poll['votes'] = 0;
		}
		if ($poll['settings']['max_voters']) {
			$limited = "Limited";
		} else {
			$limited = false;
		}
		if ($poll['votes'] === 0) {
			$r .= "\n" . getTranslate('renderer' . $limited . 'ZeroVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
		} elseif ($poll['votes'] === 1) {
			$r .= "\n" . getTranslate('renderer' . $limited . 'SingleVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
		} else {
			$r .= "\n" . getTranslate('renderer' . $limited . 'MultiVotedSoFar', [$poll['votes'], $poll['settings']['max_voters']], $lang);
		}
		if ($poll['type'] == "limited doodle" or $poll['settings']['type'] == "limited doodle") {
			$total = count($poll['choice']);
			$max = $poll['settings']['max_choices'];
			$r .= "\nℹ️ " . getTranslate('limitedDoodleYouCanChooseSoMany', [$max, $total], $lang);
		}
		if ($poll['anonymous']) {
			$r .= "\n📖 " . italic(getTranslate('inlineDescriptionAnonymous' . maiuscolo($poll['type']), false, $lang));
		} else {
			$r .= "\n📖 " . italic(getTranslate('inlineDescriptionPersonal' . maiuscolo($poll['type']), false, $lang));
		}
		$r .= "<a href='https://web.masterpoll.xyz/assets/tgBotWebPreviews.example.php'>&#8203;</><a href='http://t.me/noautopinbot?start=pinned'>&#8203;</>";
		return ['ok' => true, 'text' => $r, 'menu' => $menu, 'disable_web_preview' => $allegat];
	}
}

function pollToArray ($poll = false) {
	if (!isset($poll)) {
		cell_error("<b>Warning:</b> la variabile \$poll non è stata settata nella funzione pollToArray");
		return false;
	}
	if ($poll['anonymous']) {
		$privacy = "anonymous";
	} else {
		$privacy = "personal";
	}
	$choices = [];
	if ($poll['usersvotes']) {
		if ($poll['type'] == "board") {
			$ischoices = "comments";
			$choices = [];
			foreach ($poll['choice'] as $id => $comment) {
				if ($poll['anonymous']) {
					$choices[] = $comment;
				} else {
					$choices[round($id)] = $comment;
				}
			}
		} elseif ($poll['type'] == "participation") {
			$ischoices = "participants";
			foreach ($poll['usersvotes']['participants'] as $id) {
				$choices[] = round($id);
			}
		} else {
			$ischoices = "choices";
			foreach ($poll['usersvotes'] as $choice => $users) {
				if ($poll['anonymous']) {
					$choices[$choice] = 0;
				} else {
					$choices[$choice] = [];
				}
				foreach ($users as $id) {
					if ($poll['anonymous']) {
						if (!$choices[$choice]) $choices[$choice] = 0;
						$choices[$choice] += 1;
					} else {
						$choices[$choice][] = round($id);
					}
				}
				if (!isset($choices[$choice])) $choices[$choice] = [];
			}
		}
	} else {
		if ($poll['type'] == "board") {
			$ischoices = "comments";
		} elseif ($poll['type'] == "participation") {
			$ischoices = "participants";
		} else {
			$ischoices = "choices";
		}
	}
	$array = [
		'poll_id' => round($poll['poll_id']),
		'owner_id' => round($poll['creator']),
		'title' => $poll['title'],
	];
	if (isset($poll['description'])) $array['description'] = $poll['description'];
	$array['privacy'] = $privacy;
	$array['status'] = $poll['status'];
	$array['type'] = $poll['type'];if ($poll['usersvotes']) {
		if ($poll['type'] == "board") {
			$ischoices = "comments";
			$choices = [];
			foreach ($poll['choice'] as $id => $comment) {
				if ($poll['anonymous']) {
					$choices[] = $comment;
				} else {
					$choices[round($id)] = $comment;
				}
			}
		} elseif ($poll['type'] == "participation") {
			$ischoices = "participants";
			foreach ($poll['usersvotes']['participants'] as $id) {
				$choices[] = round($id);
			}
		} elseif ($poll['type'] == "rating") {
			$ischoices = "rating";
			foreach ($poll['usersvotes'] as $choice => $users) {
				if ($poll['anonymous']) {
					$choices[$choice] = 0;
				} else {
					$choices[$choice] = [];
				}
				foreach ($users as $id) {
					if ($poll['anonymous']) {
						if (!$choices[$choice]) $choices[$choice] = 0;
						$choices[$choice] += 1;
					} else {
						$choices[$choice][] = round($id);
					}
				}
				if (!isset($choices[$choice])) $choices[$choice] = [];
			}
		} else {
			$ischoices = "choices";
			foreach ($poll['usersvotes'] as $choice => $users) {
				if ($poll['anonymous']) {
					$choices[$choice] = 0;
				} else {
					$choices[$choice] = [];
				}
				foreach ($users as $id) {
					if ($poll['anonymous']) {
						if (!$choices[$choice]) $choices[$choice] = 0;
						$choices[$choice] += 1;
					} else {
						$choices[$choice][] = round($id);
					}
				}
				if (!isset($choices[$choice])) $choices[$choice] = [];
			}
		}
	} else {
		if ($poll['type'] == "rating") {
			$ischoices = "rating";
		} elseif ($poll['type'] == "board") {
			$ischoices = "comments";
		} elseif ($poll['type'] == "participation") {
			$ischoices = "participants";
		} else {
			$ischoices = "choices";
		}
	}
	if ($poll['type'] == "limited doodle") $array['max_choices'] = $poll['settings']['max_choices'];
	if ($poll['type'] == "quiz") $array['right_answer'] = $poll['settings']['right_answers'];
	$array[$ischoices] = $choices;
	return $array;
}

function bars ($p = 0, $type = 0) {
	if ($p == "NAN") $p = 0;
	if ($type == "like") {
		if ($p <= 10) {
			return "👍";
		} elseif ($p >=11 && $p <= 20) {
			return "👍👍";
		} elseif ($p >= 21 && $p <= 30) {
			return "👍👍👍";
		} elseif ($p >= 31 && $p <= 40) {
			return "👍👍👍👍";
		} elseif ($p >= 41 && $p <= 50) {
			return "👍👍👍👍👍";
		} elseif ($p >= 51 && $p <= 60) {
			return "👍👍👍👍👍👍";
		} elseif ($p >= 61 && $p <= 70) {
			return "👍👍👍👍👍👍👍";
		} elseif ($p >= 71 && $p <= 80) {
			return "👍👍👍👍👍👍👍👍";
		} elseif ($p >= 81 && $p <= 90) {
			return "👍👍👍👍👍👍👍👍👍";
		} elseif ($p >= 91 && $p <= 100) {
			return "👍👍👍👍👍👍👍👍👍👍";
		} else {
			return "👍👍👍👍👍👍👍👍👍👍";
		}
	}
	if ($type == "quad") {
		if ($p == 0) {
			return "⚪⚪⚪⚪⚪⚪⚪⚪⚪⚪";
		} elseif ($p <= 10) {
			return "⚫⚪⚪⚪⚪⚪⚪⚪⚪⚪";
		} elseif ($p >=11 && $p <= 20) {
			return "⚫⚫⚪⚪⚪⚪⚪⚪⚪⚪";
		} elseif ($p >= 21 && $p <= 30) {
			return "⚫⚫⚫⚪⚪⚪⚪⚪⚪⚪";
		} elseif ($p >= 31 && $p <= 40) {
			return "⚫⚫⚫⚫⚪⚪⚪⚪⚪⚪";
		} elseif ($p >= 41 && $p <= 50) {
			return "⚫⚫⚫⚫⚫⚪⚪⚪⚪⚪";
		} elseif ($p >= 51 && $p <= 60) {
			return "⚫⚫⚫⚫⚫⚫⚪⚪⚪⚪";
		} elseif ($p >= 61 && $p <= 70) {
			return "⚫⚫⚫⚫⚫⚫⚫⚪⚪⚪";
		} elseif ($p >= 71 && $p <= 80) {
			return "⚫⚫⚫⚫⚫⚫⚫⚫⚪⚪";
		} elseif ($p >= 81 && $p <= 90) {
			return "⚫⚫⚫⚫⚫⚫⚫⚫⚫⚪";
		} elseif ($p >= 91 && $p <= 100) {
			return "⚫⚫⚫⚫⚫⚫⚫⚫⚫⚫";
		} else {
			return "⚫⚫⚫⚫⚫⚫⚫⚫⚫⚫";
		}
	}
	if ($type == "dot") {
		if ($p == 0) {
			return "○○○○○○○○○○";
		} elseif ($p <= 10) {
			return "●○○○○○○○○○";
		} elseif ($p >=11 && $p <= 20) {
			return "●●○○○○○○○○";
		} elseif ($p >= 21 && $p <= 30) {
			return "●●●○○○○○○○";
		} elseif ($p >= 31 && $p <= 40) {
			return "●●●●○○○○○○";
		} elseif ($p >= 41 && $p <= 50) {
			return "●●●●●○○○○○";
		} elseif ($p >= 51 && $p <= 60) {
			return "●●●●●●○○○○";
		} elseif ($p >= 61 && $p <= 70) {
			return "●●●●●●●○○○";
		} elseif ($p >= 71 && $p <= 80) {
			return "●●●●●●●●○○";
		} elseif ($p >= 81 && $p <= 90) {
			return "●●●●●●●●●○";
		} elseif ($p >= 91 && $p <= 100) {
			return "●●●●●●●●●●";
		} else {
			return "●●●●●●●●●●";
		}
	}
	cell_error("<b>Warning:</b> la variabile \$type non è valida nella funzione bars");
	return "❌";
}

function getID ($creator = false) {
	if (!$creator) {
		call_error("<b>Errore:</b> la variabile \$creator non è stata settata nella funzione getID");
		return 1;
	} elseif(!is_numeric($creator)) {
		call_error("<b>Error:</b> la variabile \$creator non è numerica nella funzione getID"); 
		return 1;
	}
	$getTotalPoll = getTotalPoll($creator);
	if ($getTotalPoll['ok']) {
		$getTotalPoll = $getTotalPoll['result'];
	} else {
		return 1;
	}
	$id = count($getTotalPoll);
	if ($id == 0) {
		return 1;
	} elseif ($id == 1) {
		return 2;
	} else {
		return $id + 1;
	}
}

function newPoll ($poll_id = false, $creator = false, $anonymous = false, $type = false, $newsettings = []) {
	if(!$poll_id) {
		call_error("<b>Error:</b> la variabile \$poll_id non è stata settata nella funzione newPoll"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$poll_id was not found"];
	} elseif(!$creator) {
		call_error("<b>Error:</b> la variabile \$creator non è stata settata nella funzione newPoll"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator was not found"];
	} elseif(!is_numeric($poll_id)) {
		call_error("<b>Error:</b> la variabile \$poll_id non è numerica nella funzione newPoll"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$poll_id must be a numeric value"];
	} elseif(!is_numeric($creator)) {
		call_error("<b>Error:</b> la variabile \$creator non è numerica nella funzione newPoll"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$creator must be a numeric value"];
	} elseif(!$type) {
		call_error("<b>Error:</b> la variabile \$type non è stata settata nella funzione newPoll");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$type was not found"];
	} else {
		$settings = [
			'type' => $type
		];
		if ($type == "limited doodle") {
			if (empty($newsettings['max_choices'])) {
				return ['ok' => false, 'error_code' => 400, "Bad Request: max_choices is empty"];
			}
		} elseif ($type == "quiz") {
			if (empty($newsettings['right_answers'])) {
				return ['ok' => false, 'error_code' => 400, "Bad Request: right_answers is empty"];
			}
		}
		if (!empty($newsettings)) $settings = array_merge($settings, $newsettings);
		return db_query("INSERT INTO polls (poll_id, user_id, status, anonymous, settings) VALUES (?,?,?,?,?)", [$poll_id, $creator, 'open', $anonymous, json_encode($settings)], "no");
	}
}

function addTitlePoll ($poll_id = false, $creator = false, $title = false) {
	if(!$poll_id) {
		call_error("<b>Error:</b> la variabile \$poll_id non è stata settata nella funzione addTitlePoll"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$poll_id was not found"];
	} elseif(!$creator) {
		call_error("<b>Error:</b> la variabile \$creator non è stata settata nella funzione addTitlePoll"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator was not found"];
	} elseif(!is_numeric($poll_id)) {
		call_error("<b>Error:</b> la variabile \$poll_id non è numerica nella funzione addTitlePoll"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$poll_id must be a numeric value"];
	} elseif(!is_numeric($creator)) {
		call_error("<b>Error:</b> la variabile \$creator non è numerica nella funzione addTitlePoll"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$creator must be a numeric value"];
	} elseif(!$title) {
		call_error("<b>Error:</b> la variabile \$title non è stata settata nella funzione addTitlePoll");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$title was not found"];
	} else {
		return db_query("UPDATE polls SET title = ? WHERE user_id = ? and poll_id = ?", [$title, $creator, $poll_id], "no");
	}
}

function addDescriptionPoll ($poll_id = false, $creator = false, $description = flase) {
	if(!$poll_id) {
		call_error("<b>Error:</b> la variabile \$poll_id non è stata settata nella funzione addDescriptionPoll"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$poll_id was not found"];
	} elseif(!$creator) {
		call_error("<b>Error:</b> la variabile \$creator non è stata settata nella funzione addDescriptionPoll"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator was not found"];
	} elseif(!is_numeric($poll_id)) {
		call_error("<b>Error:</b> la variabile \$poll_id non è numerica nella funzione addDescriptionPoll"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$poll_id must be a numeric value"];
	} elseif(!is_numeric($creator)) {
		call_error("<b>Error:</b> la variabile \$creator non è numerica nella funzione addDescriptionPoll"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$creator must be a numeric value"];
	} elseif(!$description) {
		call_error("<b>Error:</b> la variabile \$description non è stata settata nella funzione addDescriptionPoll");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$description was not found"];
	} else {
		return db_query("UPDATE polls SET description = ? WHERE user_id = ? and poll_id = ?", [$description, $creator, $poll_id], "no");
	}
}

function cancelPoll($poll_id = false, $creator = false) {
	if(!$poll_id) {
		call_error("<b>Error:</b> la variabile \$poll_id non è stata settata nella funzione cancelPoll"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$poll_id was not found"];
	} elseif(!$creator) {
		call_error("<b>Error:</b> la variabile \$creator non è stata settata nella funzione cancelPoll"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator was not found"];
	} elseif(!is_numeric($poll_id)) {
		call_error("<b>Error:</b> la variabile \$poll_id non è numerica nella funzione cancelPoll"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$poll_id must be a numeric value"];
	} elseif(!is_numeric($creator)) {
		call_error("<b>Error:</b> la variabile \$creator non è numerica nella funzione cancelPoll"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$creator must be a numeric value"];
	} else {
		return db_query("UPDATE polls SET title = ?, description = ?, choices = ?, status = ?, settings = ?, anonymous = ? WHERE user_id = ? and poll_id = ?", ["false", "", "[]", 'deleted', '[]', "", $creator, $poll_id], "no");
	}
}

function cancelAllUserPoll($creator = false) {
	if(!$creator) {
		call_error("<b>Error:</b> la variabile \$creator non è stata settata nella funzione cancelAllUserPoll");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator was not found"];
	} elseif(!is_numeric($creator)) {
		call_error("<b>Error:</b> la variabile \$creator non è stata settata nella funzione cancelAllUserPoll");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$creator must be a numeric value"];
	} else {
		return db_query("UPDATE polls SET choices = '[]', status = 'deleted', settings = '[]' WHERE user_id = ?", [$creator], "no");
	}
}

function addChoicePoll($p = false, $newChoice = false) {
	if ($p === false) {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione addChoicePoll");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
		$p = sendPoll($p['poll_id'], $p['creator']);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			return $p;
		}
	} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
	} else {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione addChoicePoll");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$p is invalid"];
	}
	if (!isset($p['status']) ) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	}
	if(!$newChoice) {
		$newChoice = "0";
	}
	if (!$p['choices']) $p['choices'] = [];
	$p['choices'][$newChoice] = [];
	return db_query("UPDATE polls SET choices = ? WHERE user_id = ? and poll_id = ?", [json_encode($p['choices']), $p['creator'], $p['poll_id']], "no");
} 

function addChoiceBoard($p = false, $user_id = false, $text = false) {
	if ($p === false) {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione addChoiceBoard");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
		$p = sendPoll($p['poll_id'], $p['creator']);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			return $p;
		}
	} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
	} else {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione addChoiceBoard");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$p is invalid"];
	}
	if (!isset($p['status']) ) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	}
	if(!$text) {
		$text = "0";
	}
	if(!$user_id) {
		call_error("<b>Error:</b> la variabile \$user_id non è stata settata nella funzione addChoiceBoard"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$user_id was not found"];
	} elseif(!is_numeric($user_id)) {
		call_error("<b>Error:</b> la variabile \$user_id non è numerica nella funzione addChoiceBoard"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$user_id must be a numeric value"];
	} else {
		$p['choices'][$user_id] = $text;
		return db_query("UPDATE polls SET choices = ? WHERE user_id = ? and poll_id = ?", [json_encode($p['choices']), $p['creator'], $p['poll_id']], "no");
	}
} 

function removeChoicePoll($p = false, $choice = false) {
	if ($p === false) {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione removeChoicePoll");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
		$p = sendPoll($p['poll_id'], $p['creator']);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			return $p;
		}
	} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
	} else {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione removeChoicePoll");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$p is invalid"];
	}
	if (!isset($p['status']) ) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	}
	if (!$choice) {
		$choice = "0";
	}
	unset($p['choices'][$choice]);
	return db_query("UPDATE polls SET choices = ? WHERE user_id = ? and poll_id = ?", [json_encode($p['choices']), $p['creator'], $p['poll_id']], "no");
}

function addChoice($p = false, $user_id = false, $choice = false) {
	if ($p === false) {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione addChoice");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
		$p = sendPoll($p['poll_id'], $p['creator']);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			return $p;
		}
	} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
	} else {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione addChoice");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$p is invalid"];
	}
	if (!isset($p['status']) ) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	}
	if(!$choice) {
		$choice = "0";
	}
	if(!$user_id) {
		call_error("<b>Error:</b> la variabile \$user_id non è stata settata nella funzione addChoice"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$user_id was not found"];
	} elseif(!is_numeric($user_id)) {
		call_error("<b>Error:</b> la variabile \$user_id non è numerica nella funzione addChoice"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$user_id must be a numeric value"];
	} else {
		if(!in_array($user_id, $p['choices'][$choice])) {
			$p['choices'][$choice][] = $user_id;
			return db_query("UPDATE polls SET choices = ? WHERE user_id = ? and poll_id = ?", [json_encode($p['choices']), $p['creator'], $p['poll_id']], "no");
		} else {
			return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: this user have already voted"];
		}
	}
} 

function removeChoice($p = false, $user_id = false, $choice = false) {
	if ($p === false) {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione removeChoice");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
		$p = sendPoll($p['poll_id'], $p['creator']);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			return $p;
		}
	} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
	} else {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione removeChoice");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$p is invalid"];
	}
	if (!isset($p['status']) ) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	}
	if(!$choice) {
		$choice = "0";
	}
	if(!$user_id) {
		call_error("<b>Error:</b> la variabile \$user_id non è stata settata nella funzione removeChoice"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$user_id was not found"];
	} elseif(!is_numeric($user_id)) {
		call_error("<b>Error:</b> la variabile \$user_id non è numerica nella funzione removeChoice"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$user_id must be a numeric value"];
	} else {
		$p['choices'][$choice] = array_diff($p['choices'][$choice], [$user_id]);
		return db_query("UPDATE polls SET choices = ? WHERE user_id = ? and poll_id = ?", [json_encode($p['choices']), $p['creator'], $p['poll_id']], "no");
	}
} 

function removeAllChoices($p = false, $user_id = false) {
	if ($p === false) {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione removeAllChoices");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
		$p = sendPoll($p['poll_id'], $p['creator']);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			return $p;
		}
	} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
	} else {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione removeAllChoices");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$p is invalid"];
	}
	if (!isset($p['status']) ) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	}
	if(!$user_id) {
		call_error("<b>Error:</b> la variabile \$user_id non è stata settata nella funzione removeAllChoices"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$user_id was not found"];
	} elseif(!is_numeric($user_id)) {
		call_error("<b>Error:</b> la variabile \$user_id non è numerica nella funzione removeAllChoices"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$user_id must be a numeric value"];
	} else {
		foreach ($p['choices'] as $choice => $users) {
			if (in_array($user_id, $users)) $p['choices'][$choice] = array_diff($p['choices'][$choice], [$user_id]);
		}
		return db_query("UPDATE polls SET choices = ? WHERE user_id = ? and poll_id = ?", [json_encode($p['choices']), $p['creator'], $p['poll_id']], "no");
	}
}
	
function haveChoices($p = false, $user_id = false) {
	if ($p === false) {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione haveChoices");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
		$p = sendPoll($p['poll_id'], $p['creator']);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			return $p;
		}
	} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
	} else {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione haveChoices");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$p is invalid"];
	}
	if (!isset($p['status']) ) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	}
	if(!$user_id) {
		call_error("<b>Error:</b> la variabile \$user_id non è stata settata nella funzione haveChoices"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$user_id was not found"];
	} elseif(!is_numeric($user_id)) {
		call_error("<b>Error:</b> la variabile \$user_id non è numerica nella funzione haveChoices"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$user_id must be a numeric value"];
	} else {
		$selectedChoice = [];
		foreach($p['choices'] as $choice => $users) {
			if (isset($choice[$user_id]) or in_array($user_id, $users)) {
				$selectedChoice[] = $choice;
			}
		}
		return ['ok' => true, 'result' => $selectedChoice];
	}
}  

function haveChoice($p = false, $user_id = false, $choice = false) {
	if ($p === false) {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione haveChoices");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
		$p = sendPoll($p['poll_id'], $p['creator']);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			return $p;
		}
	} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
	} else {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione haveChoices");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$p is invalid"];
	}
	if (!isset($p['status']) ) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	}
	if(!$choice and $choice !== 0) {
		call_error("<b>Error:</b> la variabile \$choice non è stata settata nella funzione haveChoice"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable choice was not found"];
	} elseif(!in_array($choice, array_keys($p['choices']))) {
		call_error("<b>Error:</b> la variabile \$choice non è presente in \$p['choices'] nella funzione haveChoice"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable choice was not found"];
	} elseif(!$user_id) {
		call_error("<b>Error:</b> la variabile \$user_id non è stata settata nella funzione haveChoice"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable user_id was not found"];
	} elseif(!is_numeric($user_id)) {
		call_error("<b>Error:</b> la variabile \$user_id non è numerica nella funzione haveChoice"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable user_id must be a numeric value"];
	} else {
		$selectedChoice = false;
		foreach($p['choices'] as $schoice => $users) {
			if (isset($schoice[$user_id]) or in_array($user_id, $users)) {
				if ($schoice == $choice) $selectedChoice = $schoice;
			}
		}
		return ['ok' => true, 'result' => $selectedChoice];
	}
}  

function userTotalChoices($p = false, $user_id = false) {
	if ($p === false) {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione userTotalChoices");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
		$p = sendPoll($p['poll_id'], $p['creator']);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			return $p;
		}
	} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
	} else {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione userTotalChoices");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$p is invalid"];
	}
	if (!isset($p['status']) ) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	}
	if(!$user_id) {
		call_error("<b>Error:</b> la variabile \$user_id non è stata settata nella funzione userTotalChoices"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$user_id was not found"];
	} elseif(!is_numeric($user_id)) {
		call_error("<b>Error:</b> la variabile \$user_id non è numerica nella funzione userTotalChoices"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$user_id must be a numeric value"];
	} else {
		$total = [];
		foreach($p['choices'] as $choice => $users) {
			if (isset($choice[$user_id]) or in_array($user_id, $users)) {
				$total[] = $choice;
			}
		}
		return ['ok' => true, 'result' => number_format(count($total))];
	}
}

function sendPoll($poll_id = false, $creator = false) {
	if(!$poll_id) {
		call_error("<b>Error:</b> la variabile \$poll_id non è stata settata nella funzione sendPoll"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$poll_id was not found"];
	} elseif(!$creator) {
		call_error("<b>Error:</b> la variabile \$creator non è stata settata nella funzione sendPoll"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator was not found"];
	} elseif(!is_numeric($poll_id)) {
		call_error("<b>Error:</b> la variabile \$poll_id non è numerica nella funzione sendPoll"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$poll_id must be a numeric value"];
	} elseif(!is_numeric($creator)) {
		call_error("<b>Error:</b> la variabile \$creator non è numerica nella funzione sendPoll"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$creator must be a numeric value"];
	} else {
		$q = db_query("SELECT * FROM polls WHERE user_id = ? and poll_id = ? LIMIT 1", [round($creator), round($poll_id)], true);
		if ($q['ok']) {
			$q = $q['result'];
		} else {
			return $q;
		}
		$q['choices'] = json_decode($q['choices'], true);
		if (is_array($q['choices'])) {
			$choicesArray = $q['choices'];
		} else {
			return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: poll choices must be an array"];
		}
		$q['settings'] = json_decode($q['settings'], true);
		if (!is_array($q['settings'])) {
			return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: poll settings must be an array"];
		}
		if (isset($q['settings']['closetime'])) {
			if (time() >= $q['settings']['closetime']) {
				$q['status'] = "closed";
				unset($q['settings']['closetime']);
				db_query("UPDATE polls SET status = ?, settings = ? WHERE user_id = ? and poll_id = ?", ['closed', json_encode($q['settings']), $creator, $poll_id], "no");
			}
		}
		if (isset($q['settings']['opentime'])) {
			if (time() >= $q['settings']['opentime']) {
				$q['status'] = "open";
				unset($q['settings']['opentime']);
				db_query("UPDATE polls SET status = ?, settings = ? WHERE user_id = ? and poll_id = ?", ['open', json_encode($q['settings']), $creator, $poll_id], "no");
			}
		}
		if ($q['settings']['type'] !== "board") {
			$users = [];
			$chosenChoice = [];
			if ($q['choices']) {
				foreach ($q['choices'] as $optionableChoice => $keys) {
					$or = count($keys);
					$chosenChoice[$optionableChoice] = $or;
					$votes = $or + $votes;
					foreach ($keys as $id) {
						$users[$id] = true;
					}
				}
			}
		} else {
			$chosenChoice = $q['choices'];
			if ($q['choices']) {
				foreach ($q['choices'] as $user => $text) {
					$users[$user] = true;
				}
				$q['choices'] = count($users);
			} else {
				$users = [];
				$q['choices'] = 0;
			}
		}
		if (!count($users)) {
			$usercount = "0";
		} else {
			$usercount = count($users);
		}
		if ($q['settings']['admins']) {
			$admins = $q['settings']['admins'];
		} else {
			$admins = [];
		}
		$result = [
			"poll_id" => $poll_id, 
			"creator" => $creator,
			"title" => $q['title'],
			"description" => $q['description'], 
			"type" => $q['settings']['type'],
			"admins" => $admins,
			"usersvotes" => $q['choices'],
			"choice" => $chosenChoice,
			"choices" => $choicesArray,
			"votes" => $usercount,
			"status" => $q['status'],
			"settings" => $q['settings'],
			"anonymous" => $q['anonymous'],
			"messages" => json_decode($q['messages'], true)
		];
		return ['ok' => true, 'result' => $result];
	}
}

function doPoll($p = false) {
	if (!$p) {
		call_error("<b>Error:</b> la variabile \$poll non è stata settata nella funzione doPoll");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	}
	$poll_id = $p['poll_id'];
	$creator = $p['user_id'];
	$p['choices'] = json_decode($p['choices'], true);
	if (is_array($p['choices'])) {
		$choicesArray = $p['choices'];
	} else {
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: poll choices must be an array"];
	}
	$p['settings'] = json_decode($p['settings'], true);
	if (!is_array($p['settings'])) {
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: poll settings must be an array"];
	}
	if (isset($p['settings']['closetime'])) {
		if (time() >= $p['settings']['closetime']) {
			$p['status'] = "closed";
			unset($p['settings']['closetime']);
			db_query("UPDATE polls SET status = ?, settings = ? WHERE user_id = ? and poll_id = ?", ['closed', json_encode($p['settings']), $creator, $poll_id], "no");
		}
	}
	if (isset($p['settings']['opentime'])) {
		if (time() >= $p['settings']['opentime']) {
			$p['status'] = "open";
			unset($p['settings']['opentime']);
			db_query("UPDATE polls SET status = ?, settings = ? WHERE user_id = ? and poll_id = ?", ['open', json_encode($p['settings']), $creator, $poll_id], "no");
		}
	}
	if ($p['settings']['type'] !== "board") {
		$users = [];
		$chosenChoice = [];
		if ($p['choices']) {
			foreach ($p['choices'] as $optionableChoice => $keys) {
				$or = count($keys);
				$chosenChoice[$optionableChoice] = $or;
				$votes = $or + $votes;
				foreach ($keys as $id) {
					$users[$id] = true;
				}
			}
		}
	} else {
		$chosenChoice = $p['choices'];
		if ($p['choices']) {
			foreach ($p['choices'] as $user => $text) {
				$users[$user] = true;
			}
			$p['choices'] = count($p['choices']);
		} else {
			$users = [];
			$p['choices'] = 0;
		}
	}
	if (!count($users)) {
		$usercount = "0";
	} else {
		$usercount = count($users);
	}
	if ($p['settings']['admins']) {
		$admins = $p['settings']['admins'];
	} else {
		$admins = [];
	}
	$result = [
		"poll_id" => $p['poll_id'], 
		"creator" => $p['user_id'],
		"title" => $p['title'],
		"description" => $p['description'], 
		"type" => $p['settings']['type'],
		"admins" => $admins,
		"usersvotes" => $p['choices'],
		"choice" => $chosenChoice,
		"choices" => $choicesArray,
		"votes" => $usercount,
		"status" => $p['status'],
		"settings" => $p['settings'],
		"anonymous" => $p['anonymous'],
		"messages" => json_decode($p['messages'], true)
	];
	return ['ok' => true, 'result' => $result];
}

function getPollAdmins($p = false) {
	if ($p === false) {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione getPollAdmins");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
		$p = sendPoll($p['poll_id'], $p['creator']);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			return $p;
		}
	} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
	} else {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione getPollAdmins");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$p is invalid"];
	}
	if (!isset($p['status']) ) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	}
	if ($p['settings']['admins']) {
		return ['ok' => true, 'result' => array_keys($p['settings']['admins'])];
	} else {
		return ['ok' => true, 'result' => []];
	}
}

function getPollAdmin($p = false, $user_id = false) {
	if ($p === false) {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione getPollAdmin");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
		$p = sendPoll($p['poll_id'], $p['creator']);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			return $p;
		}
	} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
	} else {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione getPollAdmin");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$p is invalid"];
	}
	if (!isset($p['status']) ) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	}
	if(!$user_id) {
		call_error("<b>Error:</b> la variabile \$user_id non è stata settata nella funzione getPollAdmin"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$user_id was not found"];
	} elseif(!is_numeric($user_id)) {
		call_error("<b>Error:</b> la variabile \$user_id non è numerica nella funzione getPollAdmin"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$user_id must be a numeric value"];
	} else {
		if (isset($p['settings']['admins'][$user_id])) {
			return ['ok' => true, 'result' => $p['settings']['admins'][$user_id]];
		} else {
			return ['ok' => true, 'result' => false];
		}
	}
}

function addAdmin($p = false, $user_id = false) {
	if ($p === false) {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione addAdmin");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
		$p = sendPoll($p['poll_id'], $p['creator']);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			return $p;
		}
	} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
	} else {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione addAdmin");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$p is invalid"];
	}
	if (!isset($p['status']) ) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	}
	if(!$user_id) {
		call_error("<b>Error:</b> la variabile \$user_id non è stata settata nella funzione addAdmin"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$user_id was not found"];
	} elseif(!is_numeric($user_id)) {
		call_error("<b>Error:</b> la variabile \$user_id non è numerica nella funzione addAdmin"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$user_id must be a numeric value"];
	} else {
		if (isset($p['settings']['admins'][$user_id])) {
			return ['ok' => true, 'result' => $p['settings']['admins'][$user_id]];
		} else {
			$p['settings']['admins'][$user_id] = [
				'status' => "waiting",
				'permissions' => []
			];
			$q = db_query("UPDATE polls SET settings = ? WHERE poll_id = ? and user_id = ?", [json_encode($p['settings']), $p['poll_id'], $p['creator']], "no");
			if (!$q['ok']) {
				return $q;
			}
			return ['ok' => true, 'result' => $p['settings']['admins'][$user_id]];
		}
	}
}

function editAdmin($p = false, $user_id = false, $perms = false) {
	if ($p === false) {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione editAdmin");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
		$p = sendPoll($p['poll_id'], $p['creator']);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			return $p;
		}
	} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
	} else {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione editAdmin");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$p is invalid"];
	}
	if (!isset($p['status']) ) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	}
	if(!$user_id) {
		call_error("<b>Error:</b> la variabile \$user_id non è stata settata nella funzione addAdmin"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$user_id was not found"];
	} elseif(!is_numeric($user_id)) {
		call_error("<b>Error:</b> la variabile \$user_id non è numerica nella funzione addAdmin"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$user_id must be a numeric value"];
	} else {
		if (isset($p['settings']['admins'][$user_id])) {
			$p['settings']['admins'][$user_id] = $perms;
			$q = db_query("UPDATE polls SET settings = ? WHERE poll_id = ? and user_id = ?", [json_encode($p['settings']), $p['poll_id'], $p['creator']], "no");
			if (!$q['ok']) {
				return $q;
			}
			return ['ok' => true, 'result' => $p['settings']['admins'][$user_id]];
		} else {
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: administrator not found"];
		}
	}
}

function removeAdmin($p = false, $user_id = false) {
	if ($p === false) {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione removeAdmin");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
		$p = sendPoll($p['poll_id'], $p['creator']);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			return $p;
		}
	} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
	} else {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione removeAdmin");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$p is invalid"];
	}
	if (!isset($p['status']) ) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	}
	if(!$user_id) {
		call_error("<b>Error:</b> la variabile \$user_id non è stata settata nella funzione removeAdmin"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$user_id was not found"];
	} elseif(!is_numeric($user_id)) {
		call_error("<b>Error:</b> la variabile \$user_id non è numerica nella funzione removeAdmin"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$user_id must be a numeric value"];
	} else {
		if (isset($p['settings']['admins'][$user_id])) {
			unset($p['settings']['admins'][$user_id]);
			$q = db_query("UPDATE polls SET settings = ? WHERE poll_id = ? and user_id = ?", [json_encode($p['settings']), $p['poll_id'], $p['creator']], "no");
			return ['ok' => true, 'result' => $q];
		} else {
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: administrator not found"];
		}
	}
}

function isAdmin($user_id = false, $p = false) {
	if ($p === false) {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione isAdmin");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
		$p = sendPoll($p['poll_id'], $p['creator']);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			return $p;
		}
	} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
	} else {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione isAdmin");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$p is invalid"];
	}
	if (!isset($p['status']) ) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	}
	if(!$user_id) {
		call_error("<b>Error:</b> la variabile \$user_id non è stata settata nella funzione isAdmin"); 
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$user_id was not found"];
	} elseif(!is_numeric($user_id)) {
		call_error("<b>Error:</b> la variabile \$user_id non è numerica nella funzione isAdmin"); 
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$user_id must be a numeric value"];
	} else {
		if (round($user_id) === round($p['creator'])) {
			return ['ok' => true, 'result' => true];
		} elseif ($p['settings']['admins'][$user_id]['status'] == "administrator") {
			return ['ok' => true, 'result' => true];
		} else {
			return ['ok' => true, 'result' => false];
		}
	}
}

function getPollPercentage($p = false) {
	if ($p === false) {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione getPollPercentage");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
		$p = sendPoll($p['poll_id'], $p['creator']);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			return $p;
		}
	} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
	} else {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione getPollPercentage");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$p is invalid"];
	}
	if (!isset($p['status']) ) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	}
	foreach($p['choice'] as $choice => $keys) {
		$total = $keys + $total;
	}
	$result = ["poll_id" => $p['poll_id'], "creator" => $p['creator'], "total" => $total];
	foreach($p['choice'] as $choice => $keys) {
		$choiceCount = $keys;
		if ($total === 0) {
			$n = 0;
		} else {
			$n = nn($choiceCount*100 / $total);
		}
		$result[0][$choice] = nn($n);
	}	
	return ['ok' => true, 'result' => $result];
}
	
function getTotalChoice($p = false) {
	if ($p === false) {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione getTotalChoice");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$p was not found"];
	} elseif (isset($p['poll_id']) and isset($p['creator']) and !isset($p['title'])) {
		$p = sendPoll($p['poll_id'], $p['creator']);
		if ($p['ok']) {
			$p = $p['result'];
		} else {
			return $p;
		}
	} elseif (isset($p['poll_id']) and isset($p['creator']) and isset($p['title'])) {
	} else {
		call_error("<b>Warning:</b> la variabile \$p non è presente nella funzione getTotalChoice");
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$p is invalid"];
	}
	if (!isset($p['status']) ) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: poll not found", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	} elseif ($p['status'] == "deleted") {
		return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: poll deleted", 'text' => $r, 'menu' => false, 'disable_web_preview' => $allegat];
	}
	$count = 0;
	foreach(json_decode($p['choices'], true) as $choice => $keys) {
		$choiceCount = count($keys);
		$count += $choiceCount;
	}
	return ['ok' => true, 'result' => $count];
}

function getTotalPoll($creator = false) {
	if(!$creator) {
		call_error("<b>Error:</b> la variabile \$creator non è stata settata nella funzione getTotalPoll");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator was not found"];
	} elseif(!is_numeric($creator)) {
		call_error("<b>Error:</b> la variabile \$creator non è numerica nella funzione getTotalPoll");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator must be a numeric value"];
	} else {
		return db_query("SELECT * FROM polls WHERE user_id = ? ORDER BY poll_id", [$creator], false);
	}
}

function getAllPoll($creator = false, $limit) {
	if(!$creator) {
		call_error("<b>Error:</b> la variabile \$creator non è stata settata nella funzione getTotalPoll");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator was not found"];
	} elseif(!is_numeric($creator)) {
		call_error("<b>Error:</b> la variabile \$creator non è numerica nella funzione getTotalPoll");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator must be a numeric value"];
	} else {
		return db_query("SELECT * FROM polls WHERE user_id = ? and (status = ? or status = ?) ORDER BY poll_id LIMIT ?", [$creator, 'open', 'closed', $limit], false);
	}
}

function getActivePoll ($creator = false, $limit = 999) {
	if(!$creator) {
		call_error("<b>Error:</b> la variabile \$creator non è stata settata nella funzione getActivePoll");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator was not found"];
	} elseif(!is_numeric($creator)) {
		call_error("<b>Error:</b> la variabile \$creator non è numerica nella funzione getActivePoll");
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$creator must be a numeric value"];
	} else {
		return db_query("SELECT * FROM polls WHERE user_id = ? and status = 'open' ORDER BY poll_id LIMIT ?", [$creator, $limit], false);
	}
}

function sorta($choices = []) {
	if (is_array($choices)) {
		if (count($choices) !== 1) {
			foreach($choices as $choice => $value) {
				$array[$value][] = $choice;
			}
			foreach ($array as $value => $choicess) {
				 sort($choicess, SORT_STRING);
				 $array[$value] = $choicess;
			}
			ksort($array, SORT_NUMERIC);
			$array = array_reverse($array, true);
			foreach($array as $value => $choices) {
				foreach($choices as $choice) {
					$newarray[$choice] = $value;
				}
			}
			return ['ok' => true, 'result' => $newarray];
		} else {
			return ['ok' => true, 'result' => $choices];
		}
	} else {
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$choices must be an array"];
	}
}

function nn($n = 0) {
	if ($n == "NAN") $n = "0";
	$n = round($n);
	if ($n === 'NAN') $n = "0";
	if (is_numeric($n)) {
		return explode(".", $n)[0];
	} else {
		$n = "0";
	}
	return $n;
}

function ari($arr, $item) {
	if(in_array($item, $arr)) {
		unset($arr[array_search($item, $arr)]); 
		return array_values($arr);
	}else{
		return $arr;
	}
}

function isSpam($text, $entities = []) {
	global $config;
	if ($entities == []) return false;
	foreach($entities as $entity) {
		if ($entity['type'] == 'mention') return true;
		if ($entity['type'] == 'url') return true;
	}
	if ($config['response']) {
		$m = sm($config['console'], $text)['result'];
		dm($config['console'], $m['message_id']);
		$entities = $m['entities'];
		if ($entities == []) return false;
		foreach($entities as $entity) {
			if ($entity['type'] == 'mention') return true;
			if ($entity['type'] == 'url') return true;
		}
	}
	return false;
}

function text_num ($text = "") {
	$nums = [
		"0" => "o",
		"1" => "i",
		"2" => "z",
		"3" => "e",
		"4" => "a",
		"5" => "s",
		"6" => "g",
		"7" => "l",
		"8" => "b",
		"9" => "g"
	];
	foreach($nums as $key => $value) {
		$text = str_replace($key, $value, $text);
	}
	return $text;
}

function text_emoji ($text = "") {
	$nums = [
		"0️⃣" => "0",
		"1️⃣" => "1",
		"2️⃣" => "2",
		"3️⃣" => "3",
		"4️⃣" => "4",
		"5️⃣" => "5",
		"6️⃣" => "6",
		"7️⃣" => "7",
		"8️⃣" => "8",
		"9️⃣" => "9",
		"🔟" => "10",
		"💯" => "100",
		"❗️" => "!",
		"❕" => "!",
		"‼️" => "!!",
		"❓" => "?",
		"❔" => "?",
		"⁉️" => "!?",
		"#️⃣" => "#",
		"➕" => "+",
		"➖" => "-",
		"🅰️" => "a",
		"🆎" => "ab",
		"🏧" => "atm",
		"🅱️" => "b",
		"🆑" => "cl",
		"🆒" => "cool",
		"🆓" => "free",
		"ℹ️" => "i",
		"🆕" => "new",
		"🆖" => "ng",
		"🅿️" => "p",
		"*️⃣" => "*",
		"🆙" => "up",
		"⭕️" => "o",
		"🅾️" => "o",
		"🆗" => "ok",
		"🆘" => "sos",
		"🚾" => "wc",
		"❌" => "x",
		"💤" => "z"
	];
	foreach($nums as $key => $value) {
		$text = str_replace($key, $value, $text);
	}
	return $text;
}

function isForbidden($text = " ", $blwords = []) {
	$text = strtolower($text);
	if (!$blwords or $blwords === []) return false;
	if(in_array($text, $blwords)) return true;
	foreach($blwords as $word) {
		$texta = text_num($text);
		$textb = text_emoji($text);
		$textc = text_emoji($texta);
		$textd = text_num($textb);
		if (strpos($text, $word) === 0) {
			return true;
		} elseif (strpos($text, $word) !== false) {
			return true;
		} elseif (strpos($texta, $word) === 0) {
			return true;
		} elseif (strpos($texta, $word) !== false) {
			return true;
		} elseif (strpos($textb, $word) === 0) {
			return true;
		} elseif (strpos($textb, $word) !== false) {
			return true;
		} elseif (strpos($textc, $word) === 0) {
			return true;
		} elseif (strpos($textc, $word) !== false) {
			return true;
		} elseif (strpos($textd, $word) === 0) {
			return true;
		} elseif (strpos($textd, $word) !== false) {
			return true;
		}
	}
	return false;
}
