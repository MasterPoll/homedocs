<?php

if (!$integ) {
	if (substr(php_sapi_name(), 0, 3) == 'cgi') {
		header("Status: 502 Bad Gateway");
	} else {
		header("HTTP/1.1 502 Bad Gateway");
	}
	echo json_encode(['ok' => false, 'error' => 502, 'description' => "Bad Gateway: #0"]);
	die;
} else {
	$pluginp = "integrations.php";
}
$ok = false;
ini_set('memory_limit', "-1");

// Configuration
@require("/home/masterpoll-documents/api-config.php");
if (!isset($config)) {
	if (substr(php_sapi_name(), 0, 3) == 'cgi') {
		header("Status: 502 Bad Gateway");
	} else {
		header("HTTP/1.1 502 Bad Gateway");
	}
	echo json_encode(['ok' => false, 'status_code' => 502, 'description' => "Bad Gateway: #2", 'monitor_url' => $config['monitor_url']]);
	die;
}
if ($config['logs_token']) {
	function call_error($error = "Null", $plugin = 'no', $chat = 'def') {
		global $config;
		global $pluginp;
		global $poll_id;
		global $creator;
		global $userID;
		if ($chat == 'def') {
			$chat = $config['console'];
		}
		if (!$pluginp) {
		} elseif ($plugin == 'no') {
			$plugin = $pluginp;
		}
		if (isset($poll_id) or isset($creator)) {
			$sondaggio = "\n<b>Sondaggio:</b> $poll_id-$creator";
		}
		$text = "#IntAPIError \n$error \n<b>Plugin:</b> $plugin $sondaggio\n<b>API User</b>: <code>$userID</code>";
		$args = [
			'chat_id' => $chat,
			'text' => $text,
			'parse_mode' => 'html'
		];
		$ch = curl_init();
		$url .= "https://api.telegram.org/bot" . $config['logs_token'] ."/sendMessage?" . http_build_query($args);
		curl_setopt_array($ch, [
			CURLOPT_URL => $url,
			CURLOPT_POST => false,
			CURLOPT_CONNECTTIMEOUT_MS => 100,
			CURLOPT_RETURNTRANSFER => true
		]);
		$output = curl_exec($ch);
		return true;
	}
	# Segnalazione errori php
	set_error_handler("errorHandler");
	register_shutdown_function("shutdownHandler");
	function errorHandler($error_level, $error_message, $error_file, $error_line, $error_context) {
		global $config;
		$error = $error_message . " \nLine: " . $error_line;
		switch ($error_level) {
			case E_ERROR:
			case E_CORE_ERROR:
			case E_COMPILE_ERROR:
			case E_PARSE:
				echo json_encode(['ok' => false, 'status_code' => 502, 'description' => "Bad Gateway: #2", 'monitor_url' => $config['monitor_url']]);
				fastcgi_finish_request();
				if ($config['log_report']['FATAL']) {
					call_error("FATAL: " . $error, $error_file);
				}
				break;
			case E_USER_ERROR:
			case E_RECOVERABLE_ERROR:
				echo json_encode(['ok' => false, 'status_code' => 502, 'description' => "Bad Gateway: #2", 'monitor_url' => $config['monitor_url']]);
				fastcgi_finish_request();
				if ($config['log_report']['ERROR']) {
					call_error("ERROR: " . $error, $error_file);
				}
				break;
			case E_WARNING:
			case E_CORE_WARNING:
			case E_COMPILE_WARNING:
			case E_USER_WARNING:
				if ($config['log_report']['WARN']) {
					call_error("WARNING: " . $error, $error_file);
				}
				break;
			case E_NOTICE:
			case E_USER_NOTICE:
				if ($config['log_report']['INFO']) {
					call_error("INFO: " . $error, $error_file);
				}
				break;
			case E_STRICT:
				if ($config['log_report']['DEBUG']) {
					call_error("DEBUG: " . $error, $error_file);
				}
				break;
			default:
				if ($config['log_report']['WARN']) {
					call_error("WARNING: " . $error, $error_file);
				}
		}
	}
	function shutdownHandler() {
		global $config;
		$lasterror = error_get_last();
		switch ($lasterror['type']) {
			case E_ERROR:
			case E_CORE_ERROR:
			case E_COMPILE_ERROR:
			case E_USER_ERROR:
			case E_RECOVERABLE_ERROR:
			case E_CORE_WARNING:
			case E_COMPILE_WARNING:
			case E_PARSE:
				if ($config['log_report']['SHUTDOWN']) {
					$error = $lasterror['message'] . " \nLine: " . $lasterror['line'];
					call_error($error, $lasterror['file']);
				}
				echo json_encode(['ok' => false, 'status_code' => 502, 'description' => "Bad Gateway: #2", 'monitor_url' => $config['monitor_url']]);
				fastcgi_finish_request();
		}
	}
} else {
	if (substr(php_sapi_name(), 0, 3) == 'cgi') {
		header("Status: 502 Bad Gateway");
	} else {
		header("HTTP/1.1 502 Bad Gateway");
	}
	echo json_encode(['ok' => false, 'error' => 502, 'description' => "Bad Gateway: #1"]);
	die;
}

$f['languages'] = "languages.json";
$config['parse_mode'] = "html";
$config['bottino_auth'] = [723935489, 836296867, 244432022];
$methods = [
	'getfuckingsuperpowa' => true,
	'getfuckingsuperultrapowa' => true
];
if (!empty($_GET)) {
	$query = $_GET;
} elseif (!empty($_POST)) {
	$query = $_POST;
} elseif (!empty(file_get_contents("php://input"))) {
	$query = json_decode(file_get_contents("php://input"), true);
} else {
	$query = [];
}
$request = strtolower(str_replace("?" . $_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']));
function log_api($response = [], $tosend = 'def') {
	global $config;
	global $pluginp;
	global $poll_id;
	global $creator;
	global $userID;
	global $islocal;
	global $request;
	if (!$config['logs']) return false;
	if ($tosend === 'def') $tosend = false;
	$chat = $config['console'];
	$plugin = $pluginp;
	if (isset($poll_id) or isset($creator)) {
		$sondaggio = "\n<b>Sondaggio:</b> $poll_id-$creator";
	}
	if ($userID) {
		$user = "<b>Utente:</> " . $userID;
		$hashtag .= "#id$userID ";
	} elseif ($islocal) {
		$user = "<b>Utente:</> locale";
	} else {
		$user = "<b>Utente:</> non identificato";
		$hashtag .= "#integrations #ip_" . str_replace('.', '_', $_SERVER['REMOTE_ADDR']) . " ";
	}
	if ($response['ok']) {
		$r = "<b>Risultato:</> ✅";
	} else {
		$r = "<b>Risultato:</> ❌";
		$r .= "\n<b>Error " . $response['error_code'] . ":</> " . $response['description'];
		$hashtag .= "#Errors ";
	}
	$text = "#Request $hashtag\n<b>Metodo:</> $request\n$r\n$user\n<b>Plugin:</b> $plugin $sondaggio";
	if ($tosend) {
		$args = [
			'chat_id' => $chat,
			'text' => $text,
			'parse_mode' => 'html'
		];
		$ch = curl_init();
		$url .= "https://api.telegram.org/bot" . $config['logs_token'] ."/sendMessage?" . http_build_query($args);
		curl_setopt_array($ch, [
			CURLOPT_URL => $url,
			CURLOPT_POST => false,
			CURLOPT_CONNECTTIMEOUT_MS => 100,
			CURLOPT_RETURNTRANSFER => true
		]);
		curl_exec($ch);
		curl_close($ch);
	}
	return true;
}
function done() {
	global $ok;
	global $config;
	global $result;
	global $error;
	global $error_description;
	$json['ok'] = $ok;
	if ($result === "status") {
		$json['result'] = true;
		$json['monitor_url'] = $config['monitor_url'];
	} elseif (isset($result)) {
		$json['result'] = $result;
	} elseif (isset($error)) {
		$json['status_code'] = $error;
		if (isset($error_description)) {
			$json['description'] = $error_description;
		} else {
			$json['description'] = $config['errors'][$error];
		}
		if (in_array($error, [500, 502, 503])) $json['monitor_url'] = $config['monitor_url'];
	} else {
		if ($monitor) {
			
		}
		$json['result'] = $ok;
	}
	if ($ok) {
		if (substr(php_sapi_name(), 0, 3) == 'cgi') {
			header("Status: 200");
		} else {
			header("HTTP/1.1 200");
		}
	} else {
		if (substr(php_sapi_name(), 0, 3) == 'cgi') {
			header("Status: $error " . $config['errors'][$error]);
		} else {
			header("HTTP/1.1 $error " . $config['errors'][$error]);
		}
	}
	echo json_encode($json);
	fastcgi_finish_request();
	log_api($json);
	return true;
}

if ($config['redis']) {
	if (!class_exists("Redis")) {
		$error = 500;
		$error_description = $config['errors'][$error] . ": Redis not work at this moment...";
		done();
		die;
	}
	$redisc = $config['redis'];
	try {
		$redis = new Redis();
		$redis->connect($redisc['host'], $redisc['port']);
	} catch (Exception $e) {
		$error = 500;
		$error_description = $config['errors'][$error];
		done();
		die;
	}
	if ($redisc['password'] !== false) {
		try {
			$redis->auth($redisc['password']);
		} catch (Exception $e) {
			$error = 500;
			$error_description = $config['errors'][$error];
			done();
			die;
		}
	}
	if ($redisc['database'] !== false) {
		try {
			$redis->select($redisc['database']);
		} catch (Exception $e) {
			$error = 500;
			$error_description = $config['errors'][$error];
			done();
			die;
		}
	}
} else {
	$error = 500;
	$error_description = $config['errors'][$error] . ": Redis is offline at this moment...";
	done();
	die;
}
if ($config['database']) {
	function db_query($query, $args = false, $fetch = false) {
		global $PDO;
		if (!$PDO) {
			call_error("#PDOError \nQuery: " . code($query) . " \nDatabase non avviato.");
			return ['ok' => false, 'error_code' => 500, 'description' => "Internal Server Error: database not started"];
		}
		try {
			$q = $PDO->prepare($query);
			$db_query['ok'] = true;
		} catch (PDOException $e) {
			$db_query['ok'] = false;
			$db_query['error'] = $e;
			return $db_query;
		}
		if (is_array($args)) {
			$q->execute($args);
			$db_query['args'] = $args;
		} else {
			$q->execute();
		}
		$error = $q->errorInfo();
		if ($error[0] !== "00000") {
			$db_query['ok'] = false;
			call_error("PDO Error\n<b>INPUT:</> " . code($query) . "\n<b>OUTPUT:</> " . code(json_encode($error)));
			$db_query['error_code'] = $error[0];
			$db_query['description'] = "[MYSQL]" . $error[2];
			return $db_query;
		}
		if ($fetch) {
			$db_query['result'] = $q->fetch(\PDO::FETCH_ASSOC);
		} elseif ($fetch == "no") {
			$db_query['result'] = true;
		} else {
			$db_query['result'] = $q->fetchAll();
		}
		return $db_query;
	}
	function getName($userID = false) {
		global $PDO;
		global $config;
		if (!$userID) {
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$userID was not found", 'result' => "Unknown user"];
		} elseif (!is_numeric($userID)) {
			return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$userID must be a numeric value", 'result' => "Unknown user"];
		}
		if (!$PDO) {
			return ['ok' => false, 'error_code' => 500, 'description' => "Internal Server Error: database not started"];
		}
		if ($config['usa_redis']) {
			$nomer = rget("name$userID");
			if ($nomer['ok'] and $nomer['result'] and !isset($nomer['result']['ok'])) return ['ok' => true, 'redis' => $nomer, 'result' => $nomer['result']];
		}
		$q = $PDO->prepare("SELECT * FROM utenti WHERE user_id = ?");
		$q->execute([$userID]);
		$error = $q->errorInfo();
		if ($error[0] !== "00000") {
			call_error("PDO Error\n<b>INPUT:</> " . code($q) . "\n<b>OUTPUT:</> " . code(json_encode($error)));
			$db_query = [
				'ok' => false,
				'error_code' => $error[0],
				'description' => "[PGSQL]" . $error[2],
				'result' => "Deleted account"
			];
		} else {
			$u = $q->fetchAll()[0];
			if ($u['cognome']) $u['nome'] .= " " . $u['cognome'];
			if (isset($u['nome'])) {
				$rr = htmlspecialchars($u['nome']);
			} else {
				$rr = "Deleted account";
			}
			if ($config['usa_redis']) {
				$res = rset("name$userID", $rr);
			}
			$db_query = [
				'ok' => true,
				'redis' => $res,
				'result' => $rr
			];
		}
		return $db_query;
	}
} else {
	$error = 500;
	$error_description = $config['errors'][$error] . ": Database is offline at this moment...";
	done();
	die;
}

if (strpos($request, "/user") === 0) {
	$q = explode("/", $_SERVER['REQUEST_URI']);
	$token = str_replace("user", '', $q[1]);
	$request = str_replace(strtolower("/user$token"), '', $request);
	$userID = explode(":", $token)[0];
	if (!in_array($userID, $config['bottino_auth'])) {
		$access = false;
		$error = 401;
		$error_description = $config['errors'][$error] . ": user id not authorized";
		done();
		die;
	}
	$password = explode(":", $token)[1];
	if (!$password or !is_numeric($userID)) {
		$access = false;
		$error = 401;
		$error_description = $config['errors'][$error] . ": invalid user key";
	} else {
		if (!$PDO) {
			try {
				$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
			} catch (PDOException $e) {
				$error = 500;
				$error_description = $config['errors'][$error] . " database connection lost";
				done();
				die;
			}
		}
		$user = db_query("SELECT * FROM utenti WHERE user_id = ?", [$userID], true);
		if ($user['ok']) {
			$user = $user['result'];
		} else {
			$error = 500;
			$error_description = $config['errors'][$error] . " database error";
			done();
			die;
		}
		if ($user['user_id'] == $userID) {
			$userID = $user['user_id'];
			if (in_array($user['user_id'], $config['admins'])){
				$isadmin = true;
			} else {
				$isadmin = false;
			}
			$user['settings'] = json_decode($user['settings'], true);
			$user['status'] = json_decode($user['status'], true);
			if (strpos($user['status'][689942156], "ban") === 0) {
				$error = 401;
				$error_description = $config['errors'][$error] . ": user banned";
				done();
				die;
			} elseif ($password == $user['settings']['token']) {
				$access = true;
			} else {
				$access = false;
				$error = 401;
				$error_description = $config['errors'][$error] . ": wrong user key";
			}
		} else {
			$access = false;
			$error = 401;
			$error_description = $config['errors'][$error] . ": user not exists";
		}
	}
} else {
	$access = false;
	$error = 401;
	$error_description = $config['errors'][$error] . ": user id not authorized";
}

if ($config['class_work']) {
	function bot_decode($string) {
		global $config;
		$key = hash('sha256', $config['secret_key']);
		$iv = substr(hash('sha256', $config['secret_iv']), 0, 8);
		$datas = openssl_decrypt(base64_decode($string), $config['encrypt_method'], $key, 0, $iv);
		return $datas;
	}
	function bot_encode($datas) {
		global $config;
		if (is_array($datas)) {
			$datas = json_encode($datas);
		}
		$key = hash('sha256', $config['secret_key']);
		$iv = substr(hash('sha256', $config['secret_iv']), 0, 8);
		$string = str_replace('=', '', base64_encode(openssl_encrypt($datas, $config['encrypt_method'], $key, 0, $iv)));
		return $string;
	}
	# Functions for formatting messages
	function textspecialchars($text, $format = 'def') {
		global $config;
		if ($format === 'def') {
			$format = $config['parse_mode'];
		}
		if (strtolower($format) == 'html') {
			return htmlspecialchars($text);
		} elseif (strtolower($format) == 'markdown') {
			return mdspecialchars($text);
		} else {
			call_error("Unknown formatting for textspecialchars: $format");
		}
		return $text;
	}
	function mdspecialchars($text) {
		# Caratteri come "*", "_" e "`" visibili in markdown
		$text = str_replace("_", "\_", $text);
		$text = str_replace("*", "\*", $text);
		$text = str_replace("`", "\`", $text);
		return str_replace("[", "\[", $text);
	}
	function code($text = false) {
		global $config;
		if (!is_string($text) and !is_numeric($text)) {
			call_error("ERROR_010: " . json_encode($text));
			$text = "{ERROR_010}";
		}
		if (strtolower($config['parse_mode']) == 'html') {
			return "<code>" . htmlspecialchars($text) . "</>";
		} else {
			return "`" . mdspecialchars($text) . "`";
		}
	}
	function bold($text) {
		global $config;
		if (!is_string($text) and !is_numeric($text)) {
			call_error("ERROR_011: " . json_encode($text));
			$text = "{ERROR_011}";
		}
		if (strtolower($config['parse_mode']) == 'html') {
			return "<b>" . htmlspecialchars($text) . "</>";
		} else {
			return "*" . mdspecialchars($text) . "*";
		}
	}
	function italic($text) {
		global $config;
		if (!is_string($text) and !is_numeric($text)) {
			call_error("ERROR_012: " . json_encode($text));
			$text = "{ERROR_012}";
		}
		if (strtolower($config['parse_mode']) == 'html') {
			return "<i>" . htmlspecialchars($text) . "</>";
		} else {
			return "_" . mdspecialchars($text) . "_";
		}
	}
	function text_link($text, $link) {
		global $config;
		if (!is_string($text) and !is_numeric($text)) {
			call_error("ERROR_013: " . json_encode($text));
			$text = "{ERROR_013}";
		}
		if (strtolower($config['parse_mode']) == 'html') {
			return "<a href='$link'>" . htmlspecialchars($text) . "</>";
		} else {
			return "[" . mdspecialchars($text) . "]($link)";
		}
	}
	function tag($user = false, $name = false, $surname = false) {
		global $nome;
		global $cognome;
		global $userID;
		if ($user) {
			if ($surname) $name .= " $surname";
		} else {
			$user = $userID;
			if ($cognome) $nome .= " $cognome";
			$name = $nome;
		}
		return text_link($name, "tg://user?id=$user");
	}
	require("/var/www/bot/hwgxh7pollbotwg47ej2/PollCreator-Class.php");
	require("/var/www/bot/hwgxh7pollbotwg47ej2/plugins/language.php");
} else {
	$error = 502;
	done();
	die;
}

if ($access and !isset($error)) {
	
	# Errore 405: Metodo sconosciuto
	if (!$methods[str_replace("/", '', $request)]) {
		$error = 405;
		done();
		die;
	}
	
	if ($request == "/getfuckingsuperpowa") {
		if (isset($query['string'])) {
			$query['string'] = str_replace("cburl-", '', $query['string']);
			$string = bot_decode($query['string']);
			foreach (["share_", "board_", "list_", "mypoll_", "append_"] as $cbtype) {
				if (strpos($string, "$cbtype") === 0) {
					$string = str_replace("$cbtype", '', $string);
					$code = str_replace("_", '', " $cbtype");
					$cbcode = $cbtype;
				}
			}
			if ($cbcode) {
				$string = str_replace($cbcode, '', $string);
				$e = explode("-", $string, 2);
				if (is_numeric($e[0]) and is_numeric($e[1])) {
					$p = sendPoll($e[0], $e[1]);
					if ($p['ok']) {
						$p = $p['result'];
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": database error";
						done();
					}
					if (!$p['status']) {
						$error = 404;
						$error_description = $config['errors'][$error] . ": poll not found";
						done();
						die;
					} elseif (!$p['status'] or $p['status'] == "deleted") {
						$error = 400;
						$error_description = $config['errors'][$error] . ": poll deleted";
						done();
						die;
					} elseif (!$config['types'][$p['type']]) {
						$error = 400;
						$error_description = $config['errors'][$error] . ": poll type in maintenance";
						done();
						die;
					} else {
						$ok  = true;
						$result = pollToArray($p);
						$result['vote_link'] = bot_encode("mypoll_{$result['poll_id']}-{$result['owner_id']}");
						$message = pollToMessage($p);
						$result['text_message'] = $message['text'];
						if ($message['disable_web_preview']) {
							$result['disable_web_page_preview'] = true;
						} else {
							$result['disable_web_page_preview'] = false;
						}
					}
				} else {
					$error = 400;
					$error_description = $config['errors'][$error] . ": invalid$code string";
				}
			} else {
				$error = 400;
				$error_description = $config['errors'][$error] . ": invalid string";
			}
		} else {
			$error = 400;
			$error_description = $config['errors'][$error] . ": string is empty";
		}
	} elseif ($request == "/getfuckingsuperultrapowa") {
		if (isset($query['id'])) {
			if (is_numeric($query['id'])) {
				if (isset($query['owner_id'])) {
					if (is_numeric($query['owner_id'])) {
						$creator = $query['owner_id'];
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": owner_id must be a numeric value";
						done();
						die;
					}
				} else {
					$error = 400;
					$error_description = $config['errors'][$error] . ": owner_id is empty";
					done();
					die;
				}
				if (!$PDO) {
					try {
						$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
					} catch (PDOException $e) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database connection lost";
						done();
						die;
					}
				}
				$poll = sendPoll($query['id'], $creator);
				if ($poll['ok']) {
					$poll = $poll['result'];
				} else {
					$error = 500;
					done();
					die;
				}
				if (!$poll['status']) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": poll not found";
				} elseif ($poll['status'] == "deleted") {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll deleted";
				} elseif (!$config['types'][$poll['type']]) {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll type in maintenance";
				} else {
					$ok = true;
					$result = pollToArray($poll);
					if ($poll['type'] == "participation") {
						if (in_array($userID, $poll['choices']['participants'])) {
							$result['participating'] = true;
						} else {
							$result['participating'] = false;
						}
					}
					$result['vote_link'] = bot_encode("mypoll_{$result['poll_id']}-{$result['owner_id']}");
					$message = pollToMessage($poll);
					$result['text_message'] = $message['text'];
					if ($message['disable_web_preview'] or $message['disable_web_preview'] === null) {
						$result['disable_web_page_preview'] = true;
					} else {
						$result['disable_web_page_preview'] = false;
					}
				}
			} else {
				$error = 400;
				$error_description = $config['errors'][$error] . ": id must be a numeric value";
			}
		} else {
			$error = 400;
			$error_description = $config['errors'][$error] . ": id is empty";
		}
	} else {
		$error = 404;
		$error_description = $config['errors'][$error] . ": Method not found";
	}
	
} else {
	if (!isset($error)) $error = 405;
}

done();

?>
