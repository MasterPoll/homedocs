<?php

if (!$api) {
	if (substr(php_sapi_name(), 0, 3) == 'cgi') {
		header("Status: 502 Bad Gateway");
	} else {
		header("HTTP/1.1 502 Bad Gateway");
	}
	echo json_encode(['ok' => false, 'error' => 502, 'description' => "Bad Gateway: #0"]);
	die;
} else {
	$pluginp = "api.php";
}
$ok = false;

// Configuration
@require("/home/masterpoll-documents/api-config.php");
if (!isset($config)) {
	if (substr(php_sapi_name(), 0, 3) == 'cgi') {
		header("Status: 502 Bad Gateway");
	} else {
		header("HTTP/1.1 502 Bad Gateway");
	}
	echo json_encode(['ok' => false, 'status_code' => 502, 'description' => "Bad Gateway: #2", 'monitor_url' => $config['monitor_url']]);
	die;
}
if ($config['logs_token']) {
	function call_error($error = "Null", $plugin = 'no', $chat = 'def') {
		global $config;
		global $pluginp;
		global $poll_id;
		global $creator;
		global $userID;
		if ($chat == 'def') {
			$chat = $config['console'];
		}
		if (!$pluginp) {
		} elseif ($plugin == 'no') {
			$plugin = $pluginp;
		}
		if (isset($poll_id) or isset($creator)) {
			$sondaggio = "\n<b>Sondaggio:</b> $poll_id-$creator";
		}
		$text = "#APIError \n$error \n<b>Plugin:</b> $plugin $sondaggio\n<b>API User</b>: <code>$userID</code>";
		$args = [
			'chat_id' => $chat,
			'text' => $text,
			'parse_mode' => 'html'
		];
		$ch = curl_init();
		$url .= "https://api.telegram.org/bot" . $config['logs_token'] ."/sendMessage?" . http_build_query($args);
		curl_setopt_array($ch, [
			CURLOPT_URL => $url,
			CURLOPT_POST => false,
			CURLOPT_CONNECTTIMEOUT_MS => 100,
			CURLOPT_RETURNTRANSFER => true
		]);
		$output = curl_exec($ch);
		return true;
	}
	if (file_exists("/home/masterpoll-documents/api-methods.json")) {
		$methods = json_decode(file_get_contents("/home/masterpoll-documents/api-methods.json"), true);
	} else {
		$methods = [];
	}
	# Segnalazione errori php
	set_error_handler("errorHandler");
	register_shutdown_function("shutdownHandler");
	function errorHandler($error_level, $error_message, $error_file, $error_line, $error_context) {
		global $config;
		$error = $error_message . " \nLine: " . $error_line;
		switch ($error_level) {
			case E_ERROR:
			case E_CORE_ERROR:
			case E_COMPILE_ERROR:
			case E_PARSE:
				echo json_encode(['ok' => false, 'status_code' => 502, 'description' => "Bad Gateway: #2", 'monitor_url' => $config['monitor_url']]);
				fastcgi_finish_request();
				if ($config['log_report']['FATAL']) {
					call_error("FATAL: " . $error, $error_file);
				}
				break;
			case E_USER_ERROR:
			case E_RECOVERABLE_ERROR:
				echo json_encode(['ok' => false, 'status_code' => 502, 'description' => "Bad Gateway: #2", 'monitor_url' => $config['monitor_url']]);
				fastcgi_finish_request();
				if ($config['log_report']['ERROR']) {
					call_error("ERROR: " . $error, $error_file);
				}
				break;
			case E_WARNING:
			case E_CORE_WARNING:
			case E_COMPILE_WARNING:
			case E_USER_WARNING:
				if ($config['log_report']['WARN']) {
					call_error("WARNING: " . $error, $error_file);
				}
				break;
			case E_NOTICE:
			case E_USER_NOTICE:
				if ($config['log_report']['INFO']) {
					call_error("INFO: " . $error, $error_file);
				}
				break;
			case E_STRICT:
				if ($config['log_report']['DEBUG']) {
					call_error("DEBUG: " . $error, $error_file);
				}
				break;
			default:
				if ($config['log_report']['WARN']) {
					call_error("WARNING: " . $error, $error_file);
				}
		}
	}
	function shutdownHandler() {
		global $config;
		$lasterror = error_get_last();
		switch ($lasterror['type']) {
			case E_ERROR:
			case E_CORE_ERROR:
			case E_COMPILE_ERROR:
			case E_USER_ERROR:
			case E_RECOVERABLE_ERROR:
			case E_CORE_WARNING:
			case E_COMPILE_WARNING:
			case E_PARSE:
				if ($config['log_report']['SHUTDOWN']) {
					$error = $lasterror['message'] . " \nLine: " . $lasterror['line'];
					call_error($error, $lasterror['file']);
				}
				echo json_encode(['ok' => false, 'status_code' => 502, 'description' => "Bad Gateway: #2", 'monitor_url' => $config['monitor_url']]);
				fastcgi_finish_request();
		}
	}
} else {
	if (substr(php_sapi_name(), 0, 3) == 'cgi') {
		header("Status: 502 Bad Gateway");
	} else {
		header("HTTP/1.1 502 Bad Gateway");
	}
	echo json_encode(['ok' => false, 'error' => 502, 'description' => "Bad Gateway: #1"]);
	die;
}

if (!empty($_GET)) {
	$query = $_GET;
} elseif (!empty($_POST)) {
	$query = $_POST;
} elseif (!empty(file_get_contents("php://input"))) {
	$query = json_decode(file_get_contents("php://input"), true);
} else {
	$query = [];
}
$request = strtolower(str_replace("?" . $_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']));
function log_api($response = [], $tosend = 'def') {
	global $config;
	global $pluginp;
	global $poll_id;
	global $creator;
	global $userID;
	global $islocal;
	global $request;
	if (!$config['logs']) return false;
	if ($tosend === 'def') $tosend = false;
	$chat = $config['console'];
	$plugin = $pluginp;
	if (isset($poll_id) or isset($creator)) {
		$sondaggio = "\n<b>Sondaggio:</b> $poll_id-$creator";
	}
	if ($userID) {
		$user = "<b>Utente:</> " . $userID;
		$hashtag .= "#id$userID ";
	} elseif ($islocal) {
		$user = "<b>Utente:</> locale";
	} else {
		$user = "<b>Utente:</> non identificato";
		$hashtag .= "#ip_" . str_replace('.', '_', $_SERVER['REMOTE_ADDR']) . " ";
	}
	if ($response['ok']) {
		$r = "<b>Risultato:</> ✅";
	} else {
		$r = "<b>Risultato:</> ❌";
		$r .= "\n<b>Error " . $response['error_code'] . ":</> " . $response['description'];
		$hashtag .= "#Errors ";
	}
	$text = "#Request $hashtag\n<b>Metodo:</> $request\n$r\n$user\n<b>Plugin:</b> $plugin $sondaggio";
	if ($tosend) {
		$args = [
			'chat_id' => $chat,
			'text' => $text,
			'parse_mode' => 'html'
		];
		$ch = curl_init();
		$url .= "https://api.telegram.org/bot" . $config['logs_token'] ."/sendMessage?" . http_build_query($args);
		curl_setopt_array($ch, [
			CURLOPT_URL => $url,
			CURLOPT_POST => false,
			CURLOPT_CONNECTTIMEOUT_MS => 100,
			CURLOPT_RETURNTRANSFER => true
		]);
		curl_exec($ch);
		curl_close($ch);
	}
	return true;
}
function done() {
	global $ok;
	global $config;
	global $result;
	global $error;
	global $error_description;
	$json['ok'] = $ok;
	if ($result === "status") {
		$json['result'] = true;
		$json['monitor_url'] = $config['monitor_url'];
	} elseif (isset($result)) {
		$json['result'] = $result;
	} elseif (isset($error)) {
		$json['status_code'] = $error;
		if (isset($error_description)) {
			$json['description'] = $error_description;
		} else {
			$json['description'] = $config['errors'][$error];
		}
		if (in_array($error, [500, 502, 503])) $json['monitor_url'] = $config['monitor_url'];
	} else {
		if ($monitor) {
			
		}
		$json['result'] = $ok;
	}
	if ($ok) {
		if (substr(php_sapi_name(), 0, 3) == 'cgi') {
			header("Status: 200");
		} else {
			header("HTTP/1.1 200");
		}
	} else {
		if (substr(php_sapi_name(), 0, 3) == 'cgi') {
			header("Status: $error " . $config['errors'][$error]);
		} else {
			header("HTTP/1.1 $error " . $config['errors'][$error]);
		}
	}
	echo json_encode($json);
	fastcgi_finish_request();
	log_api($json);
	return true;
}

if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
	$_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
}
if ($config['redis']) {
	if (!class_exists("Redis")) {
		$error = 500;
		$error_description = $config['errors'][$error] . ": Redis not work at this moment...";
		done();
		die;
	}
	$redisc = $config['redis'];
	try {
		$redis = new Redis();
		$redis->connect($redisc['host'], $redisc['port']);
	} catch (Exception $e) {
		$error = 500;
		$error_description = $config['errors'][$error];
		done();
		die;
	}
	if ($redisc['password'] !== false) {
		try {
			$redis->auth($redisc['password']);
		} catch (Exception $e) {
			$error = 500;
			$error_description = $config['errors'][$error];
			done();
			die;
		}
	}
	if ($redisc['database'] !== false) {
		try {
			$redis->select($redisc['database']);
		} catch (Exception $e) {
			$error = 500;
			$error_description = $config['errors'][$error];
			done();
			die;
		}
	}
} else {
	$error = 500;
	$error_description = $config['errors'][$error] . ": Redis is offline at this moment...";
	done();
	die;
}
if ($config['database']) {
	function db_query($query, $args = false, $fetch = false) {
		global $PDO;
		if (!$PDO) {
			call_error("#PDOError \nQuery: " . code($query) . " \nDatabase non avviato.");
			return ['ok' => false, 'error_code' => 500, 'description' => "Internal Server Error: database not started"];
		}
		try {
			$q = $PDO->prepare($query);
			$db_query['ok'] = true;
		} catch (PDOException $e) {
			$db_query['ok'] = false;
			$db_query['error'] = $e;
			return $db_query;
		}
		if (is_array($args)) {
			$q->execute($args);
			$db_query['args'] = $args;
		} else {
			$q->execute();
		}
		$error = $q->errorInfo();
		if ($error[0] !== "00000") {
			$db_query['ok'] = false;
			call_error("PDO Error\n<b>INPUT:</> " . code($query) . "\n<b>OUTPUT:</> " . code(json_encode($error)));
			$db_query['error_code'] = $error[0];
			$db_query['description'] = "[MYSQL]" . $error[2];
			return $db_query;
		}
		if ($fetch) {
			$db_query['result'] = $q->fetch(\PDO::FETCH_ASSOC);
		} elseif ($fetch == "no") {
			$db_query['result'] = true;
		} else {
			$db_query['result'] = $q->fetchAll();
		}
		return $db_query;
	}
	function getName($userID = false) {
		global $PDO;
		global $config;
		if (!$userID) {
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$userID was not found", 'result' => "Unknown user"];
		} elseif (!is_numeric($userID)) {
			return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$userID must be a numeric value", 'result' => "Unknown user"];
		}
		if (!$PDO) {
			return ['ok' => false, 'error_code' => 500, 'description' => "Internal Server Error: database not started"];
		}
		if ($config['usa_redis']) {
			$nomer = rget("name$userID");
			if ($nomer['ok'] and $nomer['result'] and !isset($nomer['result']['ok'])) return ['ok' => true, 'redis' => $nomer, 'result' => $nomer['result']];
		}
		$q = $PDO->prepare("SELECT * FROM utenti WHERE user_id = ?");
		$q->execute([$userID]);
		$error = $q->errorInfo();
		if ($error[0] !== "00000") {
			call_error("PDO Error\n<b>INPUT:</> " . code($q) . "\n<b>OUTPUT:</> " . code(json_encode($error)));
			$db_query = [
				'ok' => false,
				'error_code' => $error[0],
				'description' => "[PGSQL]" . $error[2],
				'result' => "Deleted account"
			];
		} else {
			$u = $q->fetchAll()[0];
			if ($u['cognome']) $u['nome'] .= " " . $u['cognome'];
			if (isset($u['nome'])) {
				$rr = htmlspecialchars($u['nome']);
			} else {
				$rr = "Deleted account";
			}
			if ($config['usa_redis']) {
				$res = rset("name$userID", $rr);
			}
			$db_query = [
				'ok' => true,
				'redis' => $res,
				'result' => $rr
			];
		}
		return $db_query;
	}
} else {
	$error = 500;
	$error_description = $config['errors'][$error];
	done();
	die;
}
function checkIfLocalIP() {
	global $_SERVER;
	if ($_SERVER['REMOTE_ADDR'] == $_SERVER['SERVER_ADDR']) {
		return true;
	} else {
		return false;
	}
}

if (checkIfLocalIP()) {
	$access = true;
	$islocal = true;
} else {
	$islocal = false;
	if (strpos($request, "/user") === 0) {
		$q = explode("/", $_SERVER['REQUEST_URI']);
		$token = str_replace("user", '', $q[1]);
		$request = str_replace(strtolower("/user$token"), '', $request);
		$userID = explode(":", $token)[0];
		$password = explode(":", $token)[1];
		if (!$password or !is_numeric($userID)) {
			$access = false;
			$error = 401;
			$error_description = $config['errors'][$error] . ": invalid user key";
		} else {
			if (!$PDO) {
				try {
					$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
				} catch (PDOException $e) {
					$error = 500;
					$error_description = $config['errors'][$error] . " database connection lost";
					done();
					die;
				}
			}
			$user = db_query("SELECT * FROM utenti WHERE user_id = ?", [$userID], true);
			if ($user['ok']) {
				$user = $user['result'];
			} else {
				$error = 500;
				$error_description = $config['errors'][$error] . " database error";
				done();
				die;
			}
			if ($user['user_id'] == $userID) {
				$userID = $user['user_id'];
				if (in_array($user['user_id'], $config['admins'])){
					$isadmin = true;
				} else {
					$isadmin = false;
				}
				$user['settings'] = json_decode($user['settings'], true);
				$user['status'] = json_decode($user['status'], true);
				if (strpos($user['status'][689942156], "ban") === 0) {
					$error = 401;
					$error_description = $config['errors'][$error] . ": user banned";
					done();
					die;
				} elseif ($password == $user['settings']['token']) {
					$access = true;
				} else {
					$access = false;
					$error = 401;
					$error_description = $config['errors'][$error] . ": wrong user key";
				}
			} else {
				$access = false;
				$error = 401;
				$error_description = $config['errors'][$error] . ": user not exists";
			}
		}
	} else {
		$access = 'limited';
	}
}
if ($config['class_work']) {
	function bot_decode($string) {
		global $config;
		$key = hash('sha256', $config['secret_key']);
		$iv = substr(hash('sha256', $config['secret_iv']), 0, 8);
		$datas = openssl_decrypt(base64_decode($string), $config['encrypt_method'], $key, 0, $iv);
		return $datas;
	}
	function bot_encode($datas) {
		global $config;
		if (is_array($datas)) {
			$datas = json_encode($datas);
		}
		$key = hash('sha256', $config['secret_key']);
		$iv = substr(hash('sha256', $config['secret_iv']), 0, 8);
		$string = str_replace('=', '', base64_encode(openssl_encrypt($datas, $config['encrypt_method'], $key, 0, $iv)));
		return $string;
	}
	
	require("/var/www/bot/hwgxh7pollbotwg47ej2/PollCreator-Class.php");
} else {
	$error = 502;
	done();
	die;
}

if ($access and !isset($error)) {
	
	# Errore 405: Metodo sconosciuto
	if (!$methods[str_replace("/", '', $request)]) {
		$error = 405;
		done();
		die;
	}
	
	# Errore 401: Utente limitato all'uso di status
	if ($access == 'limited' and !$userID and !$islocal) {
		if (!in_array(strtolower(str_replace('/', '', $request)), ['status'])) {
			$error = 401;
			$error_description = $config['errors'][$error] . ": Limited user can't use other methods";
			done();
			die;
		}
	}

	$time = time();
	$ip = $_SERVER['REMOTE_ADDR'];
	$method = strtolower(str_replace("/", '', $request));
	if ($config['limits'][$method]) {
		$ex = $redis->get($ip . $method);
		if ($ex >= time() and is_numeric($ex)) {
			header("Retry-After: " . date("r"));
			header("Retry-After: " . round($ex - time() + 1));
			$error = 429;
			$error_description = $config['errors'][$error] . ": retry after " . round($ex - time() + 1);
			done();
			log_api(['ok' => false, 'error_code' => $error, 'description' => $error_description], true);
			die;
		} elseif (!is_numeric($ex)) {
			$redis->set($ip . $method, $time + $config['limits'][$method]);
		} else {
			$redis->set($ip . $method, $time + $config['limits'][$method]);
		}
	}
	
	if (isset($query) and is_array($query) and !empty($query)) {
		foreach (array_keys($query) as $key) {
			if (!in_array($key, $config['place_holders'][$method])) {
				$ok = false;
				$error = 400;
				$error_description = $config['errors'][$error] . ": parameter $key isn't expected here";
				done();
				die;
			}
		}
	}
	
	if ($request == "/status") {
		$ok = true;
		$result = "status";
		if (isset($query['alsossio'])) {
			$result = "You found a Sossio easter egg!";
		}
		done();
	} elseif ($request == "/getsubscribers") {
		try {
			$ex = $redis->get('subs-' . time());
		} catch (RedisException $e) {
			$error = 500;
			$error_description = $config['errors'][$error] . ": cache database connection lost";
			done();
			die;
		}
		$ok = true;
		if ($ex) {
			$subs = round($ex);
		} else {
			if (!$PDO) {
				try {
					$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
				} catch (PDOException $e) {
					$error = 500;
					$error_description = $config['errors'][$error] . ": database connection lost";
					done();
					die;
				}
			}
			$q = db_query("SELECT COUNT(*) FROM utenti", false, false);
			if (!$q['ok']) {
				$error = 500;
				$error_description = $config['errors'][$error] . ": database error";
				done();
				die;
			}
			$subs = round($q['result'][0]['count']);
		}
		$result = $subs;
		done();
		$redis->set('subs-' . time(), $subs);
		$redis->set('subs-' . time() + 1, $subs);
		$redis->set('subs-' . time() + 2, $subs);
	} elseif ($request == "/graph") {
		if (isset($query['id'])) {
			if (is_numeric($query['id'])) {
				$poll_id = $query['id'];
				if (isset($query['owner_id'])) {
					if (is_numeric($query['owner_id'])) {
						if ($query['owner_id'] == $userID or $islocal) {
							$creator = $query['owner_id'];
						} else {
							$error = 401;
							$error_description = $config['errors'][$error] . ": owner_id must be your";
							done();
							die;
						}
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": owner_id must be a numeric value";
						done();
						die;
					}
				} else {
					$creator = $query['owner_id'] = $userID;
				}
				$time = date("d-m-y-h-i");
				if (!$PDO) {
					try {
						$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
					} catch (PDOException $e) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database connection lost";
						done();
						die;
					}
				}
				$p = sendPoll($poll_id, $creator);
				if ($p['ok']) {
					$p = $p['result'];
				} else {
					$error = 400;
					$error_description = $config['errors'][$error] . ": database error";
					done();
				}
				if (!$p['status']) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": poll not found";
					done();
					die;
				} elseif (!$p['status'] or $p['status'] == "deleted") {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll deleted";
					done();
					die;
				} elseif (!in_array($p['type'], ['vote', 'limited doodle', 'doodle', 'rating', 'quiz'])) {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll type not supported";
					done();
					die;
				} elseif (!$config['types'][$p['type']]) {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll type in maintenance";
					done();
					die;
				} else {
					$ute = db_query("SELECT * FROM utenti WHERE user_id = ?", [$creator], true);
					if ($ute['ok']) {
						$ute = $ute['result'];
						$ute['settings'] = json_decode($ute['settings'], true);
					} else {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database error";
						done();
						die;
					}
					if ($query['theme'] == "sossio") {
						$theme = "chartMangiapollWatermark";
						$fontColor = "#DDDDCC";
					} elseif ($query['theme'] == "light") {
						$theme = "chartWatermark";
						$fontColor = "#444444";
					} else {
						$theme = "chartDarkWatermark";
						$fontColor = "#DDDDCC";
					}
					if (strlen($p['title']) <= 75) {
						$title = substr($p['title'], 0, 75);
					} else {
						$title = substr($p['title'], 0, 75) . "...";
					}
					foreach ($p['choice'] as $key => $users) {
						if (is_numeric($users)) {
							$users = round($users);
						} else {
							$users = 0;
						}
						if ($query['type'] == "bar") {
							$labels[] = [
								'label'		=> $key,
								'data'		=> [$users],
								'fill'		=> false
							];
						} elseif ($query['type'] == "pie") {
							$labels[] = $users;
							$nlabels[] = $key;
						}
					}
					if ($query['type'] == "bar") {
						$nlabels = [[]];
						$datasets = $labels;
					} elseif ($query['type'] == "pie") {
						$datasets = [['data' => $labels]];
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": type not found";
						done();
						die;
					}
					$taargs = [
						'type'		=> $query['type'],
						'data'		=> [
							'labels'	=> $nlabels,
							'datasets'	=> $datasets
						],
						'options'	=> [
							'title'		=> [
								'display'	=> true,
								'text'		=> $title,
								'fontColor'	=> $fontColor,
								'fontSize'	=> 32
							],
							'legend'	=> [
								'position' => "bottom"
							]
						]
					];
					$targs = [
						'c'			=> json_encode($taargs),
						'format'	=> "png",
						'width'		=> 1200,
						'height'	=> 800
					];
					$link = "https://quickchart.io/chart?" . http_build_query($targs);
					$filename = "graph_" . bot_encode("$poll_id-$creator") . ".png";
					copy($link, "/var/www/masterpoll-web/assets/chart/$filename");
					$ok = true;
					$final_img = imagecreatetruecolor(2000, 1600);
					if ($query['theme'] == "light") {
						$w = imagecolorallocatealpha($final_img, 0, 0, 0, 1);
					} else {
						$w = imagecolorallocatealpha($final_img, 255, 255, 255, 0);
					}
					imagecolortransparent($final_img, $w);
					$img_dark = imagecreatefrompng("/var/www/masterpoll-web/assets/$theme.png");
					imagealphablending($img_dark, true);
					imagesavealpha($img_dark, true);
					$graph_img = imagecreatefrompng("/var/www/masterpoll-web/assets/chart/$filename");
					imagealphablending($graph_img, true);
					imagesavealpha($graph_img, true);
					imagefill($graph_img, 2000, 1600, imagecolorallocatealpha($graph_img, 0, 0, 0, 127));
					imagecopy($final_img, $img_dark, 120, 110, 0, 0, 150, 150);
					imagecopy($final_img, $graph_img, 0, 0, 0, 0, 2000, 1600);
					imagepng($final_img, "/var/www/masterpoll-web/assets/chart/$filename");
					$args = ['file' => curl_file_create("/var/www/masterpoll-web/assets/chart/$filename", "image/jpeg")];
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "https://telegra.ph/upload");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
					$res = curl_exec($ch);
					curl_close($ch);
					$res = json_decode($res, true);
					if (isset($res[0]['src'])) {
						$ok = true;
						$link = "https://telegra.ph" . $res[0]['src'];
						$result = $link;
						done();
					} else {
						$error = 500;
						$error_description = $config['errors'][$error] . ": Telegraph not work at the moment.";
					}
					if (file_exists("/var/www/masterpoll-web/assets/chart/$filename")) {
						unlink("/var/www/masterpoll-web/assets/chart/$filename");
					}
				}
			} else {
				$error = 400;
				$error_description = $config['errors'][$error] . ": id must be a numeric value";
			}
		} else {
			$error = 400;
			$error_description = $config['errors'][$error] . ": id is empty";
		}
	} elseif ($request == "/getactivepolls" and !$islocal) {
		if (!$PDO) {
			try {
				$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
			} catch (PDOException $e) {
				$error = 500;
				$error_description = $config['errors'][$error] . ": database connection lost";
				done();
				die;
			}
		}
		$polls = getActivePoll($userID, 50);
		if ($polls['ok']) {
			$polls = $polls['result'];
		} else {
			$error = 500;
			done();
			die;
		}
		$result = [];
		foreach ($polls as $poll) {
			$p = doPoll($poll);
			if ($p['ok']) {
				$p = $p['result'];
			} else {
				$error = 500;
				done();
				die;
			}
			if ($config['types'][$p['type']]) $result[] = pollToArray($p);
		}
		$ok = true;
	} elseif ($request == "/getallpolls" and !$islocal) {
		if (!$PDO) {
			try {
				$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
			} catch (PDOException $e) {
				$error = 500;
				$error_description = $config['errors'][$error] . ": database connection lost";
				done();
				die;
			}
		}
		$polls = getAllPoll($userID, 50);
		if ($polls['ok']) {
			$polls = $polls['result'];
		} else {
			$error = 500;
			done();
			die;
		}
		$result = [];
		foreach ($polls as $poll) {
			$p = doPoll($poll);
			if ($p['ok']) {
				$p = $p['result'];
			} else {
				$error = 500;
				done();
				die;
			}
			if ($config['types'][$p['type']]) $result[] = pollToArray($p);
		}
		$ok = true;
	} elseif ($request == "/getme" and !$islocal) {
		if (!$PDO) {
			try {
				$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
			} catch (PDOException $e) {
				$error = 500;
				$error_description = $config['errors'][$error] . ": database connection lost";
				done();
				die;
			}
		}
		$u = db_query("SELECT * FROM utenti WHERE user_id = ?", [$userID], true);
		if ($u['ok']) {
			$u = $u['result'];
		} else {
			$error = 500;
			$error_description = $config['errors'][$error] . ": database error";
			done();
			die;
		}
		$u['settings'] = json_decode($u['settings'], true);
		unset($u['page']);
		$result = [
			'id' => $u['user_id'],
			'first_name' => $u['nome'],
			'last_name' => $u['cognome'],
			'username' => $u['username'],
			'language_code' => $u['lang']
		];
		$ok = true;
	} elseif ($request == "/getpoll" and !$islocal) {
		if (isset($query['id'])) {
			if (is_numeric($query['id'])) {
				if (isset($query['owner_id'])) {
					if (is_numeric($query['owner_id'])) {
						$creator = $query['owner_id'];
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": owner_id must be a numeric value";
						done();
						die;
					}
				} else {
					$creator = $userID;
				}
				if (!$PDO) {
					try {
						$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
					} catch (PDOException $e) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database connection lost";
						done();
						die;
					}
				}
				$poll = sendPoll($query['id'], $creator);
				if ($poll['ok']) {
					$poll = $poll['result'];
				} else {
					$error = 500;
					done();
					die;
				}
				if (!$poll['status']) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": poll not found";
				} elseif ($poll['status'] == "deleted") {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll deleted";
				} elseif (!$poll['settings']['sharable'] and $creator !== $userID and $userID !== 244432022) {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll data must be shareable";
				} elseif (!$config['types'][$poll['type']]) {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll type in maintenance";
				} else {
					$ok = true;
					$array = pollToArray($poll);
					if ($poll['type'] == "participation") {
						if (in_array($userID, $poll['choices']['participants'])) {
							$array['participating'] = true;
						} else {
							$array['participating'] = false;
						}
					}
					$result = $array;
				}
			} else {
				$error = 400;
				$error_description = $config['errors'][$error] . ": id must be a numeric value";
			}
		} else {
			$error = 400;
			$error_description = $config['errors'][$error] . ": id is empty";
		}
	} elseif ($request == "/deletepoll" and !$islocal) {
		if (isset($query['id'])) {
			if (is_numeric($query['id'])) {
				if (isset($query['owner_id'])) {
					if (is_numeric($query['owner_id'])) {
						if ($query['owner_id'] == $userID or $islocal) {
							$creator = $query['owner_id'];
						} else {
							$error = 401;
							$error_description = $config['errors'][$error] . ": owner_id must be your";
							done();
							die;
						}
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": owner_id must be a numeric value";
						done();
						die;
					}
				} else {
					$creator = $query['owner_id'] = $userID;
				}
				if (!$PDO) {
					try {
						$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
					} catch (PDOException $e) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database connection lost";
						done();
						die;
					}
				}
				$poll = sendPoll($query['id'], $query['owner_id']);
				if ($poll['ok']) {
					$poll = $poll['result'];
				} else {
					$error = 500;
					done();
					die;
				}
				if (!$poll['status']) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": poll not found";
				} elseif ($poll['status'] == "deleted") {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll already deleted";
				} elseif (!$config['types'][$poll['type']]) {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll type in maintenance";
				} else {
					$ok = true;
					$result = true;
					cancelPoll($query['id'], $userID);
				}
			} else {
				$error = 400;
				$error_description = $config['errors'][$error] . ": id must be a numeric value";
			}
		} else {
			$error = 400;
			$error_description = $config['errors'][$error] . ": id is empty";
		}
	} elseif ($request == "/closepoll" and !$islocal) {
		if (isset($query['id'])) {
			if (is_numeric($query['id'])) {
				if (isset($query['owner_id'])) {
					if (is_numeric($query['owner_id'])) {
						if ($query['owner_id'] == $userID or $islocal) {
							$creator = $query['owner_id'];
						} else {
							$error = 401;
							$error_description = $config['errors'][$error] . ": owner_id must be your";
							done();
							die;
						}
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": owner_id must be a numeric value";
						done();
						die;
					}
				} else {
					$creator = $query['owner_id'] = $userID;
				}
				if (!$PDO) {
					try {
						$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
					} catch (PDOException $e) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database connection lost";
						done();
						die;
					}
				}
				$p = sendPoll($query['id'], $query['owner_id']);
				if ($p['ok']) {
					$p = $p['result'];
				} else {
					$error = 500;
					done();
					die;
				}
				if (!$p['status']) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": poll not found";
				} elseif ($p['status'] == "deleted") {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll deleted";
				} elseif (!$config['types'][$p['type']]) {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll type in maintenance";
				} elseif ($p['status'] == 'open') {
					db_query("UPDATE polls SET status = ? WHERE user_id = ? and poll_id = ?", ['closed', $query['owner_id'], $query['id']], 'no');
					$ok = true;
					$result = true;
				} else {
					$error = 400;
					$error_description = $config['errors'][$error] . ": the poll is closed";
				}
			} else {
				$error = 400;
				$error_description = $config['errors'][$error] . ": id must be a numeric value";
			}
		} else {
			$error = 400;
			$error_description = $config['errors'][$error] . ": id is empty";
		}
	} elseif ($request == "/openpoll" and !$islocal) {
		if (isset($query['id'])) {
			if (is_numeric($query['id'])) {
				if (isset($query['owner_id'])) {
					if (is_numeric($query['owner_id'])) {
						if ($query['owner_id'] == $userID or $islocal) {
							$creator = $query['owner_id'];
						} else {
							$error = 401;
							$error_description = $config['errors'][$error] . ": owner_id must be your";
							done();
							die;
						}
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": owner_id must be a numeric value";
						done();
						die;
					}
				} else {
					$creator = $query['owner_id'] = $userID;
				}
				if (!$PDO) {
					try {
						$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
					} catch (PDOException $e) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database connection lost";
						done();
						die;
					}
				}
				$p = sendPoll($query['id'], $query['owner_id']);
				if ($p['ok']) {
					$p = $p['result'];
				} else {
					$error = 500;
					done();
					die;
				}
				if (!$p['status']) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": poll not found";
				} elseif ($p['status'] == "deleted") {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll deleted";
				} elseif (!$config['types'][$p['type']]) {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll type in maintenance";
				} elseif ($p['status'] == 'closed') {
					db_query("UPDATE polls SET status = ? WHERE user_id = ? and poll_id = ?", ['open', $query['owner_id'], $query['id']], 'no');
					$ok = true;
					$result = true;
				} else {
					$error = 400;
					$error_description = $config['errors'][$error] . ": the poll is open";
				}
			} else {
				$error = 400;
				$error_description = $config['errors'][$error] . ": id must be a numeric value";
			}
		} else {
			$error = 400;
			$error_description = $config['errors'][$error] . ": id is empty";
		}
	} elseif ($request == "/editpoll" and !$islocal) {
		if (isset($query['id'])) {
			if (is_numeric($query['id'])) {
				if (isset($query['owner_id'])) {
					if (is_numeric($query['owner_id'])) {
						if ($query['owner_id'] == $userID or $islocal) {
							$creator = $query['owner_id'];
						} else {
							$error = 401;
							$error_description = $config['errors'][$error] . ": owner_id must be your";
							done();
							die;
						}
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": owner_id must be a numeric value";
						done();
						die;
					}
				} else {
					$creator = $query['owner_id'] = $userID;
				}
				if (!$PDO) {
					try {
						$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
					} catch (PDOException $e) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database connection lost";
						done();
						die;
					}
				}
				$poll = sendPoll($query['id'], $query['owner_id']);
				if ($poll['ok']) {
					$poll = $poll['result'];
				} else {
					$error = 500;
					done();
					die;
				}
				if (!empty($query['settings'])) {
					if (!is_array($query['settings'])) {
						$query['settings'] = json_decode($query['settings'], true);
					}
					if (!empty($query['settings'])) {
						if (is_array($query['settings'])) {
							$stringsetting = [
								"disable_web_page_preview",
								"disable_web_content_preview",
								"sort",
								"appendable",
								"notification",
								"web_content_preview",
								"votes_limit"
							];
							$newsettings = [];
							foreach (array_keys($query['settings']) as $string) {
								if (in_array($string, array_keys($stringsetting))) {
									if ($string == "disable_web_page_preview") {
										if (!is_bool($query['settings'][$string])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
											done();
											die;
										}
										if ($query['settings']['disable_web_page_preview']) {
											$poll['settings']['web_page'] = false;
											$newsettings['disable_web_page_preview'] = true;
										} else {
											unset($poll['settings']['web_content']);
											unset($poll['settings']['web_page']);
											$newsettings['disable_web_page_preview'] = true;
											if ($poll['settings']['web_page'] or $newsettings['disable_web_content_preview']) $newsettings['disable_web_content_preview'] = false;
										}
									} elseif ($string == "disable_web_content_preview") {
										if (!is_bool($query['settings'][$string])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
											done();
											die;
										}
										if ($query['settings']['disable_web_content_preview']) {
											if (!$poll['settings']['web_page']) $newsettings['disable_web_page_preview'] = true;
											$newsettings['disable_web_content_preview'] = $query['settings']['disable_web_content_preview'];
											$poll['settings']['web_content'] = true;
										} else {
											$error = 400;
											$error_description = "In order to disable the preview, edit the poll and set disable_web_content_preview to true";
											done();
											die;
										}
									} elseif ($string == "web_content_preview") {
										if (!is_string($query['settings'][$string])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type string";
											done();
											die;
										}
										if (!empty($query['settings']['web_content_preview'])) {
											if (!$poll['settings']['web_page']) $newsettings['disable_web_page_preview'] = false;
											$newsettings['disable_web_content_preview'] = $query['settings']['disable_web_content_preview'];
											$newsettings['web_content_preview'] = $query['settings'][$string];
											$poll['settings']['web_content'] = $query['settings'][$string];
										} else {
											$error = 400;
											$error_description = $config['errors'][$error] . "cannot be an empty string";
											done();
											die;
										}
									} elseif ($string == "sort") {
										if (!is_bool($query['settings'][$string])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
											done();
											die;
										}
										if (!in_array($poll['type'], ['vote', 'doodle', 'limited doodle'])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field $string isn't expected here";
											done();
											die;
										} elseif ($query['settings']['sort']) {
											$poll['settings']['sort'] = true;
											$newsettings['sort'] = true;
										} else {
											unset($poll['settings']['sort']);
											$newsettings['sort'] = false;
										}
									} elseif ($string == "appendable") {
										if (!is_bool($query['settings'][$string])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
											done();
											die;
										}
										if (!in_array($poll['type'], ['vote', 'doodle', 'limited doodle'])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field $string isn't expected here";
											done();
											die;
										} elseif ($query['settings']['appendable']) {
											$poll['settings']['appendable'] = true;
											$newsettings['appendable'] = true;
										} else {
											unset($poll['settings']['appendable']);
											$newsettings['appendable'] = false;
										}
									} elseif ($string == "notification") {
										if (!is_bool($query['settings'][$string])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
											done();
											die;
										}
										if (!in_array($poll['type'], ['vote', 'doodle', 'limited doodle', 'board'])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field $string isn't expected here";
											done();
											die;
										} elseif ($query['settings']['notification']) {
											$poll['settings']['notification'] = true;
											$newsettings['notification'] = true;
										} else {
											unset($poll['settings']['notification']);
											$newsettings['notification'] = false;
										}
									} elseif ($string == "votes_limit") {
										if (isset($query['settings']['votes_limit'])) {
											if (!is_numeric($query['settings']['votes_limit'])) {
												$error = 400;
												$error_description = $config['errors'][$error] . ": votes_limit must be a numeric value";
												done();
												die;
											}
											if ($query['settings']['votes_limit'] > 0) {
												$error = 400;
												$error_description = $config['errors'][$error] . ": Illegal integer on $settings";
												done();
												die;
											}
											if ($query['settings']['votes_limit'] === 0) {
												unset($poll['settings']['max_voters']);
												$newsettings['votes_limit'] = false;
											} elseif ($query['settings']['votes_limit'] < $poll['votes']) {
												$error = 400;
												$error_description = $config['errors'][$error] . ": Illegal integer on $settings";
												done();
												die;
											} else {
												$poll['settings']['max_voters'] = round($query['settings']['votes_limit']);
												$newsettings['votes_limit'] = round($query['settings']['votes_limit']);
											}
										} else {
											unset($poll['settings']['max_voters']);
											$newsettings['votes_limit'] = false;
										}
									}
								} else {
									$error = 400;
									$error_description = $config['errors'][$error] . ": field $string isn't expected in settings";
									done();
									die;
								}
							}
							db_query("UPDATE polls SET settings = ? WHERE user_id = ? and poll_id = ?", [json_encode($poll['settings']), $query['owner_id'], $query['id']], false);
							$y = true;
						} else {
							$ok = false;
							$error = 400;
							$error_description = $config['errors'][$error] . ": settings must be an array";
							done();
							die;
						}
					} else {
						$ok = false;
						$error = 400;
						$error_description = $config['errors'][$error] . ": settings are empty";
						done();
						die;
					}
				}
				if (isset($query['privacy'])) {
					if (is_string($query['privacy'])) {
						if (strlen($query['privacy']) <= 32) {
							if (in_array($query['privacy'], array_keys($config['poll_privacy']))) {
								if ($query['privacy'] == "anonymous") {
									if (!$poll['anonymous']) {
										$privacy = true;
										db_query("UPDATE polls SET anonymous = ? WHERE poll_id = ? and user_id = ?", [1, $owner['id'], $query['owner_id']], 'no');
									} else {
										$ok = false;
										$error = 400;
										$error_description = $config['errors'][$error] . ": poll already anonymous";
										done();
										die;
									}
								} else {
									$ok = false;
									$error = 400;
									$error_description = $config['errors'][$error] . ": poll is already public 😐";
									done();
									die;
								}
							} else {
								$ok = false;
								$error = 400;
								$error_description = $config['errors'][$error] . ": invalid value in the privacy field. Only personal or anonymous are accepted";
								done();
								die;
							}
						} else {
							$ok = false;
							$error = 400;
							$error_description = $config['errors'][$error] . ": privacy is too long";
							done();
							die;
						}
					} else {
						$ok = false;
						$error = 400;
						$error_description = $config['errors'][$error] . ": privacy must be a string";
						done();
						die;
					}
					$y = true;
				}
				if (!isset($y)) {
					$ok = false;
					$error = 400;
					$error_description = $config['errors'][$error];
					done();
					die;
				} else {
					$ok = true;
					$result = [];
					if (isset($newsettings)) $result['settings'] = $newsettings;
					if (isset($privacy)) $result['privacy'] = 'anonymous';
					done();
				}
			} else {
				$ok = false;
				$error = 400;
				$error_description = $config['errors'][$error] . ": id must be a numeric value";
			}
		} else {
			$ok = false;
			$error = 400;
			$error_description = $config['errors'][$error] . ": id is empty";
		}
	} elseif ($request == "/addvote" and !$islocal) {
		if (isset($query['id'])) {
			if (is_numeric($query['id'])) {
				if (isset($query['owner_id'])) {
					if (is_numeric($query['owner_id'])) {
						if ($query['owner_id'] == $userID or $islocal) {
							$creator = $query['owner_id'];
						} else {
							$error = 401;
							$error_description = $config['errors'][$error] . ": owner_id must be your";
							done();
							die;
						}
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": owner_id must be a numeric value";
						done();
						die;
					}
				} else {
					$creator = $query['owner_id'] = $userID;
				}
				if (!$PDO) {
					try {
						$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
					} catch (PDOException $e) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database connection lost";
						done();
						die;
					}
				}
				$p = sendPoll($query['id'], $creator);
				if ($p['ok']) {
					$p = $p['result'];
				} else {
					$error = 500;
					done();
					die;
				}
				if (!$p['status']) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": poll not found";
					done();
					die;
				} elseif ($p['status'] == "closed") {
					$error = 401;
					$error_description = $config['errors'][$error] . ": poll closed";
					done();
					die;
				} elseif ($p['status'] == "deleted") {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll deleted";
					done();
					die;
				} elseif (!$config['types'][$p['type']]) {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll type in maintenance";
					done();
					die;
				}
				if (!isAdmin($userID, $p)['result']) {
					$banlist = db_query("SELECT banlist FROM utenti WHERE user_id = ?", [$creator], true);
					if ($banlist['ok']) {
						$banlist = $banlist['result'];
					} else {
						$error = 500;
						$error_description = $config['errors'][$error] . ": error to get the banlist";
						done();
						die;
					}
					$banlist = json_decode($banlist['banlist'], true);
					if (isset($banlist)) {
						if (isset($banlist[$userID])) {
							if (is_numeric($banlist[$userID])) {
								if ($banlist[$userID] <= time()) {
									unset($banlist[$userID]);
									db_query("UPDATE utenti SET banlist = ? WHERE user_id = ?", [json_encode($banlist), $creator], 'no');
								} else {
									$error = 401;
									$error_description = $config['errors'][$error] . ": user_id banned from owner_id";
									done();
									die;
								}
							} else {
								$error = 401;
								$error_description = $config['errors'][$error] . ": user_id banned from owner_id";
								done();
								die;
							}
						}
					}
				} else {
					$admin = true;
				}
				if (isset($p['settings']['max_voters'])) {
					if ($p['votes'] == $p['settings']['max_voters']) {
						$haveChoices = haveChoices($p, $userID);
						if ($haveChoices['ok']) {
							$haveChoices = $haveChoices['result'];
						} else {
							$error = 500;
							$error_description = $config['errors'][$error] . ": database error";
							done();
							die;
						}
						if ($haveChoices) {} else {
							$error = 400;
							$error_description = $config['errors'][$error] . ": poll is full";
							done();
							die;
						}
					}
				}
				if ($p['type'] == "vote") {
					if (isset($query['choice']) and !empty($query['choice'])) {
						$r = haveChoices($p, $userID);
						if ($r['ok']) {
							if (!empty($r['result'])) {
								$error = 400;
								$error_description = $config['errors'][$error] . ": you're already in this vote";
								done();
								die;
							}
							$r = $r['result'];
						} else {
							$error = 500;
							$error_description = $config['errors'][$error] . ": database error";
							done();
						}
						if (!in_array($query['choice'], $r)) {
							$q1 = removeAllChoices($p, $userID);
							if (!$q1['ok']) {
								$error = 500;
								$error_description = $config['errors'][$error] . ": database error";
								done();
							}
							if (!in_array($query['choice'], array_keys($p['choices']))) {
								$error = 400;
								$error_description = $config['errors'][$error] . ": choice not found";
								done();
							}
							$q1 = addChoice(["poll_id" => $p['poll_id'], "creator" => $p['creator']], $userID, $query['choice']);
							if (!$q1['ok']) {
								$error = 500;
								$error_description = $config['errors'][$error] . ": database error";
								done();
							}
							$ok = true;
						} else {
							$q1 = removeAllChoices($p, $userID);
							if (!$q1['ok']) {
								$error = 500;
								$error_description = $config['errors'][$error] . ": database error";
								done();
							}
							$ok = true;
							$result = false;
						}
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": choice is empty";
						done();
					}
				} elseif ($p['type'] == "doodle") {
					if (isset($query['choice']) and !empty($query['choice'])) {
						$r = haveChoice($p, $userID, $query['choice']);
						if ($r['ok']) {
							$r = $r['result'];
						} else {
							$error = $r['error_code'];
							$error_description = $r['description'];
							done();
						}
						if ($r !== $query['choice']) {
							$q1 = addChoice($p, $userID, $query['choice']);
							if (!$q1['ok']) {
								$error = 500;
								$error_description = $config['errors'][$error] . ": database error";
								done();
							}
							$ok = true;
						} else {
							$error = 400;
							$error_description = $config['errors'][$error] . ": you're already voted this option";
							done();
							die;
						}
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": choice is empty";
						done();
					}
				} elseif ($p['type'] == "limited doodle") {
					if (isset($query['choice']) and !empty($query['choice'])) {
						$r = haveChoice($p, $userID, $query['choice']);
						if ($r['ok']) {
							$r = $r['result'];
						} else {
							$error = $r['error_code'];
							$error_description = $r['description'];
							done();
						}
						$max = $p['settings']['max_choices'];
						if ($r !== $query['choice']) {
							$userTotalChoices = userTotalChoices($p, $userID);
							if ($userTotalChoices['ok']) {
								$userTotalChoices = $userTotalChoices['result'];
							} else {
								$error = 500;
								$error_description = $config['errors'][$error] . ": database error";
								done();
							}
							if ($userTotalChoices < $max) {
								$q1 = addChoice($p, $userID, $query['choice']);
								if (!$q1['ok']) {
									$error = 500;
									$error_description = $config['errors'][$error] . ": database error";
									done();
								}
								$ok = true;
							} else {
								$error = 400;
								$error_description = $config['errors'][$error] . ": you can choose only $max choices";
								done();
							}
						} else {
							$error = 400;
							$error_description = $config['errors'][$error] . ": you're already voted this option";
							done();
							die;
						}
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": choice is empty";
						done();
					}
				} elseif ($p['type'] == "board") {
					if (isset($query['comment']) and !empty($query['comment'])) {
						if (is_string($query['comment'])) {
							if (strlen($query['comment']) <= 128) {
								$r = haveChoices($p, $userID);
								if ($r['ok']) {
									if (!empty($r['result'])) {
										$error = 400;
										$error_description = $config['errors'][$error] . ": you're already in this board";
										done();
										die;
									}
									$r = $r['result'];
								} else {
									$error = 500;
									$error_description = $config['errors'][$error] . ": database error";
									done();
								}
								$q1 = addChoiceBoard($p, $userID, $query['comment']);
								if (!$q1['ok']) {
									$error = $r['error_code'];
									$error_description = $r['description'];
									done();
								}
								$ok = true;
							} else {
								$error = 400;
								$error_description = $config['errors'][$error] . ": comment must be less than 128 characters";
								done();
							}
						} else {
							$error = 400;
							$error_description = $config['errors'][$error] . ": comment must be a string";
							done();
						}
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": comment is empty";
						done();
					}
				} elseif ($p['type'] == "quiz") {
					if (isset($query['choice']) and !empty($query['choice'])) {
						if (!in_array($query['choice'], array_keys($p['choices']))) {
							$error = 400;
							$error_description = $config['errors'][$error] . ": choice not found";
							done();
						}
						$r = haveChoices($p, $userID);
						if ($r['ok']) {
							if (!empty($r['result'])) {
								$error = 400;
								$error_description = $config['errors'][$error] . ": you're already on this quiz";
								done();
								die;
							}
							$r = $r['result'];
						} else {
							$error = 500;
							$error_description = $config['errors'][$error] . ": database error";
							done();
						}
						if (count($p['settings']['right_answers']) > 1) {
							$error = 400;
							$error_description = $config['errors'][$error] . ": poll type not allowed";
							done();
							die;
						}
						$q1 = removeAllChoices($p, $userID);
						if (!$q1['ok']) {
							$error = 500;
							$error_description = $config['errors'][$error] . ": database error";
							done();
						}
						$q1 = addChoice(["poll_id" => $p['poll_id'], "creator" => $p['creator']], $userID, $query['choice']);
						if (!$q1['ok']) {
							$error = 500;
							$error_description = $config['errors'][$error] . ": database error";
							done();
						}
						$ok = true;
						$result = true;
					}
				} elseif ($p['type'] == "participation") {
					$r = haveChoices($p, $userID);
					if ($r['ok']) {
						if (!empty($r['result'])) {
							$error = 400;
							$error_description = $config['errors'][$error] . ": you're already in this participation";
							done();
							die;
						}
						$r = $r['result'];
					} else {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database error";
						done();
					}
					if (!in_array("participate", $r)) {
						$q1 = removeAllChoices($p, $userID);
						if (!$q1['ok']) {
							$error = 500;
							$error_description = $config['errors'][$error] . ": database error";
							done();
							die;
						}
						$q1 = addChoice(["poll_id" => $p['poll_id'], "creator" => $p['creator']], $userID, 'participate');
						if (!$q1['ok']) {
							$error = 500;
							$error_description = $config['errors'][$error] . ": database error";
							done();
							die;
						}
						$ok = true;
						$result = true;
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": you're already in this participation";
						done();
						die;
					}
				} elseif ($p['type'] == "rating") {
					if (isset($query['rate'])) {
						if (is_numeric($query['rate'])) {
							if (in_array($query['rate'], array_keys($p['choices']))) {
								$r = haveChoices($p, $userID);
								if ($r['ok']) {
									if (!empty($r['result'])) {
										$error = 400;
										$error_description = $config['errors'][$error] . ": you're already in this rating";
										done();
										die;
									}
									$r = $r['result'];
								} else {
									$error = 500;
									$error_description = $config['errors'][$error] . ": database error";
									done();
								}
								if (!in_array($query['rate'], $r)) {
									$error = 400;
									$error_description = $config['errors'][$error] . ": you're already in this rating";
									done();
									die;
								}
								$q1 = addChoice(["poll_id" => $p['poll_id'], "creator" => $p['creator']], $userID, $query['rate']);
								if (!$q1['ok']) {
									$error = 500;
									$error_description = $config['errors'][$error] . ": database error";
									done();
								}
								$ok = true;
								$result = true;
							} else {
								$error = 400;
								$error_description = $config['errors'][$error] . ": rate must be an existing value";
								done();
							}
						} else {
							$error = 400;
							$error_description = $config['errors'][$error] . ": rate must be a numeric value";
							done();
						}
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . "rate is empty";
						done();
					}
				}
				done();
				fastcgi_finish_request();
			} else {
				$ok = false;
				$error = 400;
				$error_description = $config['errors'][$error] . ": id must be a numeric value";
			}
		} else {
			$ok = false;
			$error = 400;
			$error_description = $config['errors'][$error] . ": id is empty";
		}
	} elseif ($request == "/editvote" and !$islocal) {
		if (isset($query['id'])) {
			if (is_numeric($query['id'])) {
				if (isset($query['owner_id'])) {
					if (is_numeric($query['owner_id'])) {
						if ($query['owner_id'] == $userID or $islocal) {
							$creator = $query['owner_id'];
						} else {
							$error = 401;
							$error_description = $config['errors'][$error] . ": owner_id must be your";
							done();
							die;
						}
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": owner_id must be a numeric value";
						done();
						die;
					}
				} else {
					$creator = $query['owner_id'] = $userID;
				}
				if (!$PDO) {
					try {
						$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
					} catch (PDOException $e) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database connection lost";
						done();
						die;
					}
				}
				$p = sendPoll($query['id'], $creator);
				if ($p['ok']) {
					$p = $p['result'];
				} else {
					$error = 500;
					done();
					die;
				}
				if (!$p['status']) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": poll not found";
					done();
					die;
				} elseif ($p['status'] == "closed") {
					$error = 401;
					$error_description = $config['errors'][$error] . ": poll closed";
					done();
					die;
				} elseif ($p['status'] == "deleted") {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll deleted";
					done();
					die;
				} elseif (!$config['types'][$p['type']]) {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll type in maintenance";
					done();
					die;
				}
				if (!isAdmin($userID, $p)['result']) {
					$banlist = db_query("SELECT banlist FROM utenti WHERE user_id = ?", [$creator], true);
					if ($banlist['ok']) {
						$banlist = $banlist['result'];
					} else {
						$error = 500;
						$error_description = $config['errors'][$error] . ": error to get the banlist";
						done();
						die;
					}
					$banlist = json_decode($banlist['banlist'], true);
					if (isset($banlist)) {
						if (isset($banlist[$userID])) {
							if (is_numeric($banlist[$userID])) {
								if ($banlist[$userID] <= time()) {
									unset($banlist[$userID]);
									db_query("UPDATE utenti SET banlist = ? WHERE user_id = ?", [json_encode($banlist), $creator], 'no');
								} else {
									$error = 401;
									$error_description = $config['errors'][$error] . ": user_id banned from owner_id";
									done();
									die;
								}
							} else {
								$error = 401;
								$error_description = $config['errors'][$error] . ": user_id banned from owner_id";
								done();
								die;
							}
						}
					}
				} else {
					$admin = true;
				}
				if ($p['type'] == "vote") {
					if (isset($query['choice'])) {
						if (!in_array($query['choice'], array_keys($p['choices']))) {
							$error = 400;
							$error_description = $config['errors'][$error] . ": choice not found";
							done();
						}
						$r = haveChoices($p, $userID);
						if ($r['ok']) {
							$r = $r['result'];
							if (empty($r['result'])) {
								$error = 400;
								$error_description = $config['errors'][$error] . ": you haven't voted in this poll yet";
								done();
								die;
							}
						} else {
							$error = 500;
							$error_description = $config['errors'][$error] . ": database error";
							done();
						}
						$q1 = removeAllChoices($p, $userID);
						if (!$q1['ok']) {
							$error = 500;
							$error_description = $config['errors'][$error] . ": database error";
							done();
						}
						$q1 = addChoice(["poll_id" => $p['poll_id'], "creator" => $p['creator']], $userID, $query['choice']);
						if (!$q1['ok']) {
							$error = 500;
							$error_description = $config['errors'][$error] . ": database error";
							done();
						}
						$ok = true;
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": choice is empty";
						done();
					}
				} elseif ($p['type'] == "quiz") {
					if (isset($query['choice'])) {
						if (!in_array($query['choice'], array_keys($p['choices']))) {
							$error = 400;
							$error_description = $config['errors'][$error] . ": choice not found";
							done();
						}
						$r = haveChoices($p, $userID);
						if ($r['ok']) {
							if (empty($r['result'])) {
								$error = 400;
								$error_description = $config['errors'][$error] . ": you haven't voted in this poll yet";
								done();
								die;
							}
							$r = $r['result'];
						} else {
							$error = 500;
							$error_description = $config['errors'][$error] . ": database error";
							done();
						}
						if (count($p['settings']['right_answers']) > 1) {
							$error = 400;
							$error_description = $config['errors'][$error] . ": poll type not allowed";
							done();
							die;
						}
						$q1 = removeAllChoices($p, $userID);
						if (!$q1['ok']) {
							$error = 500;
							$error_description = $config['errors'][$error] . ": database error";
							done();
						}
						$q1 = addChoice(["poll_id" => $p['poll_id'], "creator" => $p['creator']], $userID, $query['choice']);
						if (!$q1['ok']) {
							$error = 500;
							$error_description = $config['errors'][$error] . ": database error";
							done();
						}
						$ok = true;
						$result = true;
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": choice is empty";
						done();
					}
				} elseif ($p['type'] == "participation") {
					if (isset($query['participate']) and !empty($query['participate'])) {
						if (!is_bool($query['participate'])) {
							$ok = false;
							$error = 400;
							$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
							done();
							die;
						}
						if ($query['participate']) {
							$r = haveChoices($p, $userID);
							if ($r['ok']) {
								if (!empty($r['result'])) {
									$error = 400;
									$error_description = $config['errors'][$error] . ": you're already in this participation";
									done();
									die;
								}
								$r = $r['result'];
							} else {
								$error = 500;
								$error_description = $config['errors'][$error] . ": database error";
								done();
							}
							if (!in_array("participate", $r)) {
								$q1 = removeAllChoices($p, $userID);
								if (!$q1['ok']) {
									$error = 500;
									$error_description = $config['errors'][$error] . ": database error";
									done();
									die;
								}
								$q1 = addChoice(["poll_id" => $p['poll_id'], "creator" => $p['creator']], $userID, 'participate');
								if (!$q1['ok']) {
									$error = 500;
									$error_description = $config['errors'][$error] . ": database error";
									done();
									die;
								}
								$ok = true;
								$result = true;
							} else {
								$error = 400;
								$error_description = $config['errors'][$error] . ": you're already in this participation";
								done();
								die;
							}
						} else {
							$r = haveChoices($p, $userID);
							if ($r['ok']) {
								$r = $r['result'];
								if (empty($r['result'])) {
									$error = 400;
									$error_description = $config['errors'][$error] . ": you haven't voted in this poll yet";
									done();
									die;
								}
							} else {
								$error = 500;
								$error_description = $config['errors'][$error] . ": database error";
								done();
							}
							$q1 = removeAllChoices($p, $userID);
							if (!$q1['ok']) {
								$error = 500;
								$error_description = $config['errors'][$error] . ": database error";
								done();
								die;
							}
						}
						$ok = true;
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": participate is empty";
						done();
					}
				} elseif ($p['type'] == "board") {
					if (isset($query['comment']) and $query['comment']) {
						if (is_string($query['comment'])) {
							if (strlen($query['comment']) <= 128) {
								$r = haveChoices($p, $userID);
								if ($r['ok']) {
									if (empty($r['result'])) {
										$error = 400;
										$error_description = $config['errors'][$error] . ": you haven't voted in this poll yet";
										done();
										die;
									}
									$r = $r['result'];
								} else {
									$error = 500;
									$error_description = $config['errors'][$error] . ": database error";
									done();
								}
								$q1 = addChoiceBoard($p, $userID, $query['comment']);
								if (!$q1['ok']) {
									$error = $r['error_code'];
									$error_description = $r['description'];
									done();
								}
								$ok = true;
							} else {
								$error = 400;
								$error_description = $config['errors'][$error] . ": comment must be less than 128 characters";
								done();
							}
						} else {
							$error = 400;
							$error_description = $config['errors'][$error] . ": comment must be a string";
							done();
						}
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": comment is empty";
						done();
					}
				} elseif ($p['type'] == "rating") {
					if (isset($query['rate'])) {
						if (is_numeric($query['rate'])) {
							if (in_array($query['rate'], array_keys($p['choices']))) {
								$r = haveChoices($p, $userID);
								if ($r['ok']) {
									if (empty($r['result'])) {
										$error = 400;
										$error_description = $config['errors'][$error] . ": you haven't voted in this poll yet";
										done();
										die;
									}
									$r = $r['result'];
								} else {
									$error = 500;
									$error_description = $config['errors'][$error] . ": database error";
									done();
								}
								$q1 = addChoice(["poll_id" => $p['poll_id'], "creator" => $p['creator']], $userID, $query['rate']);
								if (!$q1['ok']) {
									$error = 500;
									$error_description = $config['errors'][$error] . ": database error";
									done();
								}
								$ok = true;
							} else {
								$error = 400;
								$error_description = $config['errors'][$error] . ": rate must be an existing value";
								done();
							}
						} else {
							$error = 400;
							$error_description = $config['errors'][$error] . ": rate must be a numeric value";
							done();
						}
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . "rate is empty";
						done();
					}
				} else {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll type not allowed";
				}
				done();
				fastcgi_finish_request();
			} else {
				$ok = false;
				$error = 400;
				$error_description = $config['errors'][$error] . ": id must be a numeric value";
			}
		} else {
			$ok = false;
			$error = 400;
			$error_description = $config['errors'][$error] . ": id is empty";
		}
	} elseif ($request == "/removevote" and !$islocal) {
		if (isset($query['id'])) {
			if (is_numeric($query['id'])) {
				if (isset($query['owner_id'])) {
					if (is_numeric($query['owner_id'])) {
						if ($query['owner_id'] == $userID or $islocal) {
							$creator = $query['owner_id'];
						} else {
							$error = 401;
							$error_description = $config['errors'][$error] . ": owner_id must be your";
							done();
							die;
						}
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": owner_id must be a numeric value";
						done();
						die;
					}
				} else {
					$creator = $query['owner_id'] = $userID;
				}
				if ($query['owner_id'] == $userID or $islocal) {
					$creator = $query['owner_id'];
				} else {
					$error = 401;
					$error_description = $config['errors'][$error] . ": owner_id must be your";
					done();
					die;
				}
				if (!$PDO) {
					try {
						$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
					} catch (PDOException $e) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database connection lost";
						done();
						die;
					}
				}
				$p = sendPoll($query['id'], $creator);
				if ($p['ok']) {
					$p = $p['result'];
				} else {
					$error = 500;
					done();
					die;
				}
				if (!$p['status']) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": poll not found";
					done();
					die;
				} elseif ($p['status'] == "closed") {
					$error = 401;
					$error_description = $config['errors'][$error] . ": poll closed";
					done();
					die;
				} elseif ($p['status'] == "deleted") {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll deleted";
					done();
					die;
				} elseif (!$config['types'][$p['type']]) {
					$error = 400;
					$error_description = $config['errors'][$error] . ": poll type in maintenance";
					done();
					die;
				}
				if (!isAdmin($userID, $p)['result']) {
					$banlist = db_query("SELECT banlist FROM utenti WHERE user_id = ?", [$creator], true);
					if ($banlist['ok']) {
						$banlist = $banlist['result'];
					} else {
						$error = 500;
						$error_description = $config['errors'][$error] . ": error to get the banlist";
						done();
						die;
					}
					$banlist = json_decode($banlist['banlist'], true);
					if (isset($banlist)) {
						if (isset($banlist[$userID])) {
							if (is_numeric($banlist[$userID])) {
								if ($banlist[$userID] <= time()) {
									unset($banlist[$userID]);
									db_query("UPDATE utenti SET banlist = ? WHERE user_id = ?", [json_encode($banlist), $creator], 'no');
								} else {
									$error = 401;
									$error_description = $config['errors'][$error] . ": user_id banned from owner_id";
									done();
									die;
								}
							} else {
								$error = 401;
								$error_description = $config['errors'][$error] . ": user_id banned from owner_id";
								done();
								die;
							}
						}
					}
				} else {
					$admin = true;
				}
				if ($p['type'] == "vote") {
					$q1 = removeAllChoices($p, $userID);
					if (!$q1['ok']) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database error";
						done();
					}
					$ok = true;
				} elseif ($p['type'] == "doodle") {
					if (isset($query['choice'])) {
						if (isset($query['choice'])) {
							$q1 = removeChoice($p, $userID, $query['choice']);
							if (!$q1['ok']) {
								$error = 500;
								$error_description = $config['errors'][$error] . ": database error";
								done();
							}
							$ok = true;
						} else {
							
						}
						$q1 = removeChoice($p, $userID, $query['choice']);
						if (!$q1['ok']) {
							$error = 500;
							$error_description = $config['errors'][$error] . ": database error";
							done();
						}
						$ok = true;
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": choice is empty";
						done();
					}
				} elseif ($p['type'] == "limited doodle") {
					if (isset($query['choice'])) {
						$q1 = removeChoice($p, $userID, $query['choice']);
						if (!$q1['ok']) {
							$error = 500;
							$error_description = $config['errors'][$error] . ": database error";
							done();
						}
						$ok = true;
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": choice is empty";
						done();
					}
				} elseif ($p['type'] == "board") {
					$q1 = removeChoicePoll($p, $userID);
					if (!$q1['ok']) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database error";
						done();
					}
					$ok = true;
				} elseif ($p['type'] == "quiz") {
					if (isset($query['choice'])) {
						$q1 = removeChoice($p, $userID, $query['choice']);
						if (!$q1['ok']) {
							$error = 500;
							$error_description = $config['errors'][$error] . ": database error";
							done();
						}
						$ok = true;
					} else {
						$error = 400;
						$error_description = $config['errors'][$error] . ": choice is empty";
						done();
					}
				} elseif ($p['type'] == "participation") {
					$q1 = removeAllChoices($p, $userID);
					if (!$q1['ok']) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database error";
						done();
						die;
					}
					$ok = true;
				} elseif ($p['type'] == "rating") {
					$q1 = removeAllChoices($p, $userID);
					if (!$q1['ok']) {
						$error = 500;
						$error_description = $config['errors'][$error] . ": database error";
						done();
					}
					$ok = true;
				}
				done();
				fastcgi_finish_request();
			} else {
				$ok = false;
				$error = 400;
				$error_description = $config['errors'][$error] . ": id must be a numeric value";
			}
		} else {
			$ok = false;
			$error = 400;
			$error_description = $config['errors'][$error] . ": id is empty";
		}
	} elseif ($request == "/createpoll" and !$islocal) {
		if (!empty($query['title']) and !empty($query['type']) and !empty($query['privacy'])) {
			if (strlen($query['title']) <= 512 and strlen($query['description']) <= 512 and strlen($query['type']) <= 64 and strlen($query['privacy']) <= 32) {
				if (!in_array($query['type'], array_keys($config['types']))) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": type not found";
				} elseif (!$config['types'][$query['type']]) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": type not available at the moment";
				} elseif (!in_array($query['privacy'], array_keys($config['poll_privacy']))) {
					$error = 404;
					$error_description = $config['errors'][$error] . ": privacy not found";
				} else {
					if (in_array($query['type'], ["participation"]) and in_array($query['privacy'], ["anonymous"])) {
						$error = 405;
						$error_description = $config['errors'][$error] . ": privacy not allowed for this type of poll";
						done();
						die;
					}
					if (!$PDO) {
						try {
							$PDO = new PDO("pgsql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['database'], $config['database']['user'], $config['database']['password']);
						} catch (PDOException $e) {
							$error = 500;
							$error_description = $config['errors'][$error] . ": database connection lost";
							done();
							die;
						}
					}
					$poll = [
						'poll_id' => getID($userID),
						'creator' => $userID,
						'title' => $query['title'],
					];
					if (isset($query['description']) and !empty($query)) $poll['description'] = $query['description'];
					$poll['settings'] = [];
					$poll['type'] = $poll['settings']['type'] = $query['type'];
					$poll['votes'] = 0;
					$poll['anonymous'] = $config['poll_privacy'][$query['privacy']];
					if ($poll['type'] == "vote") {
						if (!empty($query['choices'])) {
							if (!is_array($query['choices'])) {
								$query['choices'] = json_decode($query['choices'], true);
							}
							if (is_array($query['choices'])) {
								if (empty($query['choices'])) {
									$error = 400;
									$error_description = $config['errors'][$error] . ": choices is empty";
									done();
									die;
								}
								foreach ($query['choices'] as $key => $value) {
									if (!$value and $value != "0") $value = $key;
									$poll['choice'][$value] = [];
								}
								$poll['usersvotes'] = $poll['choice'];
								$poll['choices'] = $poll['choice'];
								$chosenChoice = $poll['choices'];
								if ($poll['choices']) {
									foreach ($poll['choices'] as $user => $text) {
										$users[$user] = true;
									}
									$poll['choices'] = count($poll['choices']);
								} else {
									$users = [];
									$poll['choices'] = 0;
								}
							} else {
								$error = 400;
								$error_description = $config['errors'][$error] . ": choices must be an array";
								done();
								die;
							}
						} else {
							$error = 400;
							$error_description = $config['errors'][$error] . ": choices is empty";
							done();
							die;
						}
					} elseif ($poll['type'] == "doodle") {
						if (!empty($query['choices'])) {
							if (!is_array($query['choices'])) {
								$query['choices'] = json_decode($query['choices'], true);
							}
							if (is_array($query['choices'])) {
								if (empty($query['choices'])) {
									$error = 400;
									$error_description = $config['errors'][$error] . ": choices is empty";
									done();
									die;
								}
								foreach ($query['choices'] as $key => $value) {
									if (empty($value) and $value != "0") $value = $key;
									$poll['choice'][$value] = [];
								}
								$poll['usersvotes'] = $poll['choice'];
								$poll['choices'] = $poll['choice'];
								$chosenChoice = $poll['choices'];
								if ($poll['choices']) {
									foreach ($poll['choices'] as $user => $text) {
										$users[$user] = true;
									}
									$poll['choices'] = count($poll['choices']);
								} else {
									$users = [];
									$poll['choices'] = 0;
								}
							} else {
								$error = 400;
								$error_description = $config['errors'][$error] . ": choices must be an array";
								done();
								die;
							}
						} else {
							$error = 400;
							$error_description = $config['errors'][$error] . ": choices is empty";
							done();
							die;
						}
					} elseif ($poll['type'] == "limited doodle") {
						if (!empty($query['choices'])) {
							if (!is_array($query['choices'])) {
								$query['choices'] = json_decode($query['choices'], true);
							}
							if (is_array($query['choices'])) {
								if (empty($query['choices'])) {
									$error = 400;
									$error_description = $config['errors'][$error] . ": choices is empty";
									done();
									die;
								}
								foreach ($query['choices'] as $key => $value) {
									if (empty($value) and $value != "0") $value = $key;
									$poll['choice'][$value] = [];
								}
								$poll['usersvotes'] = $poll['choice'];
								$poll['choices'] = $poll['choice'];
								$chosenChoice = $poll['choices'];
								if ($poll['choices']) {
									foreach ($poll['choices'] as $user => $text) {
										$users[$user] = true;
									}
									$poll['choices'] = count($poll['choices']);
								} else {
									$users = [];
									$poll['choices'] = 0;
								}
							} else {
								$error = 400;
								$error_description = $config['errors'][$error] . ": choices must be an array";
								done();
								die;
							}
						} else {
							$error = 400;
							$error_description = $config['errors'][$error] . ": choices is empty";
							done();
							die;
						}
						if ($query['max_choices']) {
							if (is_numeric($query['max_choices'])) {
								$query['max_choices'] = round(str_replace("-", '', str_replace(".", '', $query['max_choices'])));
								if ($query['max_choices'] <= count(array_keys($poll['choice'])) and $query['max_choices'] >= 1) {
									$poll['settings']['max_choices'] = $query['max_choices'];
								} else {
									$error = 400;
									$error_description = $config['errors'][$error] . ": max_choices must be minus or equal to the count of choices";
									done();
									die;
								}
							} else {
								$error = 400;
								$error_description = $config['errors'][$error] . ": max_choices must be a numeric value";
								done();
								die;
							}
						} else {
							$error = 400;
							$error_description = $config['errors'][$error] . ": max_choices is empty";
							done();
							die;
						}
					} elseif ($poll['type'] == "quiz") {
						if (!empty($query['choices'])) {
							if (!is_array($query['choices'])) {
								$query['choices'] = json_decode($query['choices'], true);
							}
							if (is_array($query['choices'])) {
								if (empty($query['choices'])) {
									$error = 400;
									$error_description = $config['errors'][$error] . ": choices is empty";
									done();
									die;
								}
								foreach ($query['choices'] as $key => $value) {
									if (empty($value) and $value != "0") $value = $key;
									$poll['choice'][$value] = [];
								}
								$poll['usersvotes'] = $poll['choice'];
								$poll['choices'] = $poll['choice'];
								$chosenChoice = $poll['choices'];
								if ($poll['choices']) {
									foreach ($poll['choices'] as $user => $text) {
										$users[$user] = true;
									}
									$poll['choices'] = count($poll['choices']);
								} else {
									$users = [];
									$poll['choices'] = 0;
								}
							} else {
								$error = 400;
								$error_description = $config['errors'][$error] . ": choices must be an array";
								done();
								die;
							}
						} else {
							$error = 400;
							$error_description = $config['errors'][$error] . ": choices is empty";
							done();
							die;
						}
						if ($query['right_choices']) {
							$query['right_choices'] = json_decode($query['right_choices'], true);
							if (is_array($query['right_choices'])) {
								if (empty($query['right_choices'])) {
									$error = 400;
									$error_description = $config['errors'][$error] . ": right_choices is empty";
									done();
									die;
								}
								foreach ($query['right_choices'] as $key => $value) {
									if (empty($value) and $value != "0") $value = $key;
									if (!isset($poll['choice'][$value])) {
										$error = 400;
										$error_description = $config['errors'][$error] . ": right_choices must be existing on choices";
										done();
										die;
									}
									$poll['settings']['right_answers'][] = $value;
								}
							} else {
								$error = 400;
								$error_description = $config['errors'][$error] . ": right_choices must be an array";
								done();
								die;
							}
						} else {
							$error = 400;
							$error_description = $config['errors'][$error] . ": right_choices is empty";
							done();
							die;
						}
					} elseif ($poll['type'] == "board") {
						$poll['choice'] = [];
						$poll['choices'] = [];
						$poll['usersvotes'] = [];
					} elseif ($poll['type'] == "participation") {
						$poll['choice'] = ['participants' => []];
						$poll['choices'] = ['participants' => []];
						$poll['usersvotes'] = ['participants' => []];
					} elseif ($poll['type'] == "rating") {
						if (!empty($query['start_rating_range']) and !empty($query['end_rating_range'])) {
							if (is_numeric($query['start_rating_range']) and is_numeric($query['end_rating_range'])) {
								$query['start_rating_range'] = round(str_replace("-", '', str_replace(".", '', $query['start_rating_range'])));
								$query['end_rating_range'] = round(str_replace("-", '', str_replace(".", '', $query['end_rating_range'])));
								if ($query['start_rating_range'] > 0 and $query['start_rating_range'] < 10 and $query['end_rating_range'] > $query['start_rating_range'] and $query['end_rating_range'] <= 10) {
									$range = range(round($query['start_rating_range']), round($query['end_rating_range']));
									foreach($range as $number) {
										$poll['choice'][$number] = [];
									}
									$poll['choices'] = $poll['choice'];
									$poll['usersvotes'] = $poll['choice'];
								} else {
									if ($query['start_rating_range'] < 1 or $query['start_rating_range'] > 9) {
										$parameter = 'start_rating_range';
										$min = 1; $max = 9;
									} 
									if ($query['end_rating_range'] < $query['start_rating_range'] or $query['end_rating_range'] > 10) {
										if (!$parameter) {
											$parameter = 'end_rating_range';
											$min = 'start_rating_range'; $max = 10;
										}
									}
									$error = 400;
									$error_description = $config['errors'][$error] . ": $parameter must be more than $min and minus or equal to $max";
									done();
									die;
								}
							} else {
								$argsnull = ['start_rating_range', 'end_rating_range'];
								foreach($argsnull as $str) {
									if (!is_numeric($query[$str])) {
										if (!$parameter) $parameter = $str;
									}
								}
								$error = 400;
								$error_description = $config['errors'][$error] . ": $parameter must be a numeric value";
								done();
								die;
							}
						} else {
							$argsnull = ['start_rating_range', 'end_rating_range'];
							foreach($argsnull as $str) {
								if (empty($query[$str])) {
									if (!$parameter) $parameter = $str;
								}
							}
							$error = 400;
							$error_description = $config['errors'][$error] . ": $parameter is empty";
							done();
							die;
						}
					} else {
						$error = 404;
						$error_description = $config['errors'][$error] . ": poll type not found";
						done();
						die;
					}
					if (!empty($query['settings'])) {
						if (!is_array($query['settings'])) {
							$query['settings'] = json_decode($query['settings'], true);
						}
						if (is_array($query['settings'])) {
							$stringsetting = [
								"disable_web_page_preview",
								"disable_web_content_preview",
								"sort",
								"appendable",
								"notification",
								"web_content_preview",
								"votes_limit"
							];
							$newsettings = [];
							foreach (array_keys($query['settings']) as $string) {
								if (in_array($string, array_keys($stringsetting))) {
									if ($string == "disable_web_page_preview") {
										if (!is_bool($query['settings'][$string])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
											done();
											die;
										}
										if ($query['settings']['disable_web_page_preview']) {
											$poll['settings']['web_page'] = false;
											$newsettings['disable_web_page_preview'] = true;
										} else {
											unset($poll['settings']['web_content']);
											unset($poll['settings']['web_page']);
											$newsettings['disable_web_page_preview'] = true;
											if ($poll['settings']['web_page'] or $newsettings['disable_web_content_preview']) $newsettings['disable_web_content_preview'] = false;
										}
									} elseif ($string == "disable_web_content_preview") {
										if (!is_bool($query['settings'][$string])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
											done();
											die;
										}
										if ($query['settings']['disable_web_content_preview']) {
											if (!$poll['settings']['web_page']) $newsettings['disable_web_page_preview'] = true;
											$newsettings['disable_web_content_preview'] = $query['settings']['disable_web_content_preview'];
											$poll['settings']['web_content'] = true;
										} else {
											$error = 400;
											$error_description = "In order to disable the preview, edit the poll and set disable_web_content_preview to true";
											done();
											die;
										}
									} elseif ($string == "web_content_preview") {
										if (!is_string($query['settings'][$string])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type string";
											done();
											die;
										}
										if (!empty($query['settings']['web_content_preview'])) {
											if (!$poll['settings']['web_page']) $newsettings['disable_web_page_preview'] = false;
											$newsettings['disable_web_content_preview'] = $query['settings']['disable_web_content_preview'];
											$newsettings['web_content_preview'] = $query['settings'][$string];
											$poll['settings']['web_content'] = $query['settings'][$string];
										} else {
											$error = 400;
											$error_description = $config['errors'][$error] . "cannot be an empty string";
											done();
											die;
										}
									} elseif ($string == "sort") {
										if (!is_bool($query['settings'][$string])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
											done();
											die;
										}
										if (!in_array($poll['type'], ['vote', 'doodle', 'limited doodle'])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field $string isn't expected here";
											done();
											die;
										} elseif ($query['settings']['sort']) {
											$poll['settings']['sort'] = true;
											$newsettings['sort'] = true;
										} else {
											unset($poll['settings']['sort']);
											$newsettings['sort'] = false;
										}
									} elseif ($string == "appendable") {
										if (!is_bool($query['settings'][$string])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
											done();
											die;
										}
										if (!in_array($poll['type'], ['vote', 'doodle', 'limited doodle'])) {
											
										} elseif ($query['settings']['appendable']) {
											$poll['settings']['appendable'] = true;
											$newsettings['appendable'] = true;
										} else {
											unset($poll['settings']['appendable']);
											$newsettings['appendable'] = false;
										}
									} elseif ($string == "notification") {
										if (!is_bool($query['settings'][$string])) {
											$ok = false;
											$error = 400;
											$error_description = $config['errors'][$error] . ": field \"$string\" in settings must be of type Boolean";
											done();
											die;
										}
										if (!in_array($poll['type'], ['vote', 'doodle', 'limited doodle', 'board'])) {
											
										} elseif ($query['settings']['notification']) {
											$poll['settings']['notification'] = true;
											$newsettings['notification'] = true;
										} else {
											unset($poll['settings']['notification']);
											$newsettings['notification'] = false;
										}
									} elseif ($string == "votes_limit") {
										if (isset($query['settings']['votes_limit'])) {
											if (!is_numeric($query['settings']['votes_limit'])) {
												$error = 400;
												$error_description = $config['errors'][$error] . ": votes_limit must be a numeric value";
												done();
												die;
											}
											if ($query['settings']['votes_limit'] > 0) {
												$error = 400;
												$error_description = $config['errors'][$error] . ": Illegal integer on $settings";
												done();
												die;
											}
											if ($query['settings']['votes_limit'] === 0) {
												unset($poll['settings']['max_voters']);
												$newsettings['votes_limit'] = false;
											} elseif ($query['settings']['votes_limit'] < $poll['votes']) {
												$error = 400;
												$error_description = $config['errors'][$error] . ": Illegal integer on $settings";
												done();
												die;
											} else {
												$poll['settings']['max_voters'] = round($query['settings']['votes_limit']);
												$newsettings['votes_limit'] = round($query['settings']['votes_limit']);
											}
										} else {
											unset($poll['settings']['max_voters']);
											$newsettings['votes_limit'] = false;
										}
									}
								} else {
									$error = 400;
									$error_description = $config['errors'][$error] . ": field $string isn't expected on settings";
									done();
									die;
								}
							}
						} else {
							$ok = false;
							$error = 400;
							$error_description = $config['errors'][$error] . ": settings must be an array";
							done();
							die;
						}
					}
					$poll['settings'] = json_encode($poll['settings']);
					$poll['choices'] = json_encode($poll['choice']);
					if (!$poll['description']) $poll['description'] = "";
					$q = db_query("INSERT INTO polls (poll_id, user_id, status, anonymous, settings, title, description, choices) VALUES (?,?,?,?,?,?,?,?)", [$poll['poll_id'], $poll['creator'], 'open', $poll['anonymous'], $poll['settings'], $poll['title'], $poll['description'], $poll['choices']], 'no');
					if (!$q['ok']) {
						$error = 500;
						$error_description = "Internal server error: database error";
						done();
						die;
					} else {
						$p = sendPoll($poll['poll_id'], $poll['creator']);
						if (!$p['ok']) {
							$error = 500;
							if ($userID == 244432022) {
								$error_description = "Internal server error: " . $p['description'];
							} else {
								$error_description = "Internal server error: database error";
							}
							done();
							die;
						} else {
							$p = $p['result'];
						}
						$result = pollToArray($p);
						$result['participating'] = false;
						$ok = true;
					}
				}
			} else {
				$argsover = ['title' => 512, 'description' => 512, 'type' => 64, 'privacy' => 32, 'start_rating_range' => 1, 'end_rating_range' => 2];
				$argsmini = ['title' => 1, 'description' => 1, 'type' => 1, 'privacy' => 1, 'start_rating_range' => 1, 'end_rating_range' => 1];
				foreach($argsnull as $str => $limit) {
					if (strlen($query[$str]) >= $limit) {
						if (!isset($parameter)) $parameter = $str;
						$limiter = "long";
					} elseif (strlen($query[$str]) < $limit) {
						if (!isset($parameter)) $parameter = $str;
						$limiter = "short";
					}
				}
				$error = 400;
				$error_description = $config['errors'][$error] . ": $parameter is too $limiter";
			}
		} else {
			$argsnull = ['title', 'type', 'privacy'];
			foreach($argsnull as $str) {
				if (empty($query[$str])) {
					if (!isset($parameter)) $parameter = $str;
				}
			}
			$error = 400;
			$error_description = $config['errors'][$error] . ": $parameter is empty";
		}
	} elseif ($request == "/sendpoll" and !$islocal) {
		$error = 403;
	} elseif ($islocal) {
		$error = 404;
		$error_description = $config['errors'][$error] . ": Unknow method";
	} else {
		$error = 404;
		$error_description = $config['errors'][$error] . ": Method not found";
	}
} else {
	if (!isset($error)) $error = 405;
}

done();